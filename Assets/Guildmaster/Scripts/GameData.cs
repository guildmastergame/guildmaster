﻿namespace Assets.Guildmaster.Scripts {
    using Mech.Entities.Creatures;
    using Mech;
    using Mech.Quests;
    using System.Collections.Generic;
    using Control;

    public class GameData {
        public List<QuestTree> qTrees;
        public Guild guild;
        public Date date;

        public GameData(GuildController gc/*, List<QuestTree> qTrees*/) {
            date = new Date();
            Date.SetGameData(this);
            var qtc = QuestTreeContainer.Deserialize("CemetryProblem.qtree");
            qTrees = new List<QuestTree>(1);
            qTrees.Add(qtc.Unpack());
            
            guild = new Guild(gc);
            guild.SetHiringSlot(0, new HiringSlot(CreatureVault.Get("guild_rookie"), 100));
            guild.SetHiringSlot(1, new HiringSlot(CreatureVault.Get("guild_scout"), 125));
            guild.SetHiringSlot(2, new HiringSlot(CreatureVault.Get("guild_cleric"), 250));
        }
    }
}
