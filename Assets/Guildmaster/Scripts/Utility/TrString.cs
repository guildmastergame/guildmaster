﻿namespace Assets.Guildmaster.Scripts.Utility {
    using System;

    [Serializable]
    public sealed class TrString {
        private string key;
                
        public string Key { get { return key; } set { key = value.Clone() as string; } }

        public override string ToString() {
            return key == null ? "#NULL_KEY#" : Language.Get(this) ?? string.Format("#{0}#", key);
        }

        public TrString(string key = "no_key") {
            this.key = key;
        }
    }
}
