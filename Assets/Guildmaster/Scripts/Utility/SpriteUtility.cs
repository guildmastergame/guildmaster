﻿namespace Assets.Guildmaster.Scripts.Utility {
    using UnityEngine;
    /// <summary>
    /// http://answers.unity3d.com/questions/651984/convert-sprite-image-to-texture.html
    /// </summary>
    public static class SpriteUtility {
        public static Texture2D GetTexture(Sprite sprite) {
            if(sprite == null) {
                return null;
            }
            if (sprite.rect.width != sprite.texture.width) {
                Texture2D newText = new Texture2D(Mathf.RoundToInt(sprite.textureRect.width), Mathf.RoundToInt(sprite.textureRect.height));
                Color[] newColors = sprite.texture.GetPixels(Mathf.RoundToInt(sprite.textureRect.x),
                                                             Mathf.RoundToInt(sprite.textureRect.y),
                                                             Mathf.RoundToInt(sprite.textureRect.width),
                                                             Mathf.RoundToInt(sprite.textureRect.height));
                newText.SetPixels(newColors);
                newText.Apply();
                return newText;
            } else {
                return sprite.texture;
            }
        }
    }
}
