﻿using System;

namespace Assets.Guildmaster.Scripts.Utility {
    public static class MathUtility {
        public static void Clamp(ref int value, int min, int max) {
            if(value < min) {
                value = min;
            } else if(value > max) {
                value = max;
            }
        }
    }
}
