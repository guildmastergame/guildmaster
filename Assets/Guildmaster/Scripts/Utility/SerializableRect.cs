﻿using System;
using UnityEngine;

namespace Assets.Guildmaster.Scripts.Utility {
    [Serializable]
    public class SerializableRect {
        public float x, y, w, h;

        public Rect Rect { get { return new Rect(x, y, w, h); } }

        public SerializableRect(Rect r) {
            this.x = r.x;
            this.y = r.y;
            this.w = r.width;
            this.h = r.height;
        }
    }
}
