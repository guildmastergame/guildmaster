﻿namespace Assets.Guildmaster.Scripts.Utility {
    using UnityEngine;

    [System.Serializable]
    public struct BoundBox {
        private int x;
        private int y;
        private int w;
        private int h;
        private int maxX;
        private int maxY;

        private void CalcMaxX() {
            maxX = x + w;
        }

        private void CalcMaxY() {
            maxY = y + h;
        }

        public int X { get { return x; } set { x = value; CalcMaxX(); } }
        public int Y { get { return y; } set { y = value; CalcMaxY(); } }
        public int W { get { return w; } set { w = value > 0 ? value : 0; CalcMaxX(); } }
        public int H { get { return h; } set { h = value > 0 ? value : 0; CalcMaxY(); } }
        public int MaxX { get { return maxX; } }
        public int MaxY { get { return maxY; } }

        public BoundBox(int x, int y, int w, int h) {
            this.x = x;
            this.y = y;
            this.w = w;
            this.h = h;
            maxX = 0;
            maxY = 0;
            CalcMaxX();
            CalcMaxY();
        }

        public bool InBound(Vector2 v, float? clampZoom = null) {
            return v.x >= x && v.x < (clampZoom == null ? maxX : (clampZoom * maxX))
                && v.y >= y && v.y < (clampZoom == null ? maxY : (clampZoom * maxY));
        }

        public Vector2 Clamp(Vector2 v, float? clampZoom = null) {
            v.x = Mathf.Clamp(v.x, x, clampZoom == null ? maxX : (int)(clampZoom * maxX));
            v.y = Mathf.Clamp(v.y, y, clampZoom == null ? maxY : (int)(clampZoom * maxY));
            return v;
        }
    }
}
