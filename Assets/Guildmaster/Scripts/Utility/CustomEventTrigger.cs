﻿namespace Assets.Guildmaster.Scripts.Utility {
    using UnityEngine;
    using UnityEngine.Events;
    using UnityEngine.EventSystems;

    public class CustomEventTrigger : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {
        public UnityAction pointerEnter = null;
        public UnityAction pointerExit = null;

        public void OnPointerEnter(PointerEventData eventData) {
            if (pointerEnter != null) {
                pointerEnter();
            }
        }

        public void OnPointerExit(PointerEventData eventData) {
            if (pointerExit != null) {
                pointerExit();
            }
        }
    }
}
