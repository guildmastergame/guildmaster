﻿using UnityEngine;

namespace Assets.Guildmaster.Scripts.Utility {
    public static class Extensions {
        public static readonly Color Transparent = new Color(1, 1, 1, 0);

        public static Color WithAlpha(this Color color, float alpha) {
            return new Color(color.r, color.g, color.b, alpha);
        }
        public static void Clamp(this int value, int min, int max) {
            if(value < min) {
                value = min;
            } else if (value > max) {
                value = max;
            }
        }
    }
}
