﻿namespace Assets.Guildmaster.Scripts.Utility {
    using System.Collections.Generic;

    public static class Language {
        /// <summary>
        /// Containts a translations of languagekeys to all given languages
        /// </summary>
        private static Dictionary<string, string[]> translator = null;
        private static int curLanguageIdx = 0;
        
        public static void ChangeLanguage(int idx) {
            curLanguageIdx = idx;
        }

        public static void Initialize() {
            translator = new Dictionary<string, string[]>();
            //fill dictionary
        }

        public static bool IsInitialized() {
            return translator != null;
        }

        public static string Get(TrString s) {
            string[] strings;
            if(translator.TryGetValue(s.Key, out strings)) {
                return strings[curLanguageIdx];
            }
            return null;
        }
    }
}
