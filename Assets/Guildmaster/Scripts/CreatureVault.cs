﻿
namespace Assets.Guildmaster.Scripts {
    using Utility;
    using Mech;
    using System.Collections.Generic;
    using System.Runtime.Serialization.Formatters.Binary;
    using System.IO;
    using UnityEngine;

    public static class CreatureVault {
        #region Fields
        public const string DefaultFilePath = "creatures.vault";
        private static Dictionary<string, Creature> creatures = new Dictionary<string, Creature>();
        private static List<Creature> creatureList;
        private static Creature c;
        private static bool isInitialized;
        #endregion

        #region Accessors
        public static List<Creature> Creatures { get { return creatureList; } }
        public static bool IsInitialized { get { return isInitialized; } }
        #endregion

        #region Methods
        public static void Initialize(string filepath = DefaultFilePath) {
            if(IsInitialized) {
                Debug.LogWarning("Crature Vault is already initialized. Vault data reloaded");
            }
            if (filepath == string.Empty)
                return;
            BinaryFormatter bformatter = new BinaryFormatter();
            bformatter.Binder = new VersionDeserializationBinder();
            var stream = new FileStream(filepath, FileMode.Open);
            creatureList = bformatter.Deserialize(stream) as List<Creature>;
            stream.Close();
            creatures = new Dictionary<string, Creature>(creatureList.Count);
            if(creatureList != null) {
                for(int i = 0; i < creatureList.Count; ++i) {
                    creatures.Add(creatureList[i].CreatureKey, creatureList[i]);
                }
            }
        }

        public static Creature Get(string key) {
            if(creatures.TryGetValue(key, out c)) {
                return c.Clone();
            } else {
                return null;
            }
        }
        #endregion
    }
}
