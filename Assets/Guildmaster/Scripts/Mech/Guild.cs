﻿namespace Assets.Guildmaster.Scripts.Mech {
    using Entities.Creatures;
    using Containers;
    using Control;

    public class Guild {
        //public const int PARTIES_COUNT = 9;
        public const int MAX_MEMBERS = 21;
        protected CreatureContainer[] members = null;
        protected int money;
        protected int fame;
        //protected Party[] parties = new Party[PARTIES_COUNT];
        protected HiringSlot[] hiringSlots = new HiringSlot[3];
        protected GuildController gc;
        public int Money { get { return money; } }
        public int Fame { get { return fame; } }
        public int FreeMemberSlots {
            get {
                int slots = 0;
                for (int i = 0; i < MAX_MEMBERS; ++i) {
                    if (members[i].IsEmpty) {
                        ++slots;
                    }
                }
                return slots;
            }
        }

        public Guild(GuildController gc) {
            this.gc = gc;
            members = new CreatureContainer[MAX_MEMBERS];
            for (int i = 0; i < members.Length; ++i) {
                int _i = i;
                members[i] = new CreatureContainer(_i, gc.cGuildMembers.UpdateSlot);
            }
            money = 1000;
        }

        public CreatureContainer[] Members { get { return members; } set { members = value; } }
        //public Party[] Parties { get { return parties; } }
        public HiringSlot[] HiringSlots { get { return hiringSlots; } }

        public void SetMember(int idx, Creature creature) {
            if (idx < 0 || idx >= MAX_MEMBERS) {
                return;
            }
            members[idx].Put(creature);
        }

        public void SetHiringSlot(int idx, HiringSlot slot) {
            hiringSlots[idx] = slot;
        }

        public bool AddMember(Creature creature) {
            if (creature == null) {
                return false;
            }
            for (int i = 0; i < MAX_MEMBERS; ++i) {
                if (members[i].Put(creature)) {
                    return true;
                }
            }
            return false;
        }

        public Creature GetMember(int idx) {
            if (idx < 0 || idx >= MAX_MEMBERS) {
                return null;
            }
            return members[idx].Creature;
        }

        //public Party GetParty(int idx) {
        //    if (idx < 0 || idx >= parties.Length) {
        //        return null;
        //    }
        //    return parties[idx];
        //}

        public bool SpendMoney(int money) {
            if (money > this.money) {
                return false;
            }
            this.money -= money;
            gc.UpdateResourceMoney(this.money);
            return true;
        }

        //public void CreateParty(int idx, CreatureContainer leader, string name) {
        //    parties[idx] = new Party();
        //}

    }
}
