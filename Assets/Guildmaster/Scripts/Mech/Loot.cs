﻿namespace Assets.Guildmaster.Scripts.Mech {
    using Containers;
    using System;
    using UnityEngine;

    public enum LootType {
        Undefined,
        GuildAttribute = 1 << 30,
        Item = 1 << 29,
        Money = GuildAttribute | 1,
        Fame = GuildAttribute | 2,
    }

    [Serializable]
    public class Loot {
        #region Fields
        protected LootType type;
        protected Container content;
        protected int quantity;
        #endregion

        #region Accessors
        public Sprite Image {
            get {
                switch (type) {
                    case LootType.Item:
                        return content.Image;
                    case LootType.Money:
                        return SpriteVault.GetSpriteIcon("coins");
                    case LootType.Fame:
                        return SpriteVault.GetSpriteIcon("flag");
                    default:
                        return null;
                }
            }
        }
        public LootType Type { get { return type; } }
        public int Quantity { get { return quantity; } }
        public Container Content { get { return Content; } }
        #endregion

        #region Methods
        public Loot(LootType type, int quantity = 1, Container content = null) {
            this.type = type;
            this.quantity = quantity;
            this.content = content;
        }
        #endregion
    }
}
