﻿
namespace Assets.Guildmaster.Scripts.Mech.Battle {
    using Scripts.Battle;
    using Control;
    using System.Collections.Generic;
    using Attributes;

    public class InitiativeQueue {
        public const int MAX_POSSIBLE_TILES = 20; //5 columns * 4 rows
        protected BattleController cBattle = null;
        protected List<BattleCreature> curRound = new List<BattleCreature>(MAX_POSSIBLE_TILES);
        protected List<BattleCreature> nextRound = new List<BattleCreature>(MAX_POSSIBLE_TILES);
        protected int round; //Current round number
        public BattleCreature Current { get { return curRound.Count > 0 ? curRound[0] : null; } }
        /// <summary>
        /// All creatures in current round.
        /// </summary>
        public List<BattleCreature> CreaturesCurrentRound { get { return curRound; } }
        public List<BattleCreature> CreaturesNextRound { get { return nextRound; } }
        public List<BattleCreature> Creatures {
            get {
                List<BattleCreature> creatures = new List<BattleCreature>(curRound.Count + nextRound.Count);
                for (int i = 0; i < curRound.Count; ++i) {
                    creatures.Add(curRound[i]);
                }
                for (int i = 0; i < nextRound.Count; ++i) {
                    creatures.Add(nextRound[i]);
                }
                return creatures;
            }
        }

        public InitiativeQueue(BattleController cBattle, int startRound = 0) {
            this.cBattle = cBattle;
            round = startRound;
        }
        /// <summary>
        /// All creatures will be inserted in next round
        /// </summary>
        public void Insert(BattleCreature creature) {
            int insIdx = -1;
            if (nextRound.Count == 0) {
                nextRound.Add(creature);
                insIdx = 0;
            } else if (nextRound.Count == 1) {
                if (nextRound[0].Ini < creature.Ini) {
                    nextRound.Insert(0, creature);
                    insIdx = 0;
                } else {
                    nextRound.Add(creature);
                    insIdx = 1;
                }
            } else {
                insIdx = SearchIdx(creature.Ini);
                nextRound.Insert(insIdx, creature);
            }
            UpdatePanel();
        }
        protected int SearchIdx(Initiative ini) {
            for (int i = 0; i < nextRound.Count; ++i) {
                if (ini > nextRound[i].Ini) {
                    return i;
                }
            }
            return nextRound.Count;
        }
        protected void UpdatePanel() {
            int idx;
            for (int i = 0; i < cBattle.iniPanel.Capacity; ++i) {
                if (i < curRound.Count) {
                    cBattle.iniPanel.Set(i, curRound[i]);
                } else if ((idx = i - curRound.Count) < nextRound.Count) {
                    cBattle.iniPanel.Set(i, nextRound[idx]);
                } else {
                    cBattle.iniPanel.Hide(i);
                }
            }
        }
        public void NextRoundStarted() {
            ++round;
            curRound.Clear();
            for (int i = 0; i < nextRound.Count; ++i) {
                nextRound[i].WaitingUsed = false;
                if (round > 1) {
                    nextRound[i].RoundFinished();
                }
                curRound.Add(nextRound[i]);
                nextRound[i].RoundStarted();
            }
            nextRound.Clear();
            if (round > 1) {
                cBattle.RoundFinished();
            }
            cBattle.RoundStarted();
            cBattle.SelectCreature(Current);
        }
        /// <returns>Next creature in queue, or null if this was last creature in this round</returns>
        public void TurnFinished() {
            BattleCreature bc = curRound[0];
            bc.TurnFinished();
            curRound.RemoveAt(0);
            Insert(bc);
            if (Current == null) {
                NextRoundStarted();
            } else {
                cBattle.SelectCreature(Current);
            }
        }
        public void Wait() {
            BattleCreature bc = curRound[0];
            curRound.RemoveAt(0);
            bc.WaitingUsed = true;
            curRound.Add(bc);//Add to the end of the current round
            UpdatePanel();
            cBattle.SelectCreature(Current);
        }
        public void Remove(BattleCreature creature) {
            if (Current == creature) {
                TurnFinished();
            }
            curRound.Remove(creature);
            nextRound.Remove(creature);
            UpdatePanel();
        }
    }
}
