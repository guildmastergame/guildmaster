﻿namespace Assets.Guildmaster.Scripts.Mech.Battle {
    using Attributes;
    using UnityEngine;

    public struct TilePopupAttributes {
        public const float SHOW_DELAY = 1f;
        public const float FADE_SPEED = 2.0f;
        public int damage;
        public DamageType dmgType;
        public bool isDodge;
        public bool isBlock;
        public bool isCritical;
        public bool isHealing;
        public bool isResistance;

        public string Text {
            get {
                if (isDodge) {
                    return "DODGE";
                } else if (isBlock && damage == 0) {
                    return "BLOCK";
                } else if (isResistance && damage == 0) {
                    return "RESIST";
                }
                return damage.ToString();
            }
        }
        public Color TextColor {
            get {
                if (isCritical) {
                    return Color.red;
                }
                return Color.white;
            }
        }

        public Color TextOutlineColor {
            get {
                if (dmgType == DamageType.Poison) {
                    return Color.green;
                } else if (dmgType == DamageType.Cold) {
                    return Color.blue;
                } else if (dmgType == DamageType.Fire) {
                    return Color.yellow;
                } else if (dmgType == DamageType.Magic) {
                    return Color.magenta;
                }//else if (dmgType == DamageType.BLEED) {
                //    return new Color(255, 0, 128);//Pink
                //}
                return Color.red;
            }
        }

        public Sprite Image {
            get {
                if (isHealing) {
                    return SpriteVault.GetSpriteIcon("healing_splash");
                } else if (isDodge) {
                    return SpriteVault.GetSpriteIcon("icon_dodge");
                } else if (isBlock || isResistance) {
                    return SpriteVault.GetSpriteIcon("icon_armor");
                }
                switch (dmgType) {
                    case DamageType.Physical:
                        return SpriteVault.GetSpriteIcon("icon_damage_physical");
                    case DamageType.Magic:
                        return SpriteVault.GetSpriteIcon("icon_damage_magical");
                    case DamageType.Poison:
                        return SpriteVault.GetSpriteIcon("icon_damage_poison");
                    case DamageType.Cold:
                        return SpriteVault.GetSpriteIcon("icon_damage_cold");
                    case DamageType.Fire:
                        return SpriteVault.GetSpriteIcon("icon_damage_fire");
                    case DamageType.Bleed:
                        return SpriteVault.GetSpriteIcon("icon_damage_bleeding");
                }
                return null;
            }
        }
        public TilePopupAttributes(int damage, DamageType dmgType, bool isDodge, bool isBlock, bool isCritical, bool isHealing, bool isResistance) {
            this.damage = damage;
            this.dmgType = dmgType;
            this.isDodge = isDodge;
            this.isBlock = isBlock;
            this.isCritical = isCritical;
            this.isHealing = isHealing;
            this.isResistance = isResistance;
        }
    }
}
