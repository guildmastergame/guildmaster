﻿namespace Assets.Guildmaster.Scripts.Mech.Battle {
    using Abilities;
    using Control;
    using Scripts.Battle;
    using System;
    using System.Collections.Generic;

    public class BattleTargetSelector {
        public struct TargetSelectionParameters {
            public List<BattleTile> allowed;
            public List<BattleTile> selected;
            public BattleTile origin;
            public Ability ability;
            public TargetType targetType;

            public TargetSelectionParameters(BattleTile origin = null, Ability ability = null, TargetType type = TargetType.Undefined) {
                allowed = new List<BattleTile>();
                selected = new List<BattleTile>();
                if ((type == TargetType.Undefined || type == TargetType.Default) && ability != null) {
                    targetType = ability.Target;
                } else {
                    targetType = type;
                }
                this.origin = origin;
                this.ability = ability;
            }
        }

        protected BattleController cBattle = null;
        public BattleTargetSelector(BattleController cBattle) {
            this.cBattle = cBattle;
        }

        /// <summary>
        /// This method should be called when player's cursor appears over any tile to highlight this tile according to game state.
        /// Parameters same as for TileClicked method
        /// </summary>
        /// <param name="tile"></param>
        /// <param name="allowed"></param>
        /// <param name="type"></param>
        public void TileCursorEnter(BattleTile tile, TargetSelectionParameters param) {
            //if (param == null) {
            //    return;
            //}
            if (param.allowed != null && param.allowed.Contains(tile)) {
                switch (param.targetType) {
                    case TargetType.Ally:
                    case TargetType.Enemy:
                    case TargetType.MeleeAlly:
                    case TargetType.MeleeEnemy:
                    case TargetType.TileAlly:
                    case TargetType.TileEnemy:
                    case TargetType.Tile:
                        tile.ShowHighlight(TileHighlightType.GREEN);
                        break;
                    case TargetType.AllyRow:
                    case TargetType.EnemyRow:
                        for (int i = 0; i < param.allowed.Count; ++i) {
                            if (param.allowed[i].Pos.y == tile.Pos.y) {
                                param.allowed[i].ShowHighlight(TileHighlightType.GREEN);
                            }
                        }
                        break;
                }

            }
        }

        /// <summary>
        /// This method should be called when player's cursor exit's from any tile to clear highlight of this tile
        /// </summary>
        /// <param name="tile"></param>
        /// <param name="allowed"></param>
        /// <param name="type"></param>
        public void TileCursorExit(BattleTile tile, TargetSelectionParameters param) {
            //if (param == null) {
            //    return;
            //}
            tile.HideHighlight();
            if (tile == param.origin) {
                tile.ShowHighlight(TileHighlightType.BLUE);
            } else if (param.allowed != null && param.allowed.Contains(tile)) {
                switch (param.targetType) {
                    case TargetType.Ally:
                    case TargetType.Enemy:
                    case TargetType.MeleeAlly:
                    case TargetType.MeleeEnemy:
                    case TargetType.TileAlly:
                    case TargetType.TileEnemy:
                    case TargetType.Tile:
                        tile.ShowHighlight(TileHighlightType.RED);
                        break;
                    case TargetType.AllyRow:
                    case TargetType.EnemyRow:
                        for (int i = 0; i < param.allowed.Count; ++i) {
                            if (param.allowed[i].Pos.y == tile.Pos.y) {
                                param.allowed[i].ShowHighlight(TileHighlightType.RED);
                            }
                        }
                        break;
                }
            }
        }

        /// <summary>
        /// This method should be called when player clicks on any character in target selection mode to define required targets for given target type and allowed targets list
        /// </summary>
        /// <param name="tile">Clicked tile</param>
        /// <returns>true if tile target selected correctly</returns>
        public bool TileClicked(BattleTile tile, ref TargetSelectionParameters param) {
            if (param.allowed.Contains(tile)) {
                CheckTarget(param.targetType);
                if ((param.targetType & TargetType.Row) > 0) { //row of tiles
                    param.selected = cBattle.bf.GetCharacterRow(tile.Pos.y, param.origin.Creature.IsPlayers, (TargetType)((int)param.targetType - (int)TargetType.Row));
                } else { //single tile
                    param.selected = new List<BattleTile>(1);
                    param.selected.Add(tile);
                }
                return true;
            }
            return false;
        }

        /// <summary>
        /// Select list of possible targets for given target type, battle tile source and ability (if needed)
        /// </summary>
        /// <param name="targets">returned true: list of selected targets.
        /// returned false: list of targets that can be selected by player</param>
        /// <returns>True if targets were chosen immediately. False if target selection is required</returns>
        public bool Select(ref TargetSelectionParameters param) {
            CheckTarget(param.targetType);
            switch (param.targetType) {
                case TargetType.Undefined:
                    throw new NotImplementedException("Can't select target tiles for UNDEFINED value");
                case TargetType.Self:
                    param.selected = new List<BattleTile>(1);
                    param.selected.Add(param.origin);
                    return true;
                case TargetType.Allies:
                    param.selected = cBattle.bf.GetCharacters(param.origin, TargetType.Ally);
                    return true;
                case TargetType.Enemies:
                    param.selected = cBattle.bf.GetCharacters(param.origin, TargetType.Enemy);
                    return true;
                case TargetType.Everyone:
                    param.selected = cBattle.bf.GetCharacters(param.origin);
                    return true;
                case TargetType.MeleeEnemies:
                    param.selected = cBattle.bf.GetMelee(param.origin, TargetType.MeleeEnemy);
                    return true;
                case TargetType.MeleeAllies:
                    param.selected = cBattle.bf.GetMelee(param.origin, TargetType.MeleeAlly);
                    return true;
                case TargetType.MeleeEnemy:
                    param.allowed = cBattle.bf.GetMelee(param.origin, TargetType.MeleeEnemy);
                    break;
                case TargetType.MeleeAlly:
                    param.allowed = cBattle.bf.GetMelee(param.origin, TargetType.MeleeAlly);
                    break;
                case TargetType.Melee:
                    param.allowed = cBattle.bf.GetMelee(param.origin);
                    break;
                case TargetType.Enemy:
                case TargetType.EnemyRow:
                    param.allowed = cBattle.bf.GetCharacters(param.origin, TargetType.Enemy);
                    break;
                case TargetType.Ally:
                case TargetType.AllyRow:
                    param.allowed = cBattle.bf.GetCharacters(param.origin, TargetType.Ally);
                    break;
                case TargetType.Tile:
                    param.allowed = cBattle.bf.GetTiles(param.origin);
                    break;
                case TargetType.TileAlly:
                    param.allowed = cBattle.bf.GetTiles(param.origin, TargetType.Ally);
                    break;
                case TargetType.TileEnemy:
                    param.allowed = cBattle.bf.GetTiles(param.origin, TargetType.Enemy);
                    break;
            }
            return false;
        }

        public void HighlightTilesAllowedToSelect(TargetSelectionParameters param) {
            param.origin.ShowHighlight(TileHighlightType.BLUE);
            for (int i = 0; i < param.allowed.Count; ++i) {
                param.allowed[i].ShowHighlight((param.ability.Target & TargetType.Enemy) > 0 ?  TileHighlightType.RED : TileHighlightType.GREEN);
            }
        }

        public void HideSelectionHighlight(TargetSelectionParameters param) {
            for (int i = 0; i < param.allowed.Count; ++i) {
                param.allowed[i].HideHighlight();
            }
            param.origin.ShowHighlight(TileHighlightType.BLUE);
        }

        /// <summary>
        /// Throws exception if targetType isn't valid (have default value or not initialized at all
        /// </summary>
        private void CheckTarget(TargetType targetType) {
            if (targetType == TargetType.Default || targetType == TargetType.Undefined) {
                throw new NotImplementedException(string.Format("Can't select for given target type %s", targetType.ToString()));
            }
        }
    }
}
