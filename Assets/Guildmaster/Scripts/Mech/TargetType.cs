﻿namespace Assets.Guildmaster.Scripts.Mech {
    public enum TargetType {
        Undefined = 0,
        Self = 1,
        Melee = 2,
        Enemy = 4,
        Ally = 8,
        Enemies = 16,
        Allies = 32,
        Tile = 64,
        Row = 128,
        Tiles = 256,
        MeleeEnemy = Melee | Enemy,
        MeleeEnemies = Melee | Enemies,
        MeleeAlly = Melee | Ally,
        MeleeAllies = Melee | Allies,
        EnemyRow = Enemy | Row,
        AllyRow = Ally | Row,
        Everyone = Enemies | Allies,
        TileEnemy = Tile | Enemy,
        TileAlly = Tile | Ally,
        Default = 1 << 15
    }
}
