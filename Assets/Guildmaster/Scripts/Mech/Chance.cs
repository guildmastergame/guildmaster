﻿namespace Assets.Guildmaster.Scripts.Mech {
    using UnityEngine;
    [System.Serializable]
    public struct Chance {
        #region Fields
        private const float Floor = 0f;
        private const float Ceil = 100f;
        private float percentage;
        #endregion

        #region Accessors
        public static readonly Chance Always = new Chance(Ceil);
        public static readonly Chance Never = new Chance(Floor);
        public float Value { get { return percentage; } set {
                percentage = value < Floor ? Floor : (value > Ceil ? Ceil : value);
            } }
        #endregion

        #region Methods
        public override string ToString() {
            return string.Format("{0:0.;;1}%", Mathf.Round(percentage));
        }
        public bool Try() {
            return percentage == Ceil ? true : (percentage == Floor ? false : Random.Range(Floor, Ceil) < percentage);
        }
        public Chance Clone() {
            return new Chance(percentage);
        }
        #endregion

        public Chance(float percentage) {
            this.percentage = percentage;
        }

        public static Chance operator +(Chance a, Chance b) {
            return new Chance(a.percentage + b.percentage);
        }
        public static Chance operator -(Chance a, Chance b) {
            var res = a.percentage + b.percentage;
            return res < 0f ? Chance.Never : new Chance(res);
        }
        public static bool operator ==(Chance a, Chance b) {
            return a.percentage == b.percentage;
        }
        public static bool operator !=(Chance a, Chance b) {
            return a.percentage != b.percentage;
        }
    }
}
