﻿namespace Assets.Guildmaster.Scripts.Mech.Abilities {
    using Utility;
    using Effects;
    using System;
    using System.Collections.Generic;
    using Scripts.Battle;
    using Battle;

    [Serializable]
    public class AbilityActive : AbilityPassive {
        #region Fields
        protected List<Effect> actives;
        #endregion

        #region Accessors
        public List<Effect> Actives { get { return actives; } }
        #endregion

        #region Methods
        /// <summary>
        /// Only for editor. In game usage isn't welcome
        /// </summary>
        /// <param name="e"></param>
        public void AddActiveEffect(Effect e) {
            actives.Add(e);
        }
        public void EditActiveEffect(int idx, Effect e) {
            actives[idx] = e;
        }

        public List<T> GetActiveEffectsOfType<T>(EffectType type) where T : Effect {
            List<T> result = new List<T>();
            for (int i = 0; i < actives.Count; ++i) {
                if (actives[i].Type == type) {
                    result.Add(actives[i] as T);
                }
            }
            return result;
        }
        public override Ability Clone() {
            var passives = new List<Effect>(this.passives.Count);
            for (int i = 0; i < this.passives.Count; ++i) {
                passives.Add(this.passives[i].Clone() as Effect);
            }
            var actives = new List<Effect>(this.actives.Count);
            for (int i = 0; i < this.actives.Count; ++i) {
                actives.Add(this.actives[i].Clone() as Effect);
            }
            return new AbilityActive(type, actives, passives, target, cooldown, name, desc, imageKey, curCooldown, usedInThisTurn, battlePos);
        }

        #endregion

        public AbilityActive(Ability ability) : base(ability) {
            this.type = AbilityType.Action;
            this.actives = new List<Effect>();
            var acts = ability as AbilityActive;
            if (acts != null) {
                this.actives = acts.actives;
            } else {
                this.actives = new List<Effect>();
            }
        }

        public AbilityActive() : base() {
            this.type = AbilityType.Action;
            this.actives = new List<Effect>();
        }

        public AbilityActive(AbilityType type, List<Effect> actives, List<Effect> passives, TargetType target, int cooldown = 0, TrString name = null, TrString desc = null, string imgKey = "", int curCooldown = 0, bool usedInThisTurn = false, BattlePositionType battlePos = BattlePositionType.General) : base(passives, target, cooldown, name, desc, imgKey, curCooldown, usedInThisTurn, battlePos) {
            this.type = type;
            this.actives = actives;
        }

        public void ApplyToTarget(BattleTile target, BattleTile source = null) {
            bool isBlocked = false;
            if (target.Creature != null) {
                //Try to decrease damage by block
                List<EffectBlock> blocks = target.Creature.GetPassiveEffectsOfType<EffectBlock>(EffectType.Block);
                for (int i = 0; i < blocks.Count; ++i) {
                    if (blocks[i].BlockAction(type)) {
                        isBlocked = true;
                        break;
                    }
                }
            }
            if (!isBlocked) {
                //get target resistances
                var resists = target.Creature != null ? target.Creature.GetPassiveEffectsOfType<EffectResistance>(EffectType.Resistance) : new List<EffectResistance>();
                for (int i = 0; i < actives.Count; ++i) {
                    bool effectResistance = false;
                    for (int j = 0; j < resists.Count; ++j) {
                        if (resists[j].ResistEffect(actives[i].Type)) {
                            effectResistance = true;
                            target.ShowPopup(new TilePopupAttributes(0, Attributes.DamageType.Physical, false, false, false, false, true));
                            break;
                        }
                    }
                    if (!effectResistance) {
                        actives[i].Apply(target, source, type);
                    }
                }
            } else {
                target.ShowPopup(new TilePopupAttributes(0, Attributes.DamageType.Physical, false, true, false, false, false));
            }
            CurrentCooldown = Cooldown;
            UsedInThisTurn = true;
        }
    }
}
