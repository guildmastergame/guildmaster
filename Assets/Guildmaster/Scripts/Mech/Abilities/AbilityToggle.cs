﻿namespace Assets.Guildmaster.Scripts.Mech.Abilities {
    using Utility;
    using System;
    using Effects;
    using System.Collections.Generic;

    [Serializable]
    public class AbilityToggle : Ability {
        #region Fields
        protected List<Effect> deactivated;
        protected List<Effect> activated;
        protected bool isActivated;
        #endregion

        #region Accessors
        public bool IsActivated { get { return isActivated; } set { isActivated = value; } }
        public List<Effect> Deactivated { get { return deactivated; } }
        public List<Effect> Activated { get { return activated; } }
        #endregion

        #region Methods
        /// <summary>
        /// Only for editor. In game usage isn't welcome
        /// </summary>
        /// <param name="e"></param>
        public void AddActivatedEffect(Effect e) {
            activated.Add(e);
        }
        public void EditActivatedEffect(int idx, Effect e) {
            activated[idx] = e;
        }

        /// <summary>
        /// Only for editor. In game usage isn't welcome
        /// </summary>
        /// <param name="e"></param>
        public void AddDeactivatedEffect(Effect e) {
            deactivated.Add(e);
        }
        public void EditDeactivatedEffect(int idx, Effect e) {
            deactivated[idx] = e;
        }

        public override List<T> GetPassiveEffectsOfType<T>(EffectType type) {
            List<T> result = new List<T>();
            var efx = isActivated ? activated : deactivated;
            for(int i = 0; i < efx.Count; ++i) {
                if(efx[i].Type == type) {
                    result.Add(efx[i] as T);
                }
            }
            return result;
        }
        public void Toggle() {
            isActivated = !isActivated;
        }

        public override Ability Clone() {
            var activated = new List<Effect>(this.activated.Count);
            for (int i = 0; i < this.activated.Count; ++i) {
                activated.Add(this.activated[i].Clone() as Effect);
            }
            var deactivated = new List<Effect>(this.deactivated.Count);
            for (int i = 0; i < this.deactivated.Count; ++i) {
                deactivated.Add(this.deactivated[i].Clone() as Effect);
            }
            return new AbilityToggle(deactivated, activated, target, name, desc, imageKey, battlePos);
        }
        #endregion

        public AbilityToggle(Ability ability) : base (ability) {
            this.type = AbilityType.Toggle;
            this.activated = new List<Effect>();
            this.deactivated = new List<Effect>();
            isActivated = false;
        }

        public AbilityToggle() : base() {
            this.type = AbilityType.Toggle;
            this.activated = new List<Effect>();
            this.deactivated = new List<Effect>();
            isActivated = false;
        }

        public AbilityToggle(List<Effect> deactivated, List<Effect> activated, TargetType target, TrString name = null, TrString desc = null, string imgKey = "", BattlePositionType battlePos = BattlePositionType.General) : base(AbilityType.Toggle, target, 0, name, desc, imgKey, 0, false, battlePos) {
            this.deactivated = deactivated;
            this.activated = activated;
            isActivated = false;
        }

    }
}
