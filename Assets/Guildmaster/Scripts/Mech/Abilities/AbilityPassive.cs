﻿namespace Assets.Guildmaster.Scripts.Mech.Abilities {
    using System;
    using System.Collections.Generic;
    using Effects;
    using Utility;

    [Serializable]
    public class AbilityPassive : Ability {
        #region Fields
        protected List<Effect> passives;
        #endregion

        #region Accessors
        public List<Effect> Passives { get { return passives; } }
        #endregion

        #region Methods
        /// <summary>
        /// Only for editor. In game usage isn't welcome
        /// </summary>
        /// <param name="e"></param>
        public void AddPassiveEffect(Effect e) {
            passives.Add(e);
        }
        public void EditPassiveEffect(int idx, Effect e) {
            passives[idx] = e;
        }

        public override List<T> GetPassiveEffectsOfType<T>(EffectType type) {
            List<T> result = new List<T>();
            for(int i = 0; i < passives.Count; ++i) {
                if(passives[i].Type == type) {
                    result.Add(passives[i] as T);
                }
            }
            return result;
        }

        public override Ability Clone() {
            var passives = new List<Effect>(this.passives.Count);
            for(int i = 0; i < this.passives.Count; ++i) {
                passives.Add(this.passives[i].Clone() as Effect);
            }
            return new AbilityPassive(passives, target, cooldown, name, desc, imageKey, curCooldown, usedInThisTurn, battlePos);
        }
        #endregion

        public AbilityPassive(Ability ability) : base(ability) {
            this.type = AbilityType.Passive;
            var pass = ability as AbilityPassive;
            if (pass != null) {
                this.passives = pass.passives;
            } else {
                this.passives = new List<Effect>();
            }
        }

        public AbilityPassive() : base() {
            this.type = AbilityType.Passive;
            this.passives = new List<Effect>();
        } 

        public AbilityPassive(List<Effect> passives, TargetType target, int cooldown = 0, TrString name = null, TrString desc = null, string imgKey = "", int curCooldown = 0, bool usedInThisTurn = false, BattlePositionType battlePos = BattlePositionType.General) : base(AbilityType.Passive, target, cooldown, name, desc, imgKey, curCooldown, usedInThisTurn, battlePos) {
            this.passives = passives;
        }
    }
}
