﻿namespace Assets.Guildmaster.Scripts.Mech.Abilities {
    using Items;
    using System;
    using UnityEngine;

    [Serializable]
    public class AbilitySlot {
        #region Fields
        protected Ability ability;
        protected Item equip;
        protected ItemType allowedEquip;
        #endregion

        #region Accessors
        public Ability Ability { get { return equip == null ? ability : equip.Ability; } }
        public Ability DefaultAbility { get { return ability; } set { ability = value; } }
        public Item Item { get { return equip; } set { equip = value; } }
        public ItemType AllowedItems { get { return allowedEquip; } set { allowedEquip = value; } }
        public Sprite Image { get { return equip == null ? ability.Image : equip.Image; } } 
        #endregion

        #region Methods
        public AbilitySlot(Ability ability) {
            this.ability = ability;
            equip = null;
            allowedEquip = 0;
        }
        public AbilitySlot(ItemType allowedEquip, Item equip) {
            this.equip = equip;
            this.allowedEquip = allowedEquip;
        }
        public void AbilityChanged(Ability ability) {
            this.ability = ability;
        }
        public void ItemChanged(Item item) {
            this.equip = item;
        }

        public AbilitySlot Clone() {
            if(allowedEquip == 0) {
                return new AbilitySlot(ability.Clone());
            }
            return new AbilitySlot(allowedEquip, equip.Clone());
        }
        #endregion

    }
}
