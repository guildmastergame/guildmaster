﻿namespace Assets.Guildmaster.Scripts.Mech.Abilities.Effects {
    using System;
    using Battle;
    using Attributes;
#if UNITY_EDITOR
    using UnityEditor;
#endif
    using Scripts.Battle;
    using System.Text;
    using Entities.Creatures;

    [Serializable]
    public class EffectDamageMod : Effect {
        #region Fields
        protected DamageType damageType;
        protected AbilityType abilityType;
        protected int app;//applicator +
        protected float mod;//modificator *
        #endregion

        #region Accessors
        public DamageType DamageType { get { return damageType; } }
        public AbilityType AbilityType { get { return abilityType; } }
        public int App { get { return app; } }
        public float Mod { get { return mod; } }
        #endregion


        #region Methods
        public EffectDamageMod(EffectType type, DamageType damageType, AbilityType abilityType, int app, float mod, EffectTarget target = EffectTarget.Default, TriggerType trigger = TriggerType.Use, int life = 0, Chance? chance = null) : base(type, target, trigger, life, chance.HasValue ? chance.Value : Chance.Always) {
            this.damageType = damageType;
            this.abilityType = abilityType;
            this.app = app;
            this.mod = mod;
        }
        public override Effect Clone() {
            return new EffectDamageMod(type, damageType, abilityType, app, mod, target, trigger, life, chance);
        }
        
        public override string GetDescription(ICreature owner, int abilityIdx, bool isActive) {
            StringBuilder sb = new StringBuilder();
            if(app != 0) {
                sb.Append(string.Format("<color=#white>{0}{1}</color> {2} {3} damage {4}\n",
                    app > 0 ? "+" : string.Empty, app, abilityType.ToString(),
                    damageType.ToString(), type == EffectType.DamageDealMod ? "dealt" : "received"));
            }
            if (mod != 1) {
                sb.Append(string.Format("<color=#white>{0}{1}%</color> {2} {3} damage {4}\n",
                    mod > 1 ? "+" : string.Empty, (mod - 1f) * 100f, abilityType.ToString(),
                    damageType.ToString(), type == EffectType.DamageDealMod ? "dealt" : "received"));
            }
            return sb.ToString();
        }
        protected override void Apply(BattleCreature target, BattleCreature source = null, AbilityType abilityType = AbilityType.Undefined) {
            throw new NotImplementedException();
        }

        public override void EditEffectOnGUI() {
#if UNITY_EDITOR
            damageType = (DamageType)EditorGUILayout.EnumPopup("Damage Type", (EditorDamageType)damageType);
            abilityType = (AbilityType)EditorGUILayout.EnumPopup("Ability Type", (EditorActiveAbilityType)abilityType);
            mod = EditorGUILayout.FloatField("Modificator(*)", mod);
            app = EditorGUILayout.IntField("Applicator(+)", app);
            base.EditEffectOnGUI();
#endif
        }

        public void AffectDamage(ref Damage dmg, AbilityType abilityType) {
            if ((this.abilityType & abilityType) == abilityType && (this.damageType & dmg.Type) == dmg.Type) {
                dmg.AddApp(App);
                dmg.AddMod(mod);
            }
        }
        #endregion
    }
}
