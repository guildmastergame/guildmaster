﻿namespace Assets.Guildmaster.Scripts.Mech.Abilities.Effects {
    using System;
    using Battle;
    using Scripts.Battle;
    using UnityEngine;
    using Entities.Creatures;

    [Serializable]
    public class EffectPassTurn : Effect {
        #region Accessors
        public override Sprite Image {
            get {
                switch (type) {
                    case EffectType.Stun:
                        return SpriteVault.GetSpriteIcon("icon_stun");
                    case EffectType.Freeze:
                        return SpriteVault.GetSpriteIcon("icon_freeze");
                    default:
                        return null;
                }
            }
        }
        #endregion

        #region Methods
        public EffectPassTurn(EffectType type, EffectTarget target = EffectTarget.Default, TriggerType trigger = TriggerType.Use, int life = 0, Chance? chance = null) : base(type, target, trigger, life, chance.HasValue ? chance.Value : Chance.Always) {}

        public override Effect Clone() {
            return new EffectPassTurn(type, target, trigger, life, chance);
        }

        private string GetPassTurnEffectName(EffectType type) {
            switch (type) {
                case EffectType.Stun:
                    return "Stun";
                case EffectType.Freeze:
                    return "Freeze";
                default:
                    return "UnhandledPassTurnEffectType";
            }
        }

        public override string GetDescription(ICreature owner, int abilityIdx, bool isActive) {
            if(chance == Chance.Always) {
                return string.Format("{0}s target for {1} round{2}", GetPassTurnEffectName(type), life, life > 1 ? "s" : string.Empty);
            }
            return string.Format("<color=#white>{0}</color> chance to {1} target for {2} rounds", chance.ToString(), GetPassTurnEffectName(type), life);
        }
        protected override void Apply(BattleCreature target, BattleCreature source = null, AbilityType abilityType = AbilityType.Undefined) {
            var effect = Clone();
            effect.IsAppliedOnThisRound = true;
            target.ApplyEffect(effect);
        }

        public override void EditEffectOnGUI() {
            base.EditEffectOnGUI();
        }
        #endregion
    }
}
