﻿namespace Assets.Guildmaster.Scripts.Mech.Abilities.Effects {
    using System;
    using Battle;
    using Attributes;
#if UNITY_EDITOR
    using UnityEditor;
#endif
    using Scripts.Battle;
    using Entities.Creatures;

    [Serializable]
    public class EffectCriticalChance : Effect {
        #region Fields
        public const float CriticalPowerMod = 2f;
        //protected Chance criticalChance;
        protected AbilityType abilityType;
        protected DamageType damageType;
        #endregion

        #region Accessors
        //public Chance CriticalChance { get { return criticalChance; } }
        public AbilityType AbilityType { get { return abilityType; } }
        public DamageType DamageType { get { return damageType; } }
        #endregion

        #region Methods
        public EffectCriticalChance(/*Chance criticalChance,*/ AbilityType abilityType, DamageType damageType, EffectTarget target = EffectTarget.Default, TriggerType trigger = TriggerType.Use, int life = 0, Chance? chance = null) : base(EffectType.CriticalChance, target, trigger, life, chance.HasValue ? chance.Value : Chance.Always) {
            //this.criticalChance = criticalChance;
            this.abilityType = abilityType;
            this.damageType = damageType;
        }

        public override Effect Clone() {
            return new EffectCriticalChance(/*criticalChance,*/ abilityType, damageType, target, trigger, life, chance);
        }
        public override string GetDescription(ICreature owner, int abilityIdx, bool isActive) {
            return string.Format("{0} critical {1} chance for {2} damage", chance.ToString(), abilityType.ToString(), EffectDamage.DamageTypeToString(damageType));
        }
        protected override void Apply(BattleCreature target, BattleCreature source = null, AbilityType abilityType = AbilityType.Undefined) {
            throw new NotImplementedException();
        }

        public override void EditEffectOnGUI() {
#if UNITY_EDITOR
            damageType = (DamageType)EditorGUILayout.EnumPopup("Damage Type", (EditorDamageType)damageType);
            abilityType = (AbilityType)EditorGUILayout.EnumPopup("Ability Type", (EditorActiveAbilityType)abilityType);
            //criticalChance.Value = EditorGUILayout.FloatField("Critical Hit Chance", criticalChance.Value);
            base.EditEffectOnGUI();
#endif
        }

        public Chance TryGetChance(DamageType type, AbilityType abilityType) {
            if((this.damageType & type) == type && (this.abilityType & abilityType) == abilityType) {
                return this.chance;
            }
            return Chance.Never;
        }
        #endregion
    }
}
