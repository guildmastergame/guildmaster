﻿namespace Assets.Guildmaster.Scripts.Mech.Abilities.Effects {
    public enum EffectTarget {
        Default = 0,// Target of the ability
        AllAround = 1, // Every tile around the creature <|*|>
        Neighbours = 2, // Only tile neighbours from left and right  <*> 
        Self = 3,
        //etc.
    }
}
