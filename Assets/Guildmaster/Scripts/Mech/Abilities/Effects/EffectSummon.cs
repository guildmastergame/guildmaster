﻿namespace Assets.Guildmaster.Scripts.Mech.Abilities.Effects {
    using System;
    using Battle;
#if UNITY_EDITOR
    using UnityEditor;
#endif
    using UnityEngine;
    using Scripts.Battle;

    [Serializable]
    public class EffectSummon : Effect {
        #region Fields
        protected string frontCreatureKey;
        protected string rearCreatureKey;
        #endregion

        #region Accessors
        public string FrontKey { get { return frontCreatureKey; } set { frontCreatureKey = value; } }
        public string RearKey { get { return frontCreatureKey; } set { rearCreatureKey = value; } }

        public object SelectCreatureEditorPopup {
            get;
            private set;
        }
        #endregion

        #region Methods
        public EffectSummon(string frontCreatureKey, string rearCreatureKey, EffectTarget target = EffectTarget.Default, TriggerType trigger = TriggerType.Use, int life = 0, Chance? chance = null) : base(EffectType.AbilityUse, target, trigger, life, chance.HasValue ? chance.Value : Chance.Always) {
            this.frontCreatureKey = frontCreatureKey;
            this.rearCreatureKey = rearCreatureKey;
        }

        public override Effect Clone() {
            return new EffectSummon(frontCreatureKey, rearCreatureKey, target, trigger, life, chance);
        }

        protected override void Apply(BattleCreature target, BattleCreature source = null, AbilityType abilityType = AbilityType.Undefined) {
            throw new NotImplementedException();
        }
        public override void EditEffectOnGUI() {
#if UNITY_EDITOR
            EditorGUILayout.BeginHorizontal();
            //TODO
            //image
            //name
            if(GUILayout.Button("Select Front")) {
                //SelectCreatureEditorPopup
            }
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.BeginHorizontal();
            //TODO
            if (GUILayout.Button("Select Rear")) {
                //SelectCreatureEditorPopup
            }
            EditorGUILayout.EndHorizontal();
            base.EditEffectOnGUI();
#endif
        }
        #endregion
    }
}
