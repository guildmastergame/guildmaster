﻿namespace Assets.Guildmaster.Scripts.Mech.Abilities.Effects {
    using System;
    using Battle;
#if UNITY_EDITOR
    using UnityEditor;
#endif
    using Scripts.Battle;
    using Entities.Creatures;

    [Serializable]
    public class EffectActionMod : Effect {
        #region Fields
        protected int actions;
        #endregion

        #region Accessors
        public int Actions { get { return actions; } }
        #endregion

        #region Methods
        public EffectActionMod(int actions, EffectTarget target = EffectTarget.Default, TriggerType trigger = TriggerType.Use, int life = 0, Chance? chance = null) : base(EffectType.ActionMod, target, trigger, life, chance.HasValue ? chance.Value : Chance.Always) {
            this.actions = actions;
        }

        public override Effect Clone() {
            return new EffectActionMod(actions, target, trigger, life, chance);
        }

        public override string GetDescription(ICreature c, int abilityIdx, bool isActive) {
            return string.Format("<color=#white>{0}{1}</color> Action{2} per round", actions > 0 ? "+" : string.Empty, actions, actions > 1 ? "s" : string.Empty);
        }

        protected override void Apply(BattleCreature target, BattleCreature source = null, AbilityType abilityType = AbilityType.Undefined) {
            throw new NotImplementedException();
        }

        public override void EditEffectOnGUI() {
#if UNITY_EDITOR
            actions = EditorGUILayout.IntField("Additional Actions Count", actions);
            base.EditEffectOnGUI();
#endif
        }
        #endregion
    }
}
