﻿namespace Assets.Guildmaster.Scripts.Mech.Abilities.Effects {
    using System;
    using Battle;
#if UNITY_EDITOR
    using UnityEditor;
#endif
    using Scripts.Battle;
    using Entities.Creatures;

    [Serializable]
    public class EffectRemoval : Effect {
        #region Fields
        protected EffectType removeType;
        #endregion

        #region Accessors
        public EffectType RemoveType { get { return removeType; } }
        #endregion

        #region Methods
        public EffectRemoval(EffectType removeType, EffectTarget target = EffectTarget.Default, TriggerType trigger = TriggerType.Use, int life = 0, Chance? chance = null) : base(EffectType.Removal, target, trigger, life, chance.HasValue ? chance.Value : Chance.Always) {
            this.removeType = removeType;
        }
        public override Effect Clone() {
            return new EffectRemoval(removeType, target, trigger, life, chance);
        }
        public override string GetDescription(ICreature owner, int abilityIdx, bool isActive) {
            //TODO: correct description. It's too lazy now
            if (chance == Chance.Always) {
                return string.Format("Removes effect of type: {0}", removeType.ToString());
            }
            return string.Format("<color=#white>{0}</color> chance to remove effect of type: {1}", chance.ToString(), removeType.ToString());
        }
        protected override void Apply(BattleCreature target, BattleCreature source = null, AbilityType abilityType = AbilityType.Undefined) {
            var efx = target.Effects;
            bool isRemovedAny = false;
            for (int i = 0; i < efx.Count; ++i) {
                if(efx[i].Type == type) {
                    efx.RemoveAt(i--);
                    isRemovedAny = true;
                }
            }
            if (isRemovedAny) {
                target.Tile.RefreshEffects();
            }
        }

        public override void EditEffectOnGUI() {
#if UNITY_EDITOR
            removeType = (EffectType)EditorGUILayout.EnumPopup("Remove Type", removeType);
            base.EditEffectOnGUI();
#endif
        }
        #endregion
    }
}
