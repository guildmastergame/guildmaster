﻿namespace Assets.Guildmaster.Scripts.Mech.Abilities.Effects {
    using System;
#if UNITY_EDITOR
    using UnityEditor;
#endif
    using Scripts.Battle;
    using System.Collections.Generic;
    using Entities.Creatures;

    [Serializable]
    public class EffectHeal : Effect {
        #region Fields
        protected int hp;
        #endregion

        #region Accessors
        public int Hp { get { return hp; } }
        #endregion

        #region Methods
        public EffectHeal(int hp, EffectTarget target = EffectTarget.Default, TriggerType trigger = TriggerType.Use, int life = 0, Chance? chance = null) : base (EffectType.Heal, target, trigger, life, chance.HasValue ? chance.Value : Chance.Always) {
            this.hp = hp;
        }
        public override Effect Clone() {
            return new EffectHeal(hp, target, trigger, life, chance);
        }
        public override string GetDescription(ICreature owner, int abilityIdx, bool isActive) {
            var heal = hp;
            ApplyHealDealMods(ref heal, owner);
            if (isActive) {
                //heal - current heal, hp - effect healing power
                return string.Format(heal > hp ? "Heals <color=#00ff00ff>{0}</color> HP"
                    : heal < hp ? "Heals <color=#ff0000ff>{0}</color> HP"
                    : "Heals <color=#white>{0}</color> HP",
                    heal);
            } else {
                return "UNHANDLED HEALING TO SELF";
            }
        }
        protected override void Apply(BattleCreature target, BattleCreature source = null, AbilityType abilityType = AbilityType.Undefined) {
            var heal = hp;
            ApplyHealDealMods(ref heal, source);
            ApplyHealRecvMods(ref heal, target);
            target.Heal(heal);
        }

        public static void ApplyHealDealMods(ref int heal, ICreature source) {
            //Affect healing by outgoing healing mods
            List<EffectHealMod> healDealMods = source.GetPassiveEffectsOfType<EffectHealMod>(EffectType.HealDealMod);
            for (int i = 0; i < healDealMods.Count; ++i) {
                healDealMods[i].AffectHeal(ref heal);
            }
        }

        public static void ApplyHealRecvMods(ref int heal, BattleCreature target) {
            //Affect healing by incoming healing mods
            List<EffectHealMod> healRecvMods = target.GetPassiveEffectsOfType<EffectHealMod>(EffectType.HealRecvMod);
            for (int i = 0; i < healRecvMods.Count; ++i) {
                healRecvMods[i].AffectHeal(ref heal);
            }
        }

        public override void EditEffectOnGUI() {
#if UNITY_EDITOR
            hp = EditorGUILayout.IntField("HP healed", hp);
            base.EditEffectOnGUI();
#endif
        }
        #endregion
    }
}
