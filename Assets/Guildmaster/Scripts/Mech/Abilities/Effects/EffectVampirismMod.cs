﻿namespace Assets.Guildmaster.Scripts.Mech.Abilities.Effects {
    using System;
    using Battle;
    using Attributes;
#if UNITY_EDITOR
    using UnityEditor;
#endif
    using Scripts.Battle;

    [Serializable]
    public class EffectVampirismMod : Effect {
        #region Fields
        protected DamageType damageType;
        protected AbilityType abilityType;
        protected float mod;//modificator *
        #endregion

        #region Accessors
        public DamageType DamageType { get { return damageType; } }
        public AbilityType AbilityType { get { return abilityType; } }
        public float Mod { get { return mod; } }
        #endregion


        #region Methods
        public EffectVampirismMod(DamageType damageType, AbilityType abilityType, float mod, EffectTarget target = EffectTarget.Default, TriggerType trigger = TriggerType.Use, int life = 0, Chance? chance = null) : base(EffectType.VampirismMod, target, trigger, life, chance.HasValue ? chance.Value : Chance.Always) {
            this.damageType = damageType;
            this.abilityType = abilityType;
            this.mod = mod;
        }
        public override Effect Clone() {
            return new EffectVampirismMod(damageType, abilityType, mod, target, trigger, life, chance);
        }

        protected override void Apply(BattleCreature target, BattleCreature source = null, AbilityType abilityType = AbilityType.Undefined) {
            throw new NotImplementedException();
        }
        public override void EditEffectOnGUI() {
#if UNITY_EDITOR
            damageType = (DamageType)EditorGUILayout.EnumPopup("Damage Type", (EditorDamageType)damageType);
            abilityType = (AbilityType)EditorGUILayout.EnumPopup("Ability Type", (EditorActiveAbilityType)abilityType);
            mod = EditorGUILayout.FloatField("Modificator(*)", mod);
            base.EditEffectOnGUI();
#endif
        }

        public EffectHeal CalculateHealing(int finalDamage, DamageType type, AbilityType abilityType) {
            int heal = 0;
            if((damageType & type) == type && (this.abilityType & abilityType) == abilityType) {
                heal = (int)(finalDamage * mod);
            }
            return heal > 0 ? new EffectHeal(heal, EffectTarget.Default) : null;
        }
        #endregion
    }
}
