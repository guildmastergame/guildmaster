﻿namespace Assets.Guildmaster.Scripts.Mech.Abilities.Effects {
    using System;
    using Battle;
#if UNITY_EDITOR
    using UnityEditor;
#endif
    using Scripts.Battle;
    using System.Text;
    using Entities.Creatures;

    [Serializable]
    public class EffectAbilityUse : Effect {
        #region Fields
        protected int abilityIdx;
        #endregion

        #region Accessors
        public int AbilityIdx { get { return abilityIdx; } }
        #endregion

        #region Methods
        public EffectAbilityUse(int abilityIdx, EffectTarget target = EffectTarget.Default, TriggerType trigger = TriggerType.Use, int life = 0, Chance? chance = null) : base(EffectType.AbilityUse, target, trigger, life, chance.HasValue ? chance.Value : Chance.Always) {
            this.abilityIdx = abilityIdx;
        }
        public override Effect Clone() {
            return new EffectAbilityUse(abilityIdx, target, trigger, life, chance);
        }

        public override string GetDescription(ICreature c, int abilityIdx, bool isActive) {
            var actives = (c.AbilitySlots[this.abilityIdx].Ability as AbilityActive).Actives;
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < actives.Count; ++i) {
                sb.Append(string.Format("{0}\n", actives[i].GetDescription(c, this.abilityIdx, true)));
            }
            return sb.ToString();
        }

        protected override void Apply(BattleCreature target, BattleCreature source = null, AbilityType abilityType = AbilityType.Undefined) {
            (source.AbilitySlots[abilityIdx].Ability as AbilityActive).ApplyToTarget(target.Tile, source.Tile);
        }

        public override void EditEffectOnGUI() {
#if UNITY_EDITOR
            abilityIdx = EditorGUILayout.IntField("Ability Index", abilityIdx);
            base.EditEffectOnGUI();
#endif
        }
        #endregion
    }
}
