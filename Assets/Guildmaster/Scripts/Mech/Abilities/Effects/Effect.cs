﻿namespace Assets.Guildmaster.Scripts.Mech.Abilities.Effects {
    using System;
    using Attributes;
#if UNITY_EDITOR
    using UnityEditor;
#endif
    using Scripts.Battle;
    using UnityEngine;
    using Entities.Creatures;

    [Serializable]
    public abstract class Effect {
        #region Fields
        protected EffectType type;
        protected EffectTarget target;
        protected TriggerType trigger;
        protected Chance chance;
        /// <summary>
        /// How much times this effect will affect it's target:
        /// -1 = constantly
        /// 1..* = equal number of affections from any trigger
        /// 0 - effect will be deleted after it will reach zero life.
        /// </summary>
        protected int life;
        [NonSerialized]
        protected BattleTile lastTarget;
        [NonSerialized]
        protected BattleTile lastSource;
        /// <summary>
        /// If this effect was applied on this round - it shouldn't decrease his life for first time
        /// </summary>
        [NonSerialized]
        protected bool isAppliedOnThisRound; 
        #endregion

        #region Accessors
        public EffectType Type { get { return type; } private set { type = value; } }
        public EffectTarget Target { get { return target; } set { target = value; } }
        public TriggerType Trigger { get { return trigger; } set { trigger = value; } }
        /// <summary>
        /// Temporary effect icon. by default it is null because not all effects can be temporary
        /// </summary>
        public virtual Sprite Image { get { return null; } }
        public int Life { get { return life; } set { life = value; } }
        public bool IsAppliedOnThisRound { get { return isAppliedOnThisRound; } internal set { isAppliedOnThisRound = value; } }
        #endregion

        #region Methods
        protected Effect(EffectType type, EffectTarget target, TriggerType trigger, int life, Chance chance) {
            this.type = type;
            this.target = target;
            this.trigger = trigger;
            this.life = life;
            this.chance = chance;
        }

        /// <param name="c">owner of ability which contains this effect</param>
        /// <param name="abilityIdx">index of ability of owner which this effect belongs to</param>
        /// <param name="isActive">true if this effect in Actives array in Ability</param>
        /// <returns>Text representation of effect for ability info</returns>
        public virtual string GetDescription(ICreature c, int abilityIdx, bool isActive) { 
            return "#no_description#";
        }

        public string GetDescriptionAsTemporary(ICreature c) {
            return "#no_description#";
        }

        /// <summary>
        /// Applies effect to given target from optional source
        /// </summary>
        /// <param name="target"></param>
        /// <param name="source"></param>
        /// <param name="abilityType"></param>
        /// <returns>true if this effect should end it's life</returns>
        public virtual bool Apply(BattleTile target, BattleTile source = null, AbilityType abilityType = AbilityType.Undefined) {
            lastSource = source;
            lastTarget = target;
            if (chance.Try()) {
                Apply(target == null ? null : target.Creature, source == null ? null : source.Creature, abilityType);
            }
            if (life > 0)
                --life;
            return life == 0;
        }
        protected abstract void Apply(BattleCreature target, BattleCreature source = null, AbilityType abilityType = AbilityType.Undefined);

        public abstract Effect Clone();
        public virtual void EditEffectOnGUI() {
#if UNITY_EDITOR
            target = (EffectTarget)EditorGUILayout.EnumPopup("Targets:", target);
            trigger = (TriggerType)EditorGUILayout.EnumMaskField("Triggers:", trigger);
            life = EditorGUILayout.IntField("life:", life);
            if(life < -1) {
                life = -1;
            }
            chance.Value = EditorGUILayout.FloatField("Chance:", chance.Value);
#endif
        }
        public static Effect CreateEffectOfType(EffectType type) {
            switch (type) {
                case EffectType.AbilityUse:
                    return new EffectAbilityUse(0);
                case EffectType.ActionMod:
                    return new EffectActionMod(0);
                case EffectType.Armor:
                    return new EffectArmor(new Armor());
                case EffectType.Bleed:
                    var eff = new EffectDamage(new Damage(1, DamageType.Bleed), Chance.Never, 0f, EffectTarget.Self, TriggerType.TurnStarted, 1, Chance.Always);
                    eff.Type = EffectType.Bleed;
                    return eff;
                case EffectType.Block:
                    return new EffectBlock(DamageType.All, AbilityType.QuickAttackAllTypes, EffectTarget.Default, 0, 0, new Chance(50f));
                case EffectType.Counterattack:
                    return new EffectCounterattack(AbilityType.QuickAttackMelee);
                case EffectType.CriticalChance:
                    return new EffectCriticalChance(AbilityType.QuickAttackAllTypes, DamageType.All, EffectTarget.Default, 0, 0, new Chance(20f));
                case EffectType.Damage:
                    return new EffectDamage(new Damage(), new Chance(0), 0f);
                case EffectType.DamageDealMod:
                    return new EffectDamageMod(EffectType.DamageDealMod, DamageType.All, AbilityType.QuickAttackAllTypes, 0, 1f);
                case EffectType.DamageRecvMod:
                    return new EffectDamageMod(EffectType.DamageRecvMod, DamageType.All, AbilityType.QuickAttackAllTypes, 0, 1f);
                case EffectType.Freeze:
                    return new EffectPassTurn(EffectType.Freeze, EffectTarget.Default, TriggerType.Use, 1);
                case EffectType.Heal:
                    return new EffectHeal(1);
                case EffectType.Poison:
                    var eff1 = new EffectDamage(new Damage(1, DamageType.Poison), Chance.Never, 0f, EffectTarget.Self, TriggerType.TurnStarted, 1, Chance.Always);
                    eff1.Type = EffectType.Poison;
                    return eff1;
                case EffectType.Removal:
                    return new EffectRemoval(EffectType.Bleed);
                case EffectType.Resistance:
                    return new EffectResistance(EffectType.Bleed, EffectTarget.Default, 0, 0, Chance.Always);
                case EffectType.Stun:
                    return new EffectPassTurn(EffectType.Stun, EffectTarget.Default, TriggerType.Use, 1);
                case EffectType.Summon:
                    return new EffectSummon(string.Empty, string.Empty);
                case EffectType.VampirismMod:
                    return new EffectVampirismMod(DamageType.Physical, AbilityType.AttackMelee, 1f);
                default:
                    return null;
            }
        }

        /// <summary>
        /// This method should be called only for lasting effects
        /// </summary>
        public virtual void RoundStarted(BattleCreature carrier) { }

        /// <summary>
        /// This method should be called only for lasting effects
        /// </summary>
        /// <param name="carrier"></param>
        /// <returns>true if this effect need to be removed from list of temporary effects</returns>
        public virtual bool RoundFinished(BattleCreature carrier) {
            if(!isAppliedOnThisRound && life > 0) {
                return --life == 0;
            }
            isAppliedOnThisRound = false;
            return false;
        }

        /// <summary>
        /// This method should be called only for lasting effects
        /// </summary>
        public virtual void TurnStarted(BattleCreature carrier) { }

        /// <summary>
        /// This method should be called only for lasting effects
        /// </summary>
        public virtual void TurnFinished(BattleCreature carrier) { }
        #endregion
    }
}
