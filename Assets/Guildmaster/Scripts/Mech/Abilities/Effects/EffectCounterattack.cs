﻿namespace Assets.Guildmaster.Scripts.Mech.Abilities.Effects {
    using System;
    using Battle;
#if UNITY_EDITOR
    using UnityEditor;
#endif
    using Scripts.Battle;

    [Serializable]
    public class EffectCounterattack : Effect {
        #region Fields
        protected AbilityType abilityType;
        #endregion

        #region Accessors
        public AbilityType AbilityType { get { return abilityType; } }
        #endregion

        #region Methods
        public EffectCounterattack(AbilityType abilityType, EffectTarget target = EffectTarget.Default, TriggerType trigger = TriggerType.Use, int life = 0, Chance? chance = null) : base(EffectType.Counterattack, target, trigger, life, chance.HasValue ? chance.Value : Chance.Always) {
            this.abilityType = abilityType;
        }
        public override Effect Clone() {
            return new EffectCounterattack(abilityType, target, trigger, life, chance);
        }

        protected override void Apply(BattleCreature target, BattleCreature source = null, AbilityType abilityType = AbilityType.Undefined) {
            throw new NotImplementedException();
        }

        public override void EditEffectOnGUI() {
#if UNITY_EDITOR
            abilityType = (AbilityType)EditorGUILayout.EnumPopup("Ability Type", (EditorActiveAbilityType)abilityType);
            base.EditEffectOnGUI();
#endif
        }
        #endregion
    }
}
