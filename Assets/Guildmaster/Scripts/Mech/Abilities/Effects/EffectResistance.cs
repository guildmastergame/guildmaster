﻿namespace Assets.Guildmaster.Scripts.Mech.Abilities.Effects {
    using System;
    using Battle;
#if UNITY_EDITOR
    using UnityEditor;
#endif
    using Scripts.Battle;
    using Entities.Creatures;

    [Serializable]
    public class EffectResistance : Effect {
        #region Fields
        protected EffectType resistType;
        #endregion

        #region Accessors
        public EffectType ResistType { get { return resistType; } }
        //public Chance ResistChance { get { return resistChance; } }
        #endregion

        #region Methods
        public EffectResistance(EffectType resistType, /*Chance resistChance,*/ EffectTarget target = EffectTarget.Default, TriggerType trigger = TriggerType.Use, int life = 0, Chance? chance = null) : base(EffectType.Resistance, target, trigger, life, chance.HasValue ? chance.Value : Chance.Always) {
            this.resistType = resistType;
            //this.resistChance = resistChance;
        }
        public override Effect Clone() {
            return new EffectResistance(resistType, /*resistChance,*/ target, trigger, life, chance);
        }
        public override string GetDescription(ICreature owner, int abilityIdx, bool isActive) {
            //TODO: again, this description is too lazy. WIP solution, should be fixed later
            if (chance == Chance.Always) {
                return string.Format("Immunity to {0}", resistType.ToString());
            }
            return string.Format("<color=#white>{0}</color> chance resist {1}", chance.ToString(), type);
        }
        protected override void Apply(BattleCreature target, BattleCreature source = null, AbilityType abilityType = AbilityType.Undefined) {
            throw new NotImplementedException();
        }
        public override void EditEffectOnGUI() {
#if UNITY_EDITOR
            resistType = (EffectType)EditorGUILayout.EnumPopup("Resist Type", resistType);
            //resistChance.Value = EditorGUILayout.FloatField("Resist Chance", resistChance.Value);
            base.EditEffectOnGUI();
#endif
        }

        public bool ResistEffect(EffectType effectType) {
            //if (resistType == EffectType.DEBUF_RESISTANCE) {
            //    return chance.Try() && (effectType == EffectType.FREEZE ||
            //        effectType == EffectType.BLEEDING || effectType == EffectType.POISON ||
            //        effectType == EffectType.WEAKNESS || effectType == EffectType.STUN);
            //}
            return effectType == resistType && chance.Try();
        }
        #endregion
    }
}
