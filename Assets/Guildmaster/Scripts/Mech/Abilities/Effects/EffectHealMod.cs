﻿namespace Assets.Guildmaster.Scripts.Mech.Abilities.Effects {
    using System;
    using Attributes;
#if UNITY_EDITOR
    using UnityEditor;
#endif
    using Scripts.Battle;

    [Serializable]
    public class EffectHealMod : Effect {
        #region Fields
        protected int app;//applicator +
        protected float mod;//modificator *
        #endregion

        #region Accessors
        public int App { get { return app; } }
        public float Mod { get { return mod; } }
        #endregion


        #region Methods
        public EffectHealMod(EffectType type, int app, float mod, EffectTarget target = EffectTarget.Default, TriggerType trigger = TriggerType.Use, int life = 0, Chance? chance = null) : base(type, target, trigger, life, chance.HasValue ? chance.Value : Chance.Always) {
            
            this.app = app;
            this.mod = mod;
        }
        public override Effect Clone() {
            return new EffectHealMod(type, app, mod, target, trigger, life, chance);
        }

        protected override void Apply(BattleCreature target, BattleCreature source = null, AbilityType abilityType = AbilityType.Undefined) {
            throw new NotImplementedException();
        }

        public override void EditEffectOnGUI() {
#if UNITY_EDITOR
            mod = EditorGUILayout.FloatField("Modificator(*)", mod);
            app = EditorGUILayout.IntField("Applicator(+)", app);
            base.EditEffectOnGUI();
#endif
        }

        public void AffectHeal(ref int heal) {
            var dmg = new Damage(heal, 0);
            dmg.AddApp(app);
            dmg.AddMod(mod);
            heal = dmg.Get().Current;
        }
        #endregion
    }
}
