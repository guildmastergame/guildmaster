﻿namespace Assets.Guildmaster.Scripts.Mech.Abilities.Effects {
    using System;
    using Battle;
    using Attributes;
#if UNITY_EDITOR
    using UnityEditor;
#endif
    using Scripts.Battle;
    using Entities.Creatures;

    [Serializable]
    public class EffectArmor : Effect {
        #region Fields
        protected Armor armor;
        #endregion

        #region Accessors
        public Armor Armor { get { return armor; } }

        #endregion

        #region Methods
        public EffectArmor(Armor armor, EffectTarget target = EffectTarget.Default, TriggerType trigger = TriggerType.Use, int life = 0, Chance? chance = null) : base(EffectType.Armor, target, trigger, life, chance.HasValue ? chance.Value : Chance.Always) {
            this.armor = armor;
        }

        public override Effect Clone() {
            return new EffectArmor(armor, target, trigger, life, chance);
        }

        public override string GetDescription(ICreature c, int abilityIdx, bool isActive) {
            var armorType = armor.Type == DamageType.All ? "Magic Protection" : armor.Type == DamageType.Physical ? "Armor" : "UnhandledArmorType";
            if(isActive) {
                return string.Format("<color=#white>+ {0}/{1}({2})</color> {3} to every target", armor.Value, armor.Cap, armor.Restoration, armorType);
            }
            return string.Format("<color=#white>{0}/{1}({2})</color> {3}", armor.Value, armor.Cap, armor.Restoration, armorType);
            //#EFFECT_TYPE# <color=#white>- #EFFECT_VALUE# </color>
        }

        protected override void Apply(BattleCreature target, BattleCreature source = null, AbilityType abilityType = AbilityType.Undefined) {
            var effect = Clone();
            effect.IsAppliedOnThisRound = true;
            target.ApplyEffect(effect);
            target.Tile.RefreshArmorAndProtection();
        }

        public void RestoreArmor() {
            armor.Restore();
        }

        public override void EditEffectOnGUI() {
#if UNITY_EDITOR
            armor.Type = (DamageType)EditorGUILayout.EnumPopup("Armor Type", (EditorDamageType)armor.Type);
            armor.Value = EditorGUILayout.IntField("Armor Value", armor.Value);
            armor.Cap = EditorGUILayout.IntField("Max Armor Value", armor.Cap);
            armor.Restoration = EditorGUILayout.IntField("Armor Restoration Per Round", armor.Restoration);
            base.EditEffectOnGUI();
#endif
        }

        public int AbsorbDamage(ref Damage dmg) {
            int lastArmorValue = armor.Value;
            armor.AbsorbDamage(ref dmg);
            return lastArmorValue - armor.Value;
        }
        #endregion
    }
}
