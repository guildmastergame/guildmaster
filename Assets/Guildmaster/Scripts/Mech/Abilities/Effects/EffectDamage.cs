﻿namespace Assets.Guildmaster.Scripts.Mech.Abilities.Effects {
    using System;
    using Battle;
    using Attributes;
#if UNITY_EDITOR
    using UnityEditor;
#endif
    using Scripts.Battle;
    using System.Collections.Generic;
    using UnityEngine;
    using Entities.Creatures;

    [Serializable]
    public class EffectDamage : Effect {
        #region Fields
        protected Damage damage;
        /// <summary>
        /// Base critical chance of attack by this kind of damage
        /// </summary>
        protected Chance criticalChance;
        /// <summary>
        /// How much amount of damage will be recovered as health: dmg * vampirism = hp
        /// </summary>
        protected float vampirism;
        #endregion

        #region Accessors
        public Damage Damage { get { return damage; } }
        public Chance CriticalChance { get { return criticalChance; } }
        public float Vampirism { get { return vampirism; } }
        /// <summary>
        /// Icon for damage over time (as temporary effect on crature)
        /// </summary>
        public override Sprite Image {
            get {
                switch (type) {
                    case EffectType.Poison:
                        return SpriteVault.GetSpriteIcon("icon_poison");
                    case EffectType.Bleed:
                        return SpriteVault.GetSpriteIcon("icon_bleeding_dark");
                    default:
                        return null;
                }
            }
        }
        #endregion

        #region Methods
        public EffectDamage(Damage damage, Chance criticalChance, float vampirism, EffectTarget target = EffectTarget.Default, TriggerType trigger = TriggerType.Use, int life = 0, Chance? chance = null) : base(EffectType.Damage, target, trigger, life, chance.HasValue ? chance.Value : Chance.Always) {
            this.damage = damage;
            this.criticalChance = criticalChance;
            this.vampirism = vampirism;
        }

        public static string DamageTypeToString(DamageType damageType) {
            switch (damageType) {
                case DamageType.Physical:
                    return "Physical";
                case DamageType.Fire:
                    return "Fire";
                case DamageType.Cold:
                    return "Cold";
                case DamageType.Poison:
                    return "Poison";
                case DamageType.Bleed:
                    return "Bleeding";
                case DamageType.Magic:
                    return "Magic";
                default:
                    return "UnhandledDamageType";
            }
        }

        public override string GetDescription(ICreature owner, int abilityIdx, bool isActive) {
            var dmg = damage.Get();
            ApplyDamageDealMods(ref dmg, owner, owner.AbilitySlots[abilityIdx].Ability.Type);
            if (isActive) {
                return string.Format(dmg.Current > dmg.Value ? "Deals <color=#00ff00ff>{0}</color> {1} Damage"
                    : dmg.Current < dmg.Value ? "Deals <color=#ff0000ff>{0}</color> {1} Damage"
                    : "Deals <color=#white>{0}</color> {1} Damage",
                    dmg.Current, DamageTypeToString(dmg.Type));
            } else {
                return "UNHANDLED DAMAGE TO SELF";
            }
        }

        protected override void Apply(BattleCreature target, BattleCreature source = null, AbilityType abilityType = AbilityType.Undefined) {

            Damage dmg = damage.Get();//Clone attribute to prevent changing the origin

            ApplyDamageDealMods(ref dmg, source, abilityType);
            //Affect damage by critical chance
            bool isCritical = CheckForCriticalStrike(ref dmg, target, source, abilityType);
            
            //Affect damage by damage receiving mods
            List<EffectDamageMod> dmgRecvMods = target.GetPassiveEffectsOfType<EffectDamageMod>(EffectType.DamageRecvMod);
            for (int i = 0; i < dmgRecvMods.Count; ++i) {
                dmgRecvMods[i].AffectDamage(ref dmg, abilityType);
            }
            //Decrease damage by armors (lasting effects first)
            var armorEfx = GetArmorList(target);
            int dmgAbsorbedByArmor = 0;
            for (int i = 0; i < armorEfx.Count; ++i) {
                dmgAbsorbedByArmor += armorEfx[i].AbsorbDamage(ref dmg);
            }
            if (dmgAbsorbedByArmor > 0) {
                //target.tile.ShowPopup(new TilePopupAttributes(dmgAbsorbedByArmor, dmg.Type, false, true, isCritical, false, false));
                target.Tile.RefreshArmorAndProtection();
            }
            
            int finalDamage = dmg.Current;
            Debug.Log(source.Name + " -> " + target.Name + string.Format(" for {0} {1} damage", finalDamage + dmgAbsorbedByArmor, dmg.Type.ToString()));
            var vampirismEfx = source.GetPassiveEffectsOfType<EffectVampirismMod>(EffectType.VampirismMod);
            for (int i = 0; i < vampirismEfx.Count; ++i) {
                EffectHeal healing = vampirismEfx[i].CalculateHealing(finalDamage, dmg.Type, abilityType);
                if (healing != null) {
                    healing.Apply(source.Tile, source.Tile, AbilityType.Vampirism);
                }
            }
            
            target.ApplyDamage(finalDamage, dmgAbsorbedByArmor, dmg.Type, isCritical); 
        }

        protected List<EffectArmor> GetArmorList(BattleCreature target) {
            List<EffectArmor> armorEffects = new List<EffectArmor>();
            List<EffectArmor> armors = target.GetPassiveEffectsOfType<EffectArmor>(EffectType.Armor);
            List<EffectArmor> tempArmors = target.GetTemporaryEffectsOfType<EffectArmor>(EffectType.Armor);
            //get damage type armor
            for (int i = 0; i < tempArmors.Count; ++i) {
                if (tempArmors[i].Armor.Type == damage.Type) {
                    armorEffects.Add(tempArmors[i]);
                }
            }
            for (int i = 0; i < armors.Count; ++i) {
                if (armors[i].Armor.Type == damage.Type) {
                    armorEffects.Add(armors[i]);
                }
            }
            //get magic protection armor (All damage type protection)
            for (int i = 0; i < tempArmors.Count; ++i) {
                if (tempArmors[i].Armor.Type == DamageType.All) {
                    armorEffects.Add(tempArmors[i]);
                }
            }
            for (int i = 0; i < armors.Count; ++i) {
                if (armors[i].Armor.Type == DamageType.All) {
                    armorEffects.Add(armors[i]);
                }
            }
            return armorEffects;
        }

        private bool CheckForCriticalStrike(ref Damage dmg, BattleCreature target, BattleCreature source, AbilityType abilityType) {
            List<EffectCriticalChance> critChances = source.GetPassiveEffectsOfType<EffectCriticalChance>(EffectType.CriticalChance);
            Chance addCritChance = Chance.Never;
            for (int i = 0; i < critChances.Count; ++i) {
                addCritChance += critChances[i].TryGetChance(dmg.Type, abilityType);
            }
            if ((CriticalChance + addCritChance).Try()) {
                dmg.AddMod(EffectCriticalChance.CriticalPowerMod);
                return true;
            }
            return false;
        }

        public static void ApplyDamageDealMods(ref Damage dmg, ICreature source, AbilityType actionType) {
            //Affect damage by damage dealing mods
            List<EffectDamageMod> dmgDealMods = source.GetPassiveEffectsOfType<EffectDamageMod>(EffectType.DamageDealMod);
            for (int i = 0; i < dmgDealMods.Count; ++i) {
                dmgDealMods[i].AffectDamage(ref dmg, actionType);
            }
            dmgDealMods = source.GetTemporaryEffectsOfType<EffectDamageMod>(EffectType.DamageDealMod);
            for (int i = 0; i < dmgDealMods.Count; ++i) {
                dmgDealMods[i].AffectDamage(ref dmg, actionType);
            }
        }


        public override Effect Clone() {
            return new EffectDamage(damage, criticalChance, vampirism, target, trigger, life, chance);
        }

        public override void EditEffectOnGUI() {
#if UNITY_EDITOR
            damage.Type = (DamageType)EditorGUILayout.EnumPopup("Damage Type", (EditorDamageType)damage.Type);
            damage.IsRandom = EditorGUILayout.Toggle("Random Damage", damage.IsRandom);
            if(damage.IsRandom) {
                damage.MinRandom = EditorGUILayout.IntField("Min Damage", damage.MinRandom);
                damage.MaxRandom = EditorGUILayout.IntField("Max Damage", damage.MaxRandom);
            } else {
                damage.Value = EditorGUILayout.IntField("Damage", damage.Value);
            }
            criticalChance.Value = EditorGUILayout.FloatField("Base Critical Hit Chance", criticalChance.Value);
            vampirism = EditorGUILayout.FloatField("Base Vampirism Mod", vampirism);
            base.EditEffectOnGUI();
#endif
        }
        #endregion
    }
}
