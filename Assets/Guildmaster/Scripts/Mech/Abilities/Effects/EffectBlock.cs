﻿namespace Assets.Guildmaster.Scripts.Mech.Abilities.Effects {
    using System;
    using Battle;
    using Attributes;
#if UNITY_EDITOR
    using UnityEditor;
#endif
    using Scripts.Battle;
    using Entities.Creatures;

    [Serializable]
    public class EffectBlock : Effect {
        #region Fields
        protected DamageType damageType;
        protected AbilityType abilityType;
        //protected Chance blockChance;
        #endregion

        #region Accessors
        public DamageType DamageType { get { return damageType; } }
        public AbilityType AbilityType { get { return abilityType; } }
        //public Chance BlockChance { get { return blockChance; } }
        #endregion

        #region Methods
        public EffectBlock(DamageType damageType, AbilityType abilityType, /*Chance blockChance,*/ EffectTarget target = EffectTarget.Default, TriggerType trigger = TriggerType.Use, int life = 0, Chance? chance = null) : base (EffectType.Block, target, trigger, life, chance.HasValue ? chance.Value : Chance.Always) {
            this.damageType = damageType;
            this.abilityType = abilityType;
            //this.blockChance = blockChance;
        }
        public override Effect Clone() {
            return new EffectBlock(damageType, abilityType,/* blockChance,*/ target, trigger, life, chance);
        }
        public override string GetDescription(ICreature c, int abilityIdx, bool isActive) {
            return string.Format("{0} chance to block {1} {2} damage", chance.ToString(), abilityType.ToString(), EffectDamage.DamageTypeToString(damageType));
        }
        protected override void Apply(BattleCreature target, BattleCreature source = null, AbilityType abilityType = AbilityType.Undefined) {
            throw new NotImplementedException();
        }

        public override void EditEffectOnGUI() {
#if UNITY_EDITOR
            damageType = (DamageType)EditorGUILayout.EnumPopup("Damage Type", (EditorDamageType)damageType);
            abilityType = (AbilityType)EditorGUILayout.EnumPopup("Ability Type", (EditorActiveAbilityType)abilityType);
            //blockChance.Value = EditorGUILayout.FloatField("Block Chance", blockChance.Value);
            base.EditEffectOnGUI();
#endif
        }

        public bool BlockAction(AbilityType abilityType) {
            if ((this.abilityType & abilityType) == abilityType && chance.Try()) {
                return true;
            }
            return false;
        }
        #endregion
    }
}
