﻿namespace Assets.Guildmaster.Scripts.Mech.Abilities {
    using Utility;
    using System;
    using System.Collections.Generic;
    using Effects;
    using UnityEngine;
    using Entities;

    [Serializable]
    public abstract class Ability : Entity {
        #region Fields
        protected AbilityType type;
        protected TargetType target;
        protected int cooldown;
        protected BattlePositionType battlePos = BattlePositionType.General;
        [NonSerialized]
        protected int curCooldown;
        [NonSerialized]
        protected bool usedInThisTurn;
        #endregion

        #region Accessors
        public int CurrentCooldown {
            get { return curCooldown; }
            set { curCooldown = value < 0 ? 0 : (value > cooldown ? cooldown : value); }
        }
        public int Cooldown {
            get { return cooldown; }
            set { cooldown = value < 0 ? 0 : value; }
        }
        public TargetType Target { get { return target; } set { target = value; } }
        public AbilityType Type { get { return type; } set { type = value; } }
        public BattlePositionType AvailablePos { get { return battlePos; } set { battlePos = value; } }
        public override Sprite Image { get { return SpriteVault.GetSpriteAbility(imageKey); } }
        public override EntityType EntityType {
            get {
                return EntityType.Ability;
            }
        }
        public bool Available(BattlePositionType curPosType) {
            return ((battlePos & curPosType) > 0) && type != AbilityType.Passive && (Cooldown == 0 || curCooldown == 0);
        }

        public abstract Ability Clone();

        /// <summary>
        /// If this ability was used in this turn we shouldn't decrease it's cooldown for a first time
        /// </summary>
        public bool UsedInThisTurn { get { return usedInThisTurn; } set { usedInThisTurn = value; } }
        #endregion

        #region Methods
        public abstract List<T> GetPassiveEffectsOfType<T>(EffectType type) where T : Effect;
        public void DecreaseCooldown(int rounds = 1) {
            if (!UsedInThisTurn) {
                curCooldown -= rounds;
                if (curCooldown < 0) {
                    curCooldown = 0;
                }
            } else {
                UsedInThisTurn = false;
            }
        }
        #endregion

        public Ability(Ability ability) : base(ability.name, ability.desc, ability.imageKey) {
            this.target = ability.target;
            this.cooldown = ability.cooldown;
            this.curCooldown = ability.curCooldown;
        }

        public Ability() : base(new TrString(), new TrString(), "") {
        } 

        public Ability(AbilityType type, TargetType target, int cooldown, TrString name = null, TrString desc = null, string imgKey = "", int curCooldown = 0, bool usedInThisTurn = false, BattlePositionType battlePos = BattlePositionType.General) : base(name, desc, imgKey) {
            this.type = type;
            this.target = target;
            this.cooldown = cooldown;
            this.curCooldown = curCooldown;
            this.usedInThisTurn = usedInThisTurn;
            this.battlePos = battlePos;
        }       
    }
}
