﻿namespace Assets.Guildmaster.Scripts.Mech.Items {
    using Abilities;
    using Utility;
    using System;
    using UnityEngine;
    using Entities;

    [Serializable]
    public class Item : Entity {
        #region Fields
        protected ItemType type;
        protected Ability ability;
        protected string itemKey;
        #endregion

        #region Accessors
        public ItemType Type { get { return type; } set { type = value; } }
        public Ability Ability { get { return ability; } }
        public override Sprite Image { get { return SpriteVault.GetSpriteItem(imageKey); } }
        public string ItemKey { get { return itemKey; } }
        public override EntityType EntityType {
            get {
                return EntityType.Item;
            }
        }
        #endregion

        #region Methods
        public Item(string itemKey) : base (new TrString(), new TrString(), string.Empty) {
            this.itemKey = itemKey;
            this.type = 0;
            this.ability = null;
        }
        public Item(string itemKey, ItemType type, Ability ability, TrString name, TrString desc, string imageKey) : base(name, desc, imageKey) {
            this.itemKey = itemKey;
            this.type = type;
            this.ability = ability;
        }
        public void AbilityChanged(Ability ability) {
            this.ability = ability;
        }

        public Item Clone() {
            return new Item(itemKey, type, ability.Clone(), name, desc, imageKey);
        }
        #endregion
    }
}
