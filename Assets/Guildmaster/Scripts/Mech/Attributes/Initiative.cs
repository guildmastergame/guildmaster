﻿namespace Assets.Guildmaster.Scripts.Mech.Attributes {
    using System;
    [Serializable]
    public struct Initiative {
        private float value;

        public float Value { get { return value; } set { this.value = value < 1 ? 1 : value; } }

        public Initiative(float value) {
            this.value = value;
        }

        public static bool operator <(Initiative a, Initiative b) {
            return a.value < b.value;
        }
        public static bool operator >(Initiative a, Initiative b) {
            return a.value > b.value;
        }
        public static bool operator <=(Initiative a, Initiative b) {
            return a.value <= b.value;
        }
        public static bool operator >=(Initiative a, Initiative b) {
            return a.value >= b.value;
        }
        public override string ToString() {
            return string.Format("{0}", Value);
        }
    }
}
