﻿namespace Assets.Guildmaster.Scripts.Mech.Attributes {
    public enum EditorDamageType {
        Physical = DamageType.Physical,
        Magic = DamageType.Magic,
        Fire = DamageType.Fire,
        Cold = DamageType.Cold,
        Poison = DamageType.Poison,
        Bleed = DamageType.Bleed,
        Physical_Magic_Fire_Cold = Physical | Magic | Fire | Cold,
        Poison_Bleed = Poison | Bleed
    }

    public enum DamageType {
        Physical = 1 << 0,
        Magic = 1 << 1,
        Fire = 1 << 2,
        Cold = 1 << 3,
        Poison = 1 << 4,
        Bleed = 1 << 5,
        All = Physical | Magic | Fire | Cold,
        Critical = 1 << 20
    }
}
