﻿namespace Assets.Guildmaster.Scripts.Mech.Attributes {
    using Utility;
    using System;
    [Serializable]
    public struct Health {
        #region Fields
        private int hp;
        private int maxHp;
        #endregion

        #region Accessors
        public int Hp {
            get { return hp; }
            set { hp = value > maxHp ? maxHp : value < 0 ? 0 : value; }
        }
        public int MaxHp {
            get { return maxHp; }
            set {
                var oldMaxHP = maxHp;
                maxHp = value < 1 ? 1 : value;
                if(oldMaxHP != maxHp) {
                    hp = maxHp;
                }
            }
        }
        #endregion

        #region Methods
        public void ApplyDamage(int hp) {
            this.hp -= hp;
            MathUtility.Clamp(ref this.hp, 0, MaxHp);
        }

        public void Heal(int hp) {
            this.hp += hp;
            MathUtility.Clamp(ref this.hp, 0, MaxHp);
        }

        public override string ToString() {
            return string.Format("{0}/{1}", hp, maxHp);
        }
        #endregion

        public Health(int maxHp) {
            this.maxHp = maxHp;
            this.hp = maxHp;
        }

        public Health(int maxHp, int hp) {
            this.maxHp = maxHp;
            this.hp = hp;
        }
    }
}
