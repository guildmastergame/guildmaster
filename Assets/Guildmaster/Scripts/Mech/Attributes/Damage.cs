﻿namespace Assets.Guildmaster.Scripts.Mech.Attributes {
    using System;
    using Scripts.Battle;
    using UnityEngine;
    using Abilities.Effects;
    using Abilities;
    using Entities.Creatures;

    [Serializable]
    public struct Damage {
        #region Fields
        private DamageType type;
        private int current;//Current damage to be displayed
        private int value;
        private int minRandom;
        private int maxRandom;
        private bool isRandom;
        #endregion

        #region Accessors
        public DamageType Type { get { return type; } set { type = value; } }
        public int Value { get { return value; } set { this.value = value > 1 ? value : 1; } }
        public int Current { get { return current; } }
        public int MinRandom {
            get { return minRandom; }
            set { minRandom = value <= maxRandom ? (value > 1 ? value : 1) : minRandom; }
        }
        public int MaxRandom {
            get { return maxRandom; }
            set { maxRandom = value >= minRandom ? (value > 1 ? value : 1) : maxRandom; }
        }
        public bool IsRandom {
            get { return isRandom; }
            set { isRandom = value; }
        }
        #endregion

        #region Methods
        public Damage(int value, DamageType type) {
            this.value = value;
            this.type = type;
            isRandom = false;
            minRandom = 0;
            maxRandom = 0;
            current = 0;
        }
        public Damage(int min, int max, DamageType type) {
            this.value = 0;
            this.type = type;
            this.minRandom = min;
            this.maxRandom = max;
            isRandom = true;
            current = 0;
        }
        public Damage Get() {
            if (IsRandom) {
                current = UnityEngine.Random.Range(minRandom, maxRandom + 1);
            } else {
                current = value;
            }
            return this;
        }

        public void AddMod(float mod) {
            current = Mathf.RoundToInt(current * mod);
            DamageBoundCheck();
        }

        public void AddApp(int app) {
            current += app;
            DamageBoundCheck();
        }
        private void DamageBoundCheck() {
            if (current < 0) {
                current = 0;
            }
        }

        public string GetAsColoredString(ICreature owner, AbilityType abilityType) {
            var dmg = Get();
            int baseDamage = dmg.Current;
            EffectDamage.ApplyDamageDealMods(ref dmg, owner, abilityType);
            return string.Format(dmg.Current > baseDamage ? "<color=#00ff00ff>{0}</color>" : dmg.Current < baseDamage ? "<color=#ff0000ff>{0}</color>" : "{0}", dmg.Current);
        }
        #endregion
    }
}
