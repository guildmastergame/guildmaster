﻿namespace Assets.Guildmaster.Scripts.Mech.Attributes {
    using System;
    using Utility;
    [Serializable]
    public struct Armor {
        #region Fields
        private DamageType type;
        private int value;
        /// <summary>
        /// Max value of the armor
        /// </summary>
        private int cap;
        /// <summary>
        /// How much armor will be restored every round
        /// </summary>
        private int restoration;
        #endregion

        #region Accessors
        public DamageType Type { get { return type; } set { type = value; } }
        public int Value { get { return value; } set { this.value = value > 0 ? (value < cap ? value : cap) : 0; } }
        public int Cap { get { return cap; } set { cap = value > 1 ? value : 1; } }
        public int Restoration { get { return restoration; } set { restoration = value; } }
        #endregion

        #region Methods
        public Armor(int value, DamageType damageType) {
            this.value = value;
            this.type = damageType;
            this.cap = value;
            this.restoration = 0;
        }
        public Armor(int value, int restoration, int cap, DamageType damageType) {
            this.value = value;
            this.restoration = restoration;
            this.cap = cap;
            this.type = damageType;
        }

        public void Restore() {
            value += restoration;
            MathUtility.Clamp(ref value, 0, cap);
        }

        public void AbsorbDamage(ref Damage damage) {
            if((damage.Type & Type) != damage.Type) {
                return;
            }
            int dmg = damage.Current;
            if (value <= dmg) {
                damage.AddApp(-value);
                value = 0;
            } else {
                value -= dmg;
                damage.AddApp(-dmg);
            }
        }
        #endregion
    }
}
