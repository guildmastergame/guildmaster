﻿namespace Assets.Guildmaster.Scripts.Mech.Entities {
    using Utility;
    using System;
    using UnityEngine;

    [Serializable]
    public abstract class Entity {
        #region Fields
        protected TrString name;
        protected TrString desc;
        protected string imageKey;
        #endregion

        #region Accessors
        public TrString Name { get { return name; } }
        public string NameKey { get { return name.Key; } set { name.Key = value; } }
        public TrString Desc { get { return desc; } }
        public string DescKey { get { return desc.Key; } set { desc.Key = value; } }
        public virtual Sprite Image { get { return null; } }
        public virtual string ImageKey { get { return imageKey; } set { imageKey = value; } }
        public virtual EntityType EntityType { get { return EntityType.Undefined; } }
        #endregion

        public Entity(TrString name, TrString desc, string imageKey) {
            this.name = name;
            this.desc = desc;
            this.imageKey = imageKey;
        }
    }
}
