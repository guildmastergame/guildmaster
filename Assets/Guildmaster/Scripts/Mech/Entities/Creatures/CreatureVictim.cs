﻿namespace Assets.Guildmaster.Scripts.Mech {
    using System;
    [Serializable]
    public struct CreatureVictim {
        private Creature creature;
        private int count;

        public Creature Creature { get { return creature; } }
        public int Count { get { return count; } }

        public CreatureVictim(Creature creature, int count) {
            this.creature = creature;
            this.count = count;
        }
    }
}
