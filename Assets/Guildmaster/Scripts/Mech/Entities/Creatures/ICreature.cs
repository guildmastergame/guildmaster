﻿namespace Assets.Guildmaster.Scripts.Mech.Entities.Creatures {
    using Abilities.Effects;
    using Abilities;
    using System.Collections.Generic;

    public interface ICreature {
        List<T> GetPassiveEffectsOfType<T>(EffectType type) where T : Effect;
        List<T> GetTemporaryEffectsOfType<T>(EffectType type) where T : Effect;
        List<AbilitySlot> AbilitySlots { get; }
    }
}
