﻿namespace Assets.Guildmaster.Scripts.Mech {
    using Abilities;
    using Abilities.Effects;
    using Entities.Creatures;
    using Attributes;
    using Entities;
    using System;
    using System.Collections.Generic;
    using UnityEngine;
    using Utility;

    [Serializable]
    public class Creature : Entity, ICreature {
        #region Fields
        protected Initiative initiative;
        protected Health health;
        protected List<CreatureTag> tags;
        protected List<AbilitySlot> slots;
        /// <summary>
        /// How much actions this creature have by default.
        /// By default any creature has one action every turn
        /// </summary>
        protected int actions = 1;
        /// <summary>
        /// Lasting effects which will be on creature from start of the battle
        /// </summary>
        protected List<Effect> startEffects; 
        protected string creatureKey;
        [NonSerialized]
        private Sprite portraitCached = null;
        [NonSerialized]
        private Texture2D portraitCachedTex = null;
        #endregion
        #region Accessors
        public string CreatureKey { get { return creatureKey; } set { creatureKey = value ?? string.Empty; } }
        public List<CreatureTag> Tags { get { return tags; } }
        public Health Health { get { return health; } set { health = value; } }
        public Initiative Ini { get { return initiative; } set { initiative = value; } }
        public List<AbilitySlot> AbilitySlots { get { return slots; } }
        public int Actions { get { return actions; } set { actions = value > 0 ? value : 0; } }
        public List<Effect> StartEffects { get { return startEffects; } }
        public override string ImageKey {
            get {
                return base.ImageKey;
            }
            set {
                portraitCached = null;
                base.ImageKey = value;
            }
        }
        public override EntityType EntityType {
            get {
                return EntityType.Creature;
            }
        }
        public Sprite ImagePortrait {
            get {
                if(portraitCached == null) {
                    portraitCached = SpriteVault.GetSpriteCreature(string.Format("{0}_portrait", ImageKey));
                }
                return portraitCached;
            }
        }
        public Texture2D TexturePortrait {
            get {
                if(portraitCachedTex == null) {
                    portraitCachedTex = SpriteUtility.GetTexture(ImagePortrait);
                }
                return portraitCachedTex;
            }
        }
        #endregion
        #region Methods
        public Creature Clone() {
            var tags = new List<CreatureTag>(this.tags);
            var startEfx = new List<Effect>(startEffects != null ? startEffects.Count : 0);
            if (startEffects != null) {
                for (int i = 0; i < startEffects.Count; ++i) {
                    startEfx.Add(startEffects[i].Clone() as Effect);
                }
            }
            var abSlots = new List<AbilitySlot>(this.slots.Count);
            for (int i = 0; i < this.slots.Count; ++i) {
                abSlots.Add(this.slots[i].Clone());
            }
            return new Creature(creatureKey, health, initiative, tags, startEffects, abSlots, name, desc, imageKey);
        }
        public int GetMainActionIdx() {
            for (int i = 0; i < slots.Count; ++i) {
                AbilityActive action = slots[i].Ability as AbilityActive;
                if (action == null) {
                    continue;
                } else {
                    return i;
                }
            }
            return -1;
        }
        public List<T> GetPassiveEffectsOfType<T>(EffectType type) where T : Effect {
            List<T> efx = new List<T>();
            for (int i = 0; i < slots.Count; ++i) {
                var abEFx = slots[i].Ability.GetPassiveEffectsOfType<T>(type);
                for (int j = 0; j < abEFx.Count; ++j) {
                    efx.Add(abEFx[j]);
                }
            }
            return efx;
        }
        public List<T> GetTemporaryEffectsOfType<T>(EffectType type) where T : Effect {
            List<T> efx = new List<T>();
            for (int i = 0; i < startEffects.Count; ++i) {
                if (startEffects[i].Type == type) {
                    efx.Add(startEffects[i] as T);
                }
            }
            return efx;
        }
        public int GetCurrentArmor(DamageType armorType = DamageType.Physical) {
            int armor = 0;
            List<EffectArmor> armors = GetPassiveEffectsOfType<EffectArmor>(EffectType.Armor);
            for (int i = 0; i < armors.Count; ++i) {
                if (armors[i].Armor.Type == armorType) {
                    armor += armors[i].Armor.Value;
                }
            }
            return armor;
        }
        public Creature(string creatureKey) : base (new TrString(), new TrString(), string.Empty) {
            this.creatureKey = creatureKey;
            this.health = new Health(1);
            this.initiative = new Initiative(1);
            this.tags = new List<CreatureTag>();
            this.startEffects = new List<Effect>();
            this.slots = new List<AbilitySlot>();
        }
        public Creature(string creatureKey, Health health, Initiative initiative, List<CreatureTag> tags, List<Effect> startEffects, List<AbilitySlot> slots, TrString name, TrString desc, string imageKey) : base(name, desc, imageKey) {
            this.creatureKey = creatureKey;
            this.health = health;
            this.initiative = initiative;
            this.tags = tags;
            this.startEffects = startEffects;
            this.slots = slots;
        }
        #endregion
    }
}
