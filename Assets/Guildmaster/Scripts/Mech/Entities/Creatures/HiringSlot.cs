﻿namespace Assets.Guildmaster.Scripts.Mech.Entities.Creatures {
    /// <summary>
    /// Contains data for hiring creatures of some kind
    /// </summary>
    public class HiringSlot {
        /// <summary>
        /// price of the each single creature
        /// </summary>
        protected int price;
        /// <summary>
        /// Number of creatures available for hire
        /// </summary>
        protected int count;
        /// <summary>
        /// Creature descritption
        /// </summary>
        protected Creature creature;

        public int Price { get { return price; } }
        public int Count { get { return count; } }
        public Creature Creature { get { return creature; } }

        public HiringSlot(Creature creature, int price, int count = 1) {
            this.creature = creature;
            this.price = price;
            this.count = count;
        }
    }
}
