﻿namespace Assets.Guildmaster.Scripts.Mech {
    public enum CreatureTag {
        Human,
        Undead,
        Dwarf,
        Warrior,
        Archer,
        Mage
    }
}
