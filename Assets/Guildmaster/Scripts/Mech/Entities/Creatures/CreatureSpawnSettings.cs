﻿namespace Assets.Guildmaster.Scripts.Mech {
    using System;
    [Serializable]
    public struct CreatureSpawnSettings {
        private Creature creature;
        private int minCount;
        private int maxCount;
        private BattlePositionType position;

        public int MinCount { get { return minCount; } }

        public CreatureSpawnSettings(Creature creature, int minCount, int maxCount, BattlePositionType position) {
            this.creature = creature;
            this.minCount = minCount;
            this.maxCount = maxCount;
            this.position = position;
        }

        public void Spawn(ref Party party) {

        }
    }
}
