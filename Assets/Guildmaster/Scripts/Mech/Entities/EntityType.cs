﻿namespace Assets.Guildmaster.Scripts.Mech.Entities {
    public enum EntityType {
        Undefined = 0,
        Creature = 1 << 1,
        //Hero = 1 << 2,
        //Monster = 1 << 3,
        //Creatures = Creature | Hero | Monster,
        Quest = 1 << 8,
        Ability = 1 << 9,
        Item = 1 << 10,
    }
}
