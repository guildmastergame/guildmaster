﻿namespace Assets.Guildmaster.Scripts.Mech.Containers {
    using UnityEngine;
    using Entities;

    /// <summary>
    /// Honestly, it's just a workaround, to save time and don't do real container grouping logic.
    /// Just add an enum field if you need a new group.
    /// </summary>
    public enum ContainerGroup {
        Default = 0,
        GuildMembers,
        PartyMembers,
        PartyLeader,
    }

    /// <summary>
    /// Used as an interface for inventories and squad containers
    /// </summary>
    public abstract class Container {
        public delegate void OnChangeCallback(int i, Container c);
        protected Entity entity;
        protected EntityType type;
        protected int idx; //Index for container content update (UI)
        protected OnChangeCallback onChange;
        protected ContainerGroup group;
        protected bool groupLock; //if true - this container are allowed to swap only in it's group
        public Entity Entity { get { return entity; } }
        public EntityType Type { get { return type; } set { type = value; } }
        public bool IsEmpty { get { return entity == null; } }
        public abstract Sprite Image { get; }
        public OnChangeCallback OnChange { get { return onChange; } }
        public bool GroupLock { get { return groupLock; } set { groupLock = value; } }

        protected Container(EntityType containerType, int idx, OnChangeCallback onChange, ContainerGroup group = ContainerGroup.Default, bool groupLock = false) {
            this.idx = idx;
            this.type = containerType;
            this.onChange = onChange;
            this.group = group;
            this.groupLock = groupLock;
        }

        public void SetCallback(int idx, OnChangeCallback onChange) {
            this.idx = idx;
            this.onChange = onChange;
        }

        /// <returns>True if items swaped correctly</returns>
        public bool Swap(Container from) {
            if (((!from.groupLock && !groupLock) || (group == from.group)) &&
                (from.Entity == null || (type & from.Entity.EntityType) == from.Entity.EntityType) &&
                (entity == null || (from.Type & entity.EntityType) == entity.EntityType)) {
                var tEntity = entity;
                var tGroupLock = this.groupLock;
                this.groupLock = from.GroupLock;
                from.groupLock = tGroupLock;
                entity = null;
                Put(from.Entity);
                from.entity = null;
                from.Put(tEntity);
                return true;
            }
            return false;
        }

        /// <returns>True if item put correctly</returns>
        public bool Put(Entity entity) {
            if (entity == null) {
                Clear();
                return true;
            }
            if ((type & entity.EntityType) == entity.EntityType && this.entity == null) {
                this.entity = entity;
                if (OnChange != null) {
                    OnChange(idx, this);
                }
                return true;
            }
            return false;
        }

        public void Clear() {
            if (OnChange != null) {
                OnChange(idx, this);
            }
            entity = null;
        }
    }
}
