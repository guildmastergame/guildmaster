﻿namespace Assets.Guildmaster.Scripts.Mech.Containers {
    using UnityEngine;
    using Entities;

    public class CreatureContainer : Container {
        public Creature Creature { get { return entity as Creature; } }

        public override Sprite Image { get { return entity == null ? null : Creature.ImagePortrait; } }

        public CreatureContainer(int idx, OnChangeCallback callback, Creature creature = null, EntityType type = EntityType.Creature, ContainerGroup group = ContainerGroup.Default, bool groupLock = false)
            : base(type, idx, callback, group, groupLock) {
            this.entity = creature;
        }
    }
}
