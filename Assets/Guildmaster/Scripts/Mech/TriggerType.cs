﻿namespace Assets.Guildmaster.Scripts.Mech {
    public enum TriggerType {
        TurnStarted = 1 << 0,
        TurnFinished = 1 << 1,
        RoundStarted = 1 << 2,
        RoundEnded = 1 << 3,
        Use = 1 << 4, //This effect will affect target only after ability use
    }
}
