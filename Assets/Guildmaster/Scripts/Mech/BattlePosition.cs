﻿namespace Assets.Guildmaster.Scripts.Mech {
    public enum BattlePositionType {
        Undefined = 0,
        Front = 1,
        Rear = 2,
        General = Front | Rear
    }

    public struct BattlePosition {
        public int x;
        public int y;
        public BattlePositionType Type { get { return y == 0 || y == 3 ? BattlePositionType.Rear : BattlePositionType.Front; } }
        public bool IsPlayersPart { get { return y >= Party.MaxHeight; } }
        public BattlePosition(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public static bool operator ==(BattlePosition a, BattlePosition b) {
            return a.x == b.x && a.y == b.y;
        }
        public static bool operator !=(BattlePosition a, BattlePosition b) {
            return a.x != b.x || a.y != b.y;
        }
        public override bool Equals(object obj) {
            try {
                return (bool)(this == (BattlePosition)obj);
            } catch {
                return false;
            }
        }
        public override int GetHashCode() {
            return x^y;
        }
    }
}
