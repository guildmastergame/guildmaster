﻿namespace Assets.Guildmaster.Scripts.Mech.Map {
    public enum MapMarkerType {
        Undefined,
        QuestNew,
        QuestFight,
        QuestProgress,
        Boss
    }
}
