﻿namespace Assets.Guildmaster.Scripts.Mech.Map {
    using Quests;
    using UnityEngine;
    public class MapMarkerData {
        /// <summary>
        /// Absolute map position x
        /// </summary>
        protected int x;
        /// <summary>
        /// Absolute map position y
        /// </summary>
        protected int y;
        protected MapMarkerType type;
        private Quest currentNode;

        public Vector2 Position { get { return new Vector2(x, y); } }
        public MapMarkerType Type { get { return type; } }
        public Sprite Icon { get { return SpriteVault.GetSpriteWorld(string.Format("map_marker_{0}", type.ToString())) ?? SpriteVault.GetSpriteIcon("icon_ability_frame"); } } //icon_ability_frame is temporal error-handling sprite.

        public MapMarkerData(int x, int y, MapMarkerType type) {
            this.x = x;
            this.y = y;
            this.type = type;
        }

        public MapMarkerData(QuestTree qTree, Vector2 mapSize) {
            Quest q = null;
            if(qTree.Status == QuestTreeStatus.Ready) {
                q = qTree.CurrentStart;
                this.type = MapMarkerType.QuestNew;
            } else if(qTree.Status == QuestTreeStatus.InProgress) {
                q = qTree.CurrentNode;
                this.type = MapMarkerType.QuestProgress;
            }
            this.x = (int)q.MapMarkerPosition.x - (int)mapSize.x / 2;
            this.y = (int)q.MapMarkerPosition.y - (int)mapSize.y / 2;
        }
    }
}
