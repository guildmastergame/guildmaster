﻿namespace Assets.Guildmaster.Scripts.Mech {
    using Containers;
    using System;
    using System.Collections.Generic;
    using System.Xml.Serialization;
    using System.Linq;

    public class PartyCreatureContainer {
        [XmlAttribute("Key")]
        public string creatureKey;
        [XmlAttribute("PosX")]
        public int partyPosX;
        [XmlAttribute("PosY")]
        public int partyPosY;

        public PartyCreatureContainer() { }
        public PartyCreatureContainer(string creatureKey, int partyPosX, int partyPosY) {
            this.creatureKey = creatureKey;
            this.partyPosX = partyPosX;
            this.partyPosY = partyPosY;
        }
    }

    public class PartyContainer {
        [XmlArray("Creatures"), XmlArrayItem("Creature")]
        public List<PartyCreatureContainer> creatures;

        public PartyContainer() { }
        public PartyContainer(Party p) {
            creatures = new List<PartyCreatureContainer>();
            for(int i = 0; i < Party.MaxHeight; ++i) {
                for(int j = 0; j < Party.MaxWidth; ++j) {
                    var c = p.GetCreature(j, i);
                    if(c != null) {
                        creatures.Add(new PartyCreatureContainer(c.CreatureKey, j, i));
                    }
                }
            }
        }
    }

    /// <summary>
    /// Contains only creature keys. 
    /// Used as a blueprint for real party spawning.
    /// </summary>
    [Serializable]
    public class PartyDescription {
        #region Fields
        protected string[,] creatureKeys = new string[Party.MaxHeight, Party.MaxWidth];
        #endregion
        #region Accessors
        #endregion
        #region Methods
        public Party GetParty() {
            var party = new Party();
            for(int i = 0; i < Party.MaxHeight; ++i) {
                for(int j = 0; j < Party.MaxWidth; ++j) {
                    if (creatureKeys[i, j] != null) {
                        party.SetCreature(CreatureVault.Get(creatureKeys[i, j]), j, i);
                    }
                }
            }
            return party;
        }
        public PartyDescription() { }
        public PartyDescription(Party party) {
            for(int i = 0; i < Party.MaxHeight; ++i) {
                for(int j = 0; j < Party.MaxWidth; ++j) {
                    var creature = party.GetCreature(j, i);
                    creatureKeys[i, j] = creature == null ? null : creature.CreatureKey;
                }
            }
        }

        public PartyDescription(PartyContainer pc) {
            creatureKeys = new string[Party.MaxHeight, Party.MaxWidth];
            for (int i = 0; i < pc.creatures.Count; ++i) {
                var c = pc.creatures[i];
                creatureKeys[c.partyPosY, c.partyPosX] = c.creatureKey;
            }
        }

        #endregion
    }

    [Serializable]
    public class Party {
        #region Fields
        public const int MaxWidth = 5;
        public const int MaxHeight = 2;
        protected CreatureContainer[,] creatures;
        #endregion

        #region Accessors
        
        #endregion

        #region Methods
        public Party() {
            creatures = new CreatureContainer[MaxHeight, MaxWidth];
            for(int i = 0; i < MaxHeight; ++i) {
                int offset = i * MaxHeight;
                for(int j = 0; j < MaxWidth; ++j) {
                    creatures[i, j] = new CreatureContainer(offset + j, null, null, Entities.EntityType.Creature, ContainerGroup.PartyMembers, false);
                }
            }
        }

        public bool CheckIsEmpty() {
            for (int i = 0; i < MaxHeight; ++i) {
                for (int j = 0; j < MaxWidth; ++j) {
                    if (creatures[i, j].Creature != null) {
                        return false;
                    }
                }
            }
            return true;
        }

        public ICollection<CreatureTag> GetPartyTags() {
            HashSet<CreatureTag> tagSet = new HashSet<CreatureTag>();
            for(int i = 0; i < MaxHeight; ++i) {
                int offset = i * MaxWidth;
                for(int j = 0; j < MaxWidth; ++j) {
                    if(creatures[i, j].Creature != null) {
                        var tags = creatures[i, j].Creature.Tags;
                        for(int k = 0; k < tags.Count; ++k) {
                            tagSet.Add(tags[k]);
                        }
                    }
                }
            }
            return tagSet;
        }

        public void InitializeCallbacks(Container.OnChangeCallback callback) {
            for (int i = 0; i < MaxHeight; ++i) {
                int offset = i * MaxWidth;
                for (int j = 0; j < MaxWidth; ++j) {
                    creatures[i, j].SetCallback(offset + j, callback);
                }
            }
        }

        public void SetCreature(Creature creature, int partyPosX, int partyPosY) {
            creatures[partyPosY, partyPosX].Put(creature);
        }

        public Creature GetCreature(int partyPosX, int partyPosY) {
            return creatures[partyPosY, partyPosX].Creature;
        }

        public CreatureContainer GetCreatureContainer(int partyPosX, int partyPosY) {
            return creatures[partyPosY, partyPosX];
        }
        #endregion
    }
}
