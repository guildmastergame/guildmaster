﻿namespace Assets.Guildmaster.Scripts.Mech {
    using System;
    using System.Text;

    [Serializable]
    public class Date {
        #region Fields
        public const int YEAR_OFFSET = 500;
        public static readonly string[] MONTH_NAMES = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
        public static readonly string[] WEEKDAY_NAMES = {"Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"};
        private static GameData GameData;
        private int dateValue;
        #endregion
        #region Accessors
        public int DateValue { get { return dateValue; } set { dateValue = value > 0 ? value : 0; } }
        public int Year { get { return dateValue / 360 + YEAR_OFFSET; } }
        public int Month { get { return (dateValue % 360) / 30 + 1; } }
        public int Day { get { return dateValue % 30 + 1; } }
        public int Weekday { get { return dateValue % 7; } }
        public string MonthName { get { return MONTH_NAMES[Month - 1]; } }
        public string WeekdayName { get { return WEEKDAY_NAMES[Weekday]; } }
        public static Date Now { get { return GameData.date; } }
        #endregion

        #region Methods
        //How much days passed from day to day. Can give negative values if 'from' day comes after 'to'
        public static int DifferenceDays(Date from, Date to) {
            return to.DateValue - from.DateValue;
        }
        public static void SetGameData(GameData gd) {
            GameData = gd;
        }
        #endregion

        public Date() {
            this.dateValue = 0;
        }

        public Date(int dateValue) {
            DateValue = dateValue;
        }

        public Date(Date date) {
            this.dateValue = date.dateValue;
        }

        public void AddDays(int daysCount) {
            DateValue += daysCount;
        }

        public override string ToString() {
            StringBuilder sb = new StringBuilder();
            sb.Append(Day);
            sb.Append(" of ");
            sb.Append(MonthName);
            return sb.ToString();
        }
    }
}
