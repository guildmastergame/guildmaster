﻿namespace Assets.Guildmaster.Scripts.Mech.Quests {
    using System;
    [Serializable]
    public enum QuestTreeStatus {
        Undefined = 0,
        Refused,
        InProgress,
        Succed,
        Failed,
        Ready
    }
}
