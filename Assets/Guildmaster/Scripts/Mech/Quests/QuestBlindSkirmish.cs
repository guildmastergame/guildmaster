﻿namespace Assets.Guildmaster.Scripts.Mech.Quests {
    using Utility;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    [Serializable]
    public class QuestBlindSkirmish : QuestBattle {
        #region Fields
        protected List<CreatureSpawnSettings> creatures;
        #endregion

        #region Accessors
        #endregion

        #region Methods
        public override void Update(ref Party party) {
            throw new NotImplementedException();
        }
        public override Party GenerateFoes() {
            throw new NotImplementedException();
        }
        #endregion

        public QuestBlindSkirmish(Quest q) : base(q.Name, q.Desc, QuestType.BlindSkirmish, q.MapMarkerPosition, q.NextIdxSuccess, q.NextIdxFail, q.ImageKey, q.Status) {
            creatures = new List<CreatureSpawnSettings>();
        }

        public QuestBlindSkirmish(TrString name, TrString desc, List<CreatureSpawnSettings> creatures, Vector2 mapMarkerPosition, int nextNodeIdxSuccess, int nextNodeIdxFail, string imageKey = null, QuestStatus status = QuestStatus.Available) : base(name, desc, QuestType.BlindSkirmish, mapMarkerPosition, nextNodeIdxSuccess, nextNodeIdxFail, imageKey, status) {
            this.creatures = creatures;
        }
    }
}
