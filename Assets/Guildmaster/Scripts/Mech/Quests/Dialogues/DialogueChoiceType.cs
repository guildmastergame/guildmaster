﻿namespace Assets.Guildmaster.Scripts.Mech.Quests.Dialogues {
    public enum DialogueChoiceType {
        Undefined = 0,
        Info,
        Check,
        Chance
    }
}
