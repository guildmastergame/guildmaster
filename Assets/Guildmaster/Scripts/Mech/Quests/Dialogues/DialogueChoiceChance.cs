﻿namespace Assets.Guildmaster.Scripts.Mech.Quests.Dialogues {
    using Utility;
    using System;
    using System.Collections.Generic;
    using System.Xml.Serialization;

    public class DialogueChoiceChanceDataContainer {
        [XmlAttribute("nextIdx")]
        public int nextNodeIdx;
        [XmlAttribute("chance")]
        public int chance;

        public DialogueChoiceChanceDataContainer() { }
        public DialogueChoiceChanceDataContainer(int nextNodeIdx, int chance) {
            this.nextNodeIdx = nextNodeIdx;
            this.chance = chance;
        }
    }

    public class DialogueChoiceChanceContainer : DialogueChoiceContainer {
        [XmlArray("NextNodes"), XmlArrayItem("Data")]
        public List<DialogueChoiceChanceDataContainer> data = new List<DialogueChoiceChanceDataContainer>();

        public override DialogueChoice Unpack() {
            var nextNodeIndexes = new List<int>(data.Count);
            var chanceValues = new List<int>(data.Count);
            for(int i = 0; i < data.Count; ++i) {
                nextNodeIndexes.Add(data[i].nextNodeIdx);
                chanceValues.Add(data[i].chance);
            }
            return new DialogueChoiceChance(new TrString(textKey), nextNodeIndexes, chanceValues);
        }

        public DialogueChoiceChanceContainer() { }
        public DialogueChoiceChanceContainer(DialogueChoiceChance choice) : base(choice) {
            data = new List<DialogueChoiceChanceDataContainer>(choice.NextNodeIndexes.Count);
        }
    }

    [Serializable]
    public class DialogueChoiceChance : DialogueChoice {
        #region Fields
        protected List<int> nextNodeIndexes;
        protected List<int> chanceValues;
        private int sumOfChances;
        #endregion

        #region Accessors
        public List<int> NextNodeIndexes { get { return nextNodeIndexes; } }
        public List<int> ChanceValues { get { return chanceValues; } }
        #endregion

        #region Methods
        public override int Select(Party party) {
            int randomValue = UnityEngine.Random.Range(0, sumOfChances);
            int curIndexValue = nextNodeIndexes[0];
            for(int i = 1; i < nextNodeIndexes.Count; ++i) {
                if(randomValue < curIndexValue) {
                    return nextNodeIndexes[i - 1];
                }
                curIndexValue += chanceValues[i];
            }
            return nextNodeIndexes[nextNodeIndexes.Count - 1];
        }

        public override void NodeDeleted(int nodeIdx) {
            throw new NotImplementedException();
        }

        public override DialogueChoiceContainer Pack() {
            throw new NotImplementedException();
        }
        #endregion

        public DialogueChoiceChance() : base(new TrString(), DialogueChoiceType.Chance) {
            this.nextNodeIndexes = new List<int>();
            this.chanceValues = new List<int>();
            sumOfChances = 0;
        }


        public DialogueChoiceChance(TrString text, List<int> nextNodeIndexes, List<int> chanceValues) : base(text, DialogueChoiceType.Chance) {
            this.nextNodeIndexes = nextNodeIndexes;
            this.chanceValues = chanceValues;
            sumOfChances = 0;
            for (int i = 0; i < chanceValues.Count; ++i) {
                sumOfChances += chanceValues[i];
            }
        }
    }
}
