﻿namespace Assets.Guildmaster.Scripts.Mech.Quests.Dialogues {
    using Utility;
    using System.Xml.Serialization;
    using System;

    [XmlInclude(typeof(DialogueChoiceChanceContainer)), XmlInclude(typeof(DialogueChoiceCheckContainer)), XmlInclude(typeof(DialogueChoiceInfoContainer))]
    public abstract class DialogueChoiceContainer {
        [XmlAttribute("TextKey")]
        public string textKey;
        [XmlAttribute("Type")]
        public DialogueChoiceType type;

        public abstract DialogueChoice Unpack();
        public DialogueChoiceContainer() { }
        public DialogueChoiceContainer(DialogueChoice choice) {
            this.textKey = choice.Text.Key;
            this.type = choice.Type;
        }
    }

    [Serializable]
    public abstract class DialogueChoice {
        #region Fields
        protected TrString text;
        protected DialogueChoiceType type;
        #endregion

        #region Accessors
        public TrString Text { get { return text; } }
        public DialogueChoiceType Type { get { return type; } set { type = value; } }
        #endregion

        #region Methods
        public abstract DialogueChoiceContainer Pack();
        /// <summary>
        /// Dialogue choice selected by player
        /// </summary>
        /// <returns>index of the next node in dialogue</returns>
        public abstract int Select(Party party);
        #endregion

        ///// <summary>
        ///// for serialization needs only
        ///// </summary>
        //public DialogueChoice() {
        //    text = null;
        //    type = DialogueChoiceType.Undefined;
        //}

        protected DialogueChoice(TrString text, DialogueChoiceType type) {
            this.text = text;
            this.type = type;
        }

        public abstract void NodeDeleted(int nodeIdx);
    }
}
