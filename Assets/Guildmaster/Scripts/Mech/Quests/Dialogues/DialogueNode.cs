﻿namespace Assets.Guildmaster.Scripts.Mech.Quests.Dialogues {
    using Utility;
    using System;
    using System.Collections.Generic;
    using System.Xml.Serialization;

    public class DialogueNodeContainer {
        [XmlAttribute("Type")]
        public DialogueNodeType type;
        [XmlAttribute("TextKey")]
        public string textKey;
        [XmlArray("DialogueChoices"), XmlArrayItem("Choice")]
        public List<DialogueChoiceContainer> choices;

        public DialogueNodeContainer() { }
        public DialogueNodeContainer(DialogueNode node) {
            this.type = node.Type;
            this.textKey = node.Text.Key;
            var nChoices = node.Choices;
            this.choices = new List<DialogueChoiceContainer>(nChoices.Count);
            for (int i = 0; i < nChoices.Count; ++i) {
                this.choices.Add(nChoices[i].Pack());
            }
        }
    }

    [Serializable]
    public class DialogueNode {
        #region Fields
        protected DialogueNodeType type;
        protected TrString text;
        protected List<DialogueChoice> choices;
        #endregion

        #region Accessors
        public DialogueNodeType Type { get { return type; } }
        public TrString Text { get { return text; } }
        public List<DialogueChoice> Choices { get { return choices; } }
        #endregion

        #region Methods
        ///// <summary>
        ///// Player selected an answer in dialogue
        ///// </summary>
        ///// <param name="idx">Index of the chosen answer</param>
        ///// <returns>index of the next dialogue node</returns>
        //public int SelectAnswer(int idx, Party party) {
        //    return choices[idx].Select(party);
        //}
        #endregion

        public DialogueNode(TrString text, DialogueNodeType type, List<DialogueChoice> choices) {
            this.text = text;
            this.type = type;
            this.choices = choices;
        }
        
        public DialogueNode(DialogueNodeContainer nc) { 
            this.text = new TrString(nc.textKey);
            this.type = nc.type;
            this.choices = new List<DialogueChoice>(nc.choices.Count);
            for(int i = 0; i < nc.choices.Count; ++i) {
                this.choices.Add(nc.choices[i].Unpack());
            }
        }
    }
}
