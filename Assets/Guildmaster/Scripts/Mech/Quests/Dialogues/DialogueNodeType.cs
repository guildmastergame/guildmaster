﻿namespace Assets.Guildmaster.Scripts.Mech.Quests.Dialogues {
    public enum DialogueNodeType {
        Start,
        Common,
        EndSuccess,
        EndFail,
        EndDeath
    }
}
