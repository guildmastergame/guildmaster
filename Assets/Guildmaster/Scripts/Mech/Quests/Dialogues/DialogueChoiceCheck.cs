﻿namespace Assets.Guildmaster.Scripts.Mech.Quests.Dialogues {
    using System;
    using System.Collections.Generic;
    using System.Xml.Serialization;
    using Utility;

    public class DialogueChoiceCheckContainer : DialogueChoiceContainer {
        [XmlAttribute("nextNodeSuccess")]
        public int nextNodeSuccess;
        [XmlAttribute("nextNodeFail")]
        public int nextNodeFail;
        [XmlArray("Tags"), XmlArrayItem("Tag")]
        public List<CreatureTag> tags;

        public override DialogueChoice Unpack() {
            return new DialogueChoiceCheck(new TrString(textKey), nextNodeSuccess, nextNodeFail, tags);
        }

        public DialogueChoiceCheckContainer() { }
        public DialogueChoiceCheckContainer(DialogueChoiceCheck choice) : base(choice) {
            this.nextNodeSuccess = choice.NextNodeSuccess;
            this.nextNodeFail = choice.NextNodeFail;
            this.tags = choice.Tags;
        }
    }

    [Serializable]
    public class DialogueChoiceCheck : DialogueChoice {
        #region Fields
        protected int nextNodeSuccess;
        protected int nextNodeFail;
        protected List<CreatureTag> tags;
        #endregion

        #region Accessors
        public int NextNodeSuccess { get { return nextNodeSuccess; } }
        public int NextNodeFail { get { return nextNodeFail; } }
        public List<CreatureTag> Tags { get { return tags; } }
        #endregion

        #region Methods
        public override int Select(Party party) {
            var partyTags = party.GetPartyTags();
            for (int i = 0; i < tags.Count; ++i) {
                if(!partyTags.Contains(tags[i])) {
                    return nextNodeFail;
                }
            }
            return nextNodeSuccess;
        }

        public override void NodeDeleted(int nodeIdx) {
            throw new NotImplementedException();
        }

        public override DialogueChoiceContainer Pack() {
            return new DialogueChoiceCheckContainer(this);
        }
        #endregion

        public DialogueChoiceCheck() : base(new TrString(), DialogueChoiceType.Check) {
            this.nextNodeSuccess = -1;
            this.nextNodeFail = -1;
            this.tags = new List<CreatureTag>();
        }

        public DialogueChoiceCheck(TrString text, int nextNodeSuccess, int nextNodeFail, List<CreatureTag> tags) : base(text, DialogueChoiceType.Check) {
            this.nextNodeSuccess = nextNodeSuccess;
            this.nextNodeFail = nextNodeFail;
            this.tags = tags;
        }
    }
}
