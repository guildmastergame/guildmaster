﻿namespace Assets.Guildmaster.Scripts.Mech.Quests.Dialogues {
    using Utility;
    using System;
    using System.Xml.Serialization;

    public class DialogueChoiceInfoContainer : DialogueChoiceContainer {
        [XmlAttribute("nextIdx")]
        public int nextNodeIdx;

        public override DialogueChoice Unpack() {
            return new DialogueChoiceInfo(new TrString(textKey), nextNodeIdx);
        }

        public DialogueChoiceInfoContainer() { }
        public DialogueChoiceInfoContainer(DialogueChoiceInfo choice) : base(choice) {
            this.nextNodeIdx = choice.NextNodeIdx;
        }
    }

    [Serializable]
    public class DialogueChoiceInfo : DialogueChoice {
        #region Fields
        protected int nextNodeIdx;
        #endregion

        #region Accessors
        public int NextNodeIdx { get { return nextNodeIdx; } set { nextNodeIdx = value; } }
        #endregion

        #region Methods
        public override int Select(Party party) {
            return nextNodeIdx;
        }

        public override void NodeDeleted(int nodeIdx) {
            if(nextNodeIdx == nodeIdx) {
                nextNodeIdx = -1;
            } else if(nextNodeIdx > nodeIdx) {
                --nextNodeIdx;
            }
        }

        public override DialogueChoiceContainer Pack() {
            return new DialogueChoiceInfoContainer(this);
        }
        #endregion

        public DialogueChoiceInfo() : base(new TrString(), DialogueChoiceType.Info) {
            this.nextNodeIdx = -1;
        }

        public DialogueChoiceInfo(TrString text, int nextNodeIdx) : base(text, DialogueChoiceType.Info) {
            this.nextNodeIdx = nextNodeIdx;
        }
    }
}
