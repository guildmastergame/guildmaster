﻿namespace Assets.Guildmaster.Scripts.Mech.Quests {
    using Utility;
    using Dialogues;
    using System;
    using System.Collections.Generic;
    using UnityEngine;
    using System.Xml.Serialization;

    public class QuestDialogueContainer : QuestContainer {
        [XmlArray("DialogueNodes"), XmlArrayItem("Node")]
        public List<DialogueNodeContainer> nodes;
        
        public override Quest Unpack() {
            List<DialogueNode> dNodes = new List<DialogueNode>(nodes.Count);
            for(int i = 0; i < nodes.Count; ++i) {
                dNodes.Add(new DialogueNode(nodes[i]));
            }
            return new QuestDialogue(new TrString(nameKey), new TrString(descKey), dNodes, new Vector2(mapMarkerX, mapMarkerY), nextIdxSuccess, nextIdxFail);
        }

        public QuestDialogueContainer() { }
        public QuestDialogueContainer(QuestDialogue q) : base(q) {
            q.PrepareToSerialization();
            var qNodes = q.Nodes;
            nodes = new List<DialogueNodeContainer>(qNodes.Count);
            for(int i = 0; i < qNodes.Count; ++i) {
                nodes.Add(new DialogueNodeContainer(qNodes[i]));
            }
        }
    }

    [Serializable]
    public class QuestDialogue : Quest {
        #region Fields
        protected List<DialogueNode> nodes;
        protected int curNodeIndex;
        #endregion

        #region Accessors
        public List<DialogueNode> Nodes { get { return nodes; } set { nodes = value; } }
        public DialogueNode CurNode { get { return nodes[curNodeIndex]; } }
        #endregion

        #region Methods
        public override void Update(ref Party party) {
            
        }
        //public void AnswerSelected(int idx, ref Party party) {
        //    curNodeIndex = nodes[curNodeIndex].SelectAnswer(idx, party);
        //}
        public void SetCurrentNodeIdx(int idx) {
            this.curNodeIndex = idx;
        }
        #endregion

        /// <summary>
        /// For editor purposes. Creates new quest with same basic data than we need to change it's type
        /// </summary>
        /// <param name="q">source quest to copy basic params</param>
        public QuestDialogue(Quest q) : base(q.Name, q.Desc, QuestType.Dialogue, q.MapMarkerPosition, q.NextIdxSuccess, q.NextIdxFail, q.ImageKey, q.Status) {
            nodes = new List<DialogueNode>();
            curNodeIndex = 0;
        }

        public QuestDialogue(TrString name, TrString desc, List<DialogueNode> nodes, Vector2 mapMarkerPosition, int nextNodeIdxSuccess, int nextNodeIdxFail, int curNodeIndex = 0, string imageKey = null, QuestStatus status = QuestStatus.Available) : base(name, desc, QuestType.Dialogue, mapMarkerPosition, nextNodeIdxSuccess, nextNodeIdxFail, imageKey, status) {
            this.nodes = nodes;
            this.curNodeIndex = curNodeIndex;
        }

        public QuestDialogue(QuestDialogueContainer qdc) : base(new TrString(qdc.nameKey), new TrString(qdc.descKey), qdc.type, new Vector2(qdc.mapMarkerX, qdc.mapMarkerY), qdc.nextIdxSuccess, qdc.nextIdxFail) {
            this.nodes = new List<DialogueNode>(qdc.nodes.Count);
            for(int i = 0; i < qdc.nodes.Count; ++i) {
                this.nodes.Add(new DialogueNode(qdc.nodes[i]));
            }
            this.curNodeIndex = 0;
        }
    }
}
