﻿namespace Assets.Guildmaster.Scripts.Mech.Quests {
    public enum QuestType {
        Undefined = 0,
        Dialogue,
        Skirmish,
        BlindSkirmish,
        Hunt,
        Scout,
        Start
    }
}
