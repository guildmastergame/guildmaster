﻿namespace Assets.Guildmaster.Scripts.Mech.Quests {
    using System;
    using System.Xml.Serialization;
    using UnityEngine;
    using Utility;

    public class QuestStartContainer : QuestContainer {
        [XmlAttribute("DaysPassed")]
        public int daysPassed;
        [XmlAttribute("RewardMoney")]
        public int rewardMoney;
        [XmlAttribute("RewardFame")]
        public int rewardFame;

        public override Quest Unpack() {
            return new QuestStart(daysPassed, rewardMoney, rewardFame, new TrString(nameKey), new TrString(descKey), new Vector2(mapMarkerX, mapMarkerY), nextIdxSuccess, nextIdxFail);
        }

        public QuestStartContainer() { }
        public QuestStartContainer(QuestStart q) : base(q) {
            q.PrepareToSerialization();
            this.daysPassed = q.DaysPassed;
            this.rewardMoney = q.RewardMoney;
            this.rewardFame = q.RewardFame;
        }
    }

    [Serializable]
    public class QuestStart : Quest {
        #region Fields
        protected int daysPassed;
        protected int rewardMoney;
        protected int rewardFame;
        #endregion

        #region Accessors
        public int DaysPassed { get { return daysPassed; } set { daysPassed = value > 0 ? value : 0; } }
        public int RewardMoney { get { return rewardMoney; } set { rewardMoney = value > 0 ? value : 0; } }
        public int RewardFame { get { return rewardFame; } set { rewardFame = value > 0 ? value : 0; } }
        #endregion

        #region Methods
        public override void Update(ref Party party) {

        }
        
        #endregion

        /// <summary>
        /// For editor purposes. Creates new quest with same basic data than we need to change it's type
        /// </summary>
        /// <param name="q">source quest to copy basic params</param>
        public QuestStart(Quest q) : base(q.Name, q.Desc, QuestType.Start, q.MapMarkerPosition, q.NextIdxSuccess, q.NextIdxFail, q.ImageKey, q.Status) {
            this.daysPassed = 0;
            this.rewardMoney = 0;
            this.rewardFame = 0;
        }

        public QuestStart(int daysPassed, int rewardMoney, int rewardFame, TrString name, TrString desc, Vector2 mapMarkerPosition, int nextNodeIdxSuccess, int nextNodeIdxFail, string imageKey = null, QuestStatus status = QuestStatus.Available) : base(name, desc, QuestType.Start, mapMarkerPosition, nextNodeIdxSuccess, nextNodeIdxFail, imageKey, status) {
            this.daysPassed = daysPassed;
            this.rewardMoney = rewardMoney;
            this.rewardFame = rewardFame;
        }

        public QuestStart(QuestStartContainer qdc) : base(new TrString(qdc.nameKey), new TrString(qdc.descKey), qdc.type, new Vector2(qdc.mapMarkerX, qdc.mapMarkerY), qdc.nextIdxSuccess, qdc.nextIdxFail) {
            this.daysPassed = qdc.daysPassed;
            this.rewardMoney = qdc.rewardMoney;
            this.rewardFame = qdc.rewardFame;
        }
    }
}
