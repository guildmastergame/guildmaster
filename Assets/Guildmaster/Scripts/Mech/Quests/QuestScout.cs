﻿namespace Assets.Guildmaster.Scripts.Mech.Quests {
    using Utility;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    [Serializable]
    public class QuestScout : Quest {
        #region Fields
        protected List<Loot> rewards;
        protected int turnsRequired;
        #endregion

        #region Accessors
        public int TurnsRequired { get { return turnsRequired; } set { turnsRequired = value < 1 ? 1 : value; } }
        #endregion

        #region Methods
        public override void Update(ref Party party) {
            throw new NotImplementedException();
        }
        #endregion

        public QuestScout(Quest q) : base(q.Name, q.Desc, QuestType.Scout, q.MapMarkerPosition, q.NextIdxSuccess, q.NextIdxFail, q.ImageKey, q.Status) {
            turnsRequired = 1;
            rewards = new List<Loot>();
        }

        public QuestScout(TrString name, TrString desc, int turnsRequired, Vector2 mapMarkerPosition, int nextNodeIdxSuccess, int nextNodeIdxFail, List<Loot> rewards = null/*, TrString msgStart = null, TrString msgEndSuccess = null, TrString msgEndFail = null, TrString msgInProgress = null*/, string imageKey = null, QuestStatus status = QuestStatus.Available) : base(name, desc, QuestType.Scout, mapMarkerPosition, nextNodeIdxSuccess, nextNodeIdxFail/*, msgStart, msgEndSuccess, msgEndFail, msgInProgress*/, imageKey, status) {
            this.turnsRequired = turnsRequired;
            this.rewards = rewards;
        }
    }
}
