﻿namespace Assets.Guildmaster.Scripts.Mech.Quests {
    using Utility;
    using System;
    using System.Collections.Generic;
    using UnityEngine;
    using System.Xml.Serialization;

    public abstract class QuestBattleContainer : QuestContainer {
        [XmlArray("Loots"), XmlArrayItem("Loot")]
        public List<Loot> loot;
    }

    [Serializable]
    public abstract class QuestBattle : Quest {
        #region Fields
        protected List<Loot> loot;
        #endregion

        #region Accessors
        #endregion

        #region Methods
        public abstract Party GenerateFoes();
        
        protected QuestBattle(TrString name, TrString desc, QuestType type,  Vector2 mapMarkerPosition, int nextNodeIdxSuccess, int nextNodeIdxFail, string imageKey = null, QuestStatus status = QuestStatus.Available) : base(name, desc, type, mapMarkerPosition, nextNodeIdxSuccess, nextNodeIdxFail, imageKey, status) {

        }
        #endregion
    }
}
