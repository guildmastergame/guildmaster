﻿namespace Assets.Guildmaster.Scripts.Mech.Quests {
    using Utility;
    using System;
    using System.Collections.Generic;
    using UnityEngine;
    using System.Xml.Serialization;

    public class QuestSkirmishContainer : QuestContainer {
        [XmlArray("FoeParties"), XmlArrayItem("Party")]
        public List<PartyContainer> foes = new List<PartyContainer>();

        public override Quest Unpack() {
            return new QuestSkirmish(this);
        }
        public QuestSkirmishContainer() { }
        public QuestSkirmishContainer(QuestSkirmish q) : base(q) {
            q.PrepareToSerialization();
            var qFoes = q.Foes;
            foes = new List<PartyContainer>(qFoes.Count);
            for(int i = 0; i < qFoes.Count; ++i) {
                foes.Add(new PartyContainer(qFoes[i]));
            } 
        }
    }

    [Serializable]
    public class QuestSkirmish : QuestBattle {
        #region Fields
        /// <summary>
        /// Actual party descriptions will be stored in quest file
        /// </summary>
        protected List<PartyDescription> foes;
        /// <summary>
        /// It's a cached real parties for quest editor to display creature icons and other info
        /// </summary>
        [NonSerialized]
        protected List<Party> cachedParties = null;
        #endregion

        #region Accessors
        public List<Party> Foes {
            get {
                if(cachedParties == null) {
                    cachedParties = GetParties();
                }
                return cachedParties;
            }
        }
        #endregion

        #region Methods
        public override void PrepareToSerialization() {
            foes = new List<PartyDescription>(cachedParties.Count);
            for(int i = 0; i < cachedParties.Count; ++i) {
                foes.Add(new PartyDescription(cachedParties[i]));
            }
        }

        public override void PrepareAfterSerialization() {
            cachedParties = GetParties();
        }

        private List<Party> GetParties() {
            var parties = new List<Party>(foes.Count);
            for(int i = 0; i < foes.Count; ++i) {
                parties.Add(foes[i].GetParty());
            }
            return parties;
        }
        public override void Update(ref Party party) {
            throw new NotImplementedException();
        }
        public override Party GenerateFoes() {
            return foes[UnityEngine.Random.Range(0, foes.Count)].GetParty();
        }
        #endregion

        public QuestSkirmish(Quest q) : base(q.Name, q.Desc, QuestType.Skirmish, q.MapMarkerPosition, q.NextIdxSuccess, q.NextIdxFail, q.ImageKey, q.Status) {
            foes = new List<PartyDescription>();
        }

        public QuestSkirmish(TrString name, TrString desc, List<PartyDescription> foes, Vector2 mapMarkerPosition, int nextNodeIdxSuccess, int nextNodeIdxFail, string imageKey = null, QuestStatus status = QuestStatus.Available) : base(name, desc, QuestType.Skirmish, mapMarkerPosition, nextNodeIdxSuccess, nextNodeIdxFail, imageKey, status) {
            this.foes = foes;
        }

        public QuestSkirmish(QuestSkirmishContainer qc) : base(new TrString(qc.nameKey), new TrString(qc.descKey), qc.type, new Vector2(qc.mapMarkerX, qc.mapMarkerY), qc.nextIdxSuccess, qc.nextIdxFail, qc.imageKey, qc.status) {
            this.foes = new List<PartyDescription>(qc.foes.Count);
            for(int i = 0; i < qc.foes.Count; ++i) {
                var fpc = qc.foes[i];
                foes.Add(new PartyDescription(fpc));
            }
        }
    }
}
