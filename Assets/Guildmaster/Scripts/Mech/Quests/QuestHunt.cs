﻿namespace Assets.Guildmaster.Scripts.Mech.Quests {
    using Utility;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    [Serializable]
    public class QuestHunt : QuestBlindSkirmish {
        #region Fields
        List<CreatureVictim> victims;
        #endregion

        #region Accessors
        #endregion

        #region Methods
        public override void Update(ref Party party) {
            throw new NotImplementedException();
        }
        #endregion

        public QuestHunt(Quest q) : base(q.Name, q.Desc, new List<CreatureSpawnSettings>(), q.MapMarkerPosition, q.NextIdxSuccess, q.NextIdxFail, q.ImageKey, q.Status) {
            type = QuestType.Hunt;
            victims = new List<CreatureVictim>();
        }

        public QuestHunt(TrString name, TrString desc, List<CreatureSpawnSettings> creatures, List<CreatureVictim> victims, Vector2 mapMarkerPosition, int nextNodeIdxSuccess, int nextNodeIdxFail/*, TrString msgStart = null, TrString msgEndSuccess = null, TrString msgEndFail = null, TrString msgInProgress = null*/, string imageKey = null, QuestStatus status = QuestStatus.Available) : base(name, desc, creatures, mapMarkerPosition, nextNodeIdxSuccess, nextNodeIdxFail/*, msgStart, msgEndSuccess, msgEndFail, msgInProgress*/, imageKey, status) {
            this.type = QuestType.Hunt;
            this.victims = victims;
        }
    }
}
