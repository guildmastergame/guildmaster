﻿namespace Assets.Guildmaster.Scripts.Mech.Quests {
    public enum QuestStatus {
        Undefined = 0,
        NotAvailable,
        Available,
        Succed,
        Failed,
        InProgress
    }
}
