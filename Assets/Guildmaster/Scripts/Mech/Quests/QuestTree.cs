﻿namespace Assets.Guildmaster.Scripts.Mech.Quests {
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using System.Xml.Serialization;
    using Dialogues;
    using Utility;
    using UnityEngine;
    using Control;

    [XmlRoot("QuestCollection")]
    public class QuestTreeContainer {
        private static XmlSerializer serializer = new XmlSerializer(typeof(QuestTreeContainer));
        [XmlArray("QuestNodes"), XmlArrayItem("QuestNode")]
        public List<QuestContainer> nodes = new List<QuestContainer>();
        [XmlArray("OptionalNodes"), XmlArrayItem("OptionalNode")]
        public List<QuestContainer> optNodes = new List<QuestContainer>();
        [XmlAttribute("NameKey")]
        public string nameKey;
        [XmlAttribute("DaysAvailable")]
        public int daysAvailable;
        [XmlAttribute("OverdueMsgKey")]
        public string overdueMessageKey;

        public QuestTreeContainer() { }

        public static QuestTreeContainer Deserialize(string filepath) {
            if(!File.Exists(filepath)) {
                return null;
            }
            //var stream = new FileStream(filepath, FileMode.Open);
            var stream = new StreamReader(filepath, Encoding.GetEncoding("UTF-8"));
            var container = serializer.Deserialize(stream) as QuestTreeContainer;
            stream.Close();
            return container;
        }

        public void Serialize(string filepath) {
            //var stream = new FileStream(filepath, FileMode.Create, );
            var stream = new StreamWriter(filepath, false, Encoding.GetEncoding("UTF-8"));
            serializer.Serialize(stream, this);
            stream.Close();
        }

        public QuestTree Unpack() {
            var nList = new List<Quest>(nodes.Count);
            for(int i = 0; i < nodes.Count; ++i) {
                nList.Add(nodes[i].Unpack());
            }
            var optList = new List<Quest>(optNodes.Count);
            for(int i = 0; i < optNodes.Count; ++i) {
                optList.Add(optNodes[i].Unpack());
            }
            return new QuestTree(nameKey, daysAvailable, overdueMessageKey, nList, optList);
        }
    }

    [Serializable]
    public class QuestTree {
        #region Fields
        /// <summary>
        /// Name of the whole quest tree
        /// </summary>
        protected TrString name;
        /// <summary>
        /// How much days this quest tree will be available befor it fails
        /// </summary>
        protected int daysAvailable;
        protected int curNodeIdx;
        protected int startIdx;
        protected TrString overdueMsg;
        protected QuestTreeStatus status;
        protected Party party;
        protected List<Quest> nodes = new List<Quest>();
        /// <summary>
        /// Optional nodes are secondary objectives which are 
        /// not required to be finished or even be passed through
        /// </summary>
        protected List<Quest> optNodes = new List<Quest>();
        /// <summary>
        /// Game date when player received this quest
        /// </summary>
        protected Date dateStart;

        public void NodePass(QuestBattle qb, BattleResult result, Party playerParty = null) {
            if (playerParty != null) {
                this.party = playerParty;
            }
            if(nodes[curNodeIdx].Type == QuestType.Skirmish || nodes[curNodeIdx].Type == QuestType.BlindSkirmish || nodes[curNodeIdx].Type == QuestType.Hunt) {
                switch (result) {
                    case BattleResult.Victory:
                        curNodeIdx = nodes[curNodeIdx].NextIdxSuccess;
                        if (curNodeIdx == -1) {
                            status = QuestTreeStatus.Succed;
                        }
                        break;
                    case BattleResult.Defeat:
                        curNodeIdx = nodes[curNodeIdx].NextIdxFail;
                        if (curNodeIdx == -1) {
                            status = QuestTreeStatus.Failed;
                        }
                        break;
                    case BattleResult.Draw:
                        break;
                }
            } else {
                Debug.LogError("NodePass: QuestType isn't Battle: " + nodes[curNodeIdx].Type);
            }
        }

        public void NodePass(DialogueNodeType type) {
            if(nodes[curNodeIdx].Type == QuestType.Dialogue) {
                switch (type) {
                    case DialogueNodeType.EndSuccess:
                        curNodeIdx = nodes[curNodeIdx].NextIdxSuccess;
                        if(curNodeIdx == -1) {
                            status = QuestTreeStatus.Succed;
                        }
                        break;
                    case DialogueNodeType.EndFail:
                        curNodeIdx = nodes[curNodeIdx].NextIdxFail;
                        if (curNodeIdx == -1) {
                            status = QuestTreeStatus.Failed;
                        }
                        break;
                    case DialogueNodeType.EndDeath:
                        curNodeIdx = -1;
                        status = QuestTreeStatus.Failed;
                        break;
                }
                
            } else {
                Debug.LogError("NodePass: QuestType isn't Dialogue: " + type);
            }
        }

        public void Embark() {
            status = QuestTreeStatus.InProgress;
        }

        /// <summary>
        /// Boolean detects if this quest tree have a party set and ready to roll
        /// </summary>
        protected bool isReadyToEmbark = false;

        [NonSerialized]
        private List<int> startNodeIdxs = new List<int>();
        #endregion

        #region Accessors
        public string Name { get { return name.ToString(); } }
        public QuestTreeStatus Status { get { return status; } }
        public Party Party { get { return party; } set { party = value; } }
        public Quest CurrentNode { get { return nodes[curNodeIdx] ?? null; } }
        public QuestStart CurrentStart { get { return nodes[startIdx] as QuestStart; } }
        public bool IsReadyToEmbark { get; set; }
        #endregion

        #region Methods
        public string GetDescription() {
            int days = Date.DifferenceDays(dateStart, Date.Now);
            StringBuilder sb = new StringBuilder();
            for(int i = 0; i < startNodeIdxs.Count; ++i) {
                var node = nodes[startNodeIdxs[i]] as QuestStart;
                if (days >= node.DaysPassed) {
                    sb.AppendLine(new Date(dateStart.DateValue + node.DaysPassed).ToString());
                    sb.AppendLine(node.Desc.ToString());
                    sb.AppendLine();
                }
            }
            return sb.ToString();
        }

        public Party GetParty() {
            if(party == null) {
                party = new Party();
            }
            return party;
        }

        public List<Quest> GetObjectives() {
            var objs = new List<Quest>();
            int idx = startIdx;
            while(idx != -1 && idx != curNodeIdx) {
                idx = nodes[idx].NextIdxSuccess;
                objs.Add(nodes[idx]);
            }
            return objs;
        }

        public void TurnFinished() {
            nodes[curNodeIdx].Update(ref party);
        }

        public void TurnStarted() {

        }

        public void RecalculateQuestStart() {
            startIdx = -1;
            int daysPassed = Date.DifferenceDays(dateStart, Date.Now);
            QuestStart start = null;
            QuestStart tempQStart = null;
            foreach (int idx in startNodeIdxs) {
                tempQStart = nodes[idx] as QuestStart;
                if (tempQStart.DaysPassed <= daysPassed && (start == null || start.DaysPassed < tempQStart.DaysPassed)) {
                    start = tempQStart;
                    startIdx = idx;
                }
            }
            if (startIdx != -1) {
                curNodeIdx = nodes[startIdx].NextIdxSuccess;
            }
        }

        public void StartQuest(Date dateStart) {
            this.dateStart = new Date(dateStart);
            RecalculateQuestStart();
            this.status = QuestTreeStatus.Ready;
        }

        /// <summary>
        /// TEMP method for test and debug the battle
        /// </summary>
        /// <returns></returns>
        internal List<Quest> GetNodes() {
            return nodes;
        }

        //public void Embark() {
        //    curNodeIdx = 0;
        //    status = QuestTreeStatus.InProgress;
        //}
        #endregion

        public QuestTree(string nameKey, int daysAvailable, string overdueMsgKey, List<Quest> nodes, List<Quest> optNodes) {
            this.name = new TrString(nameKey);
            this.daysAvailable = daysAvailable;
            this.overdueMsg = new TrString(overdueMsgKey);
            this.nodes = nodes;
            for(int i = 0; i < nodes.Count; ++i) {
                if(nodes[i].Type == QuestType.Start) {
                    startNodeIdxs.Add(i);
                }
            }
            this.optNodes = optNodes;
            this.curNodeIdx = -1;
            status = QuestTreeStatus.Ready;
        }
    }
}
