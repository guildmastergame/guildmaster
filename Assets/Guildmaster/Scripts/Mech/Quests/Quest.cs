﻿namespace Assets.Guildmaster.Scripts.Mech.Quests {
    using Utility;
    using System;
    using UnityEngine;
    using System.Xml.Serialization;
    using Entities;

    [XmlInclude(typeof(QuestSkirmishContainer)), XmlInclude(typeof(QuestDialogueContainer)), XmlInclude(typeof(QuestStartContainer))]
    public abstract class QuestContainer {
        #region Fields
        public QuestStatus status;
        public QuestType type;
        [XmlAttribute("mapX")]
        public float mapMarkerX;
        [XmlAttribute("mapY")]
        public float mapMarkerY;
        [XmlAttribute("successIdx")]
        public int nextIdxSuccess;
        [XmlAttribute("failIdx")]
        public int nextIdxFail;
        public string nameKey;
        public string descKey;
        public string imageKey;
        #endregion

        public abstract Quest Unpack();

        public static QuestContainer PackEditorNode(Quest node) {
            switch (node.Type) {
                case QuestType.Dialogue:
                    return new QuestDialogueContainer(node as QuestDialogue); 
                case QuestType.Skirmish:
                    return new QuestSkirmishContainer(node as QuestSkirmish);
                case QuestType.Start:
                    return new QuestStartContainer(node as QuestStart);
                default:
                    return null;
            }
        }

        protected QuestContainer() { }
        protected QuestContainer(Quest q) {
            this.nameKey = q.NameKey;
            this.descKey = q.DescKey;
            this.imageKey = q.ImageKey;
            this.mapMarkerX = q.MapMarkerPosition.x;
            this.mapMarkerY = q.MapMarkerPosition.y;
            this.status = q.Status;
            this.type = q.Type;
            this.nextIdxSuccess = q.NextIdxSuccess;
            this.nextIdxFail = q.NextIdxFail;
        }
    }

    /// <summary>
    /// This is the quest node also known as quest objective
    /// Quests are stored in QuestTree
    /// </summary>
    [Serializable]
    public abstract class Quest : Entity {
        #region Fields
        protected QuestStatus status;
        protected QuestType type;
        protected float mapMarkerX;
        protected float mapMarkerY;
        protected int nextIdxSuccess;
        protected int nextIdxFail;
        #endregion

        #region Accessors
        public QuestType Type { get { return type; } }
        public QuestStatus Status { get { return status; } }
        public Vector2 MapMarkerPosition { get { return new Vector2(mapMarkerX, mapMarkerY); } set { mapMarkerX = value.x; mapMarkerY = value.y; } }
        /// <summary>
        /// Next node index if this node returns success state. 
        /// -1 - equals to quest tree completed with success status
        /// </summary>
        public int NextIdxSuccess { get { return nextIdxSuccess; } set { nextIdxSuccess = value; } }
        /// <summary>
        /// Next node index if this node returns fail state
        /// -1 - equals to quest tree completed with fail status
        /// </summary>
        public int NextIdxFail { get { return nextIdxFail; } set { nextIdxFail = value; } }
        public override EntityType EntityType {
            get {
                return EntityType.Quest;
            }
        }
        #endregion

        #region Methods
        public abstract void Update(ref Party party);
        #endregion
        public virtual void PrepareToSerialization() { }
        public virtual void PrepareAfterSerialization() { }

        public Quest(TrString name, TrString desc, QuestType type, Vector2 mapMarkerPos, int nextIdxSuccess, int nextIdxFail, string imageKey = null, QuestStatus status = QuestStatus.Available) : base(name, desc, imageKey) {
            this.type = type;
            this.mapMarkerX = mapMarkerPos.x;
            this.mapMarkerY = mapMarkerPos.y;
            this.nextIdxSuccess = nextIdxSuccess;
            this.nextIdxFail = nextIdxFail;
            this.status = status;
        }

        public void SetMarkerCallback(int posX, int posY) {
            this.mapMarkerX = posX;
            this.mapMarkerY = posY;
        }
    }
}
