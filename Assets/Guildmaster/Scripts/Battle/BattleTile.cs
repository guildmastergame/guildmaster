﻿namespace Assets.Guildmaster.Scripts.Battle {
    using Mech.Abilities.Effects;
    using Control;
    using Mech;
    using Mech.Attributes;
    using Mech.Battle;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.UI;
    using Utility;

    public enum TileHighlightType {
        BLUE,
        GREEN,
        RED
    }

    [Serializable]
    public class BattleTile : MonoBehaviour {
        #region Fields
        protected static BattleController cBattle;
        private static readonly Color TILE_HIGHLIGHT_BLUE = Color.blue;
        private static readonly Color TILE_HIGHLIGHT_GREEN = Color.green;
        private static readonly Color TILE_HIGHLIGHT_RED = Color.red;
        public const int MAX_EFFECTS_ON_VIEW = 8;
        public const float HIGHLIGHT_BREATHING_SPEED = 0.3f;
        public const float HIGHLIGHT_BREATHING_CEIL = 0.35f;
        public const float HIGHLIGHT_BREATHING_FLOOR = 0.1f;
        public const float ARMOR_FADE_SPEED = 2.0f;
        public const float ARMOR_SHOW_TIME_DELAY = 2.5f;
        protected BattleCreature creature;
        public Image imgPortrait;
        public Image imgDmgFilter;
        public Image imgHighlight;
        public Image imgFrame;
        public Image imgArmor;
        public Image imgProtection;
        public Image imgAttack;
        public Image imgPopup;
        public Text textArmor;
        public Text textProtection;
        public Text textAttack;
        public Text textHealth;
        public Text textPopup;
        public GameObject goAttack;
        public GameObject goArmor;
        public GameObject goProtection;
        public GameObject goPopup;
        public GameObject goHighlight;
        public GameObject goHealthText;
        public GameObject goDmgFilter;
        public GameObject[] goEffects;
        protected Image[] imgEffects;
        protected BattlePosition pos;
        //Animation variables
        protected bool highlightBreathInc = true;
        protected bool showArmor = false;
        protected bool showProtection = false;
        protected int currentArmorShown = 0; // 0 - none, 1 - armor, 2 - protection
        protected float curArmorShowDelay = 0f;
        protected Queue<TilePopupAttributes> popupQueue = new Queue<TilePopupAttributes>();//queue of popup's to show
        //
        private RectTransform rtrDmgFilter;
        private float dmgFilterBaseHeight;
        #endregion
        #region Accessors
        public static BattleController BattleController { set { cBattle = value; } }
        public BattleCreature Creature { get { return creature; } }
        public BattlePosition Pos { get { return pos; } }
        #endregion
        #region Methods
        void Awake() {
            rtrDmgFilter = goDmgFilter.GetComponent<RectTransform>();
            dmgFilterBaseHeight = rtrDmgFilter.sizeDelta.y;
        }
        void Start() {
            imgEffects = new Image[goEffects.Length];
            for(int i = 0; i < goEffects.Length; ++i) {
                imgEffects[i] = goEffects[i].GetComponent<Image>();
            }
            
        }
        public void Initialize(BattlePosition pos) {
            this.pos = pos;
            Reset();
        }
        public void Reset() {
            this.creature = null;
            goArmor.SetActive(false);
            goPopup.SetActive(false);
            goAttack.SetActive(false);
            goDmgFilter.SetActive(false);
            goProtection.SetActive(false);
            goHighlight.SetActive(false);
            goHealthText.SetActive(false);
            Refresh();
        }
        public void Set(BattleCreature bc) {
            if (bc == null) {
                Reset();
            } else {
                this.creature = bc;
                this.creature.SetTile(this);
            }
            Refresh();
        }
        public void Refresh() {
            if (creature == null) {
                imgPortrait.color = Extensions.Transparent;
            } else {
                imgPortrait.color = Color.white;
                imgPortrait.sprite = creature.ImagePortrait;
            }
            RefreshHealth();
            RefreshAttack();
            RefreshArmorAndProtection();
            RefreshEffects();   
        }
        public void RefreshEffects() {
            int idx = 0;
            if(creature != null) {
                var efx = creature.Effects;
                for (int i = 0; i < efx.Count && idx < goEffects.Length; ++i) {
                    var effectIcon = efx[i].Image;
                    if (effectIcon != null) {
                        goEffects[idx].SetActive(true);
                        imgEffects[idx++].sprite = effectIcon;
                    }
                }
            }
            for(int i = idx; i < goEffects.Length; ++i) {
                goEffects[i].SetActive(false);
            }
        }
        public void RefreshHealth() {
            if(creature == null) {
                return;
            }
            var health = creature.Health;
            textHealth.text = string.Format("{0}/{1}", health.Hp, health.MaxHp);
            rtrDmgFilter.sizeDelta = new Vector2(rtrDmgFilter.sizeDelta.x, (1f - (float)health.Hp / health.MaxHp) * dmgFilterBaseHeight);
            goDmgFilter.SetActive(true);
        }
        public void RefreshAttack() {
            if (creature == null) {
                return;
            }
            var mAbility = creature.MainAbility;
            var damageEfx = mAbility.GetActiveEffectsOfType<EffectDamage>(EffectType.Damage);
            if (damageEfx.Count > 0) {
                //Pick a first one for simplicity now
                textAttack.text = damageEfx[0].Damage.GetAsColoredString(creature, mAbility.Type);
                goAttack.SetActive(true);
            } else { //it's not damage dealing ability. No we will just skip it 
                goArmor.SetActive(false);
            }
        }
        public void RefreshArmorAndProtection() {
            if (creature == null) {
                return;
            }
            bool lastArmorStatus = showArmor;
            bool lastProtectionStatus = showProtection;
            int armor = creature.GetCurrentArmor();
            textArmor.text = armor.ToString();
            showArmor = armor > 0;
            int protection = creature.GetCurrentArmor(DamageType.All);
            textProtection.text = protection.ToString();
            showProtection = protection > 0;
            SetupArmorIconAnimation(lastArmorStatus, lastProtectionStatus);
        }
        protected IEnumerator ShowArmor() {
            Color cArmor = imgArmor.color, cArmorText = textArmor.color;
            float fadeValue = ARMOR_FADE_SPEED * Time.deltaTime;
            cArmor.a += fadeValue;
            cArmorText.a += fadeValue;
            if (cArmor.a < 0.98f) {
                imgArmor.color = cArmor;
                textArmor.color = cArmorText;
                yield return new WaitForSeconds(0.02f);
            }
            cArmor.a = 1f;
            cArmorText.a = 1f;
            imgArmor.color = cArmor;
            textArmor.color = cArmorText;
        }
        protected IEnumerator ShowProtection() {
            Color cProtection = imgProtection.color, cProtectionText = textProtection.color;
            float fadeValue = ARMOR_FADE_SPEED * Time.deltaTime;
            cProtection.a += fadeValue;
            cProtectionText.a += fadeValue;
            if (cProtection.a < 0.98f) {
                imgProtection.color = cProtection;
                textProtection.color = cProtectionText;
                yield return new WaitForSeconds(0.02f);
            }
            cProtection.a = 1f;
            cProtectionText.a = 1f;
            imgProtection.color = cProtection;
            textProtection.color = cProtectionText;
        }
        protected IEnumerator HideArmor() {
            Color cArmor = imgArmor.color, cArmorText = textArmor.color;
            float fadeValue = ARMOR_FADE_SPEED * Time.deltaTime;
            cArmor.a -= fadeValue;
            cArmorText.a -= fadeValue;
            if (cArmor.a > 0.02f) {
                imgArmor.color = cArmor;
                textArmor.color = cArmorText;
                yield return new WaitForSeconds(0.02f);
            }
            goArmor.SetActive(false);
        }
        protected IEnumerator HideProtection() {
            Color cProtection = imgProtection.color, cProtectionText = textProtection.color;
            float fadeValue = ARMOR_FADE_SPEED * Time.deltaTime;
            cProtection.a -= fadeValue;
            cProtectionText.a -= fadeValue;
            if (cProtection.a > 0.02f) {
                imgProtection.color = cProtection;
                textProtection.color = cProtectionText;
                yield return new WaitForSeconds(0.02f);
            }
            goProtection.SetActive(false);
        }
        protected void SetupArmorIconAnimation(bool lastArmorStatus, bool lastProtectionStatus) {
            Color cArmor = imgArmor.color, cArmorText = textArmor.color;
            Color cProtection = imgProtection.color, cProtectionText = textProtection.color;
            curArmorShowDelay = 0f;
            if (!lastArmorStatus && !lastProtectionStatus) {
                if (showArmor && showProtection) {
                    currentArmorShown = 1;
                    cProtection.a = 0f;
                    cProtectionText.a = 0f;
                    imgProtection.color = cProtection;
                    textProtection.color = cProtectionText;
                } else if (showArmor) {
                    cArmor.a = 0f;
                    cArmorText.a = 0f;
                    imgArmor.color = cArmor;
                    textArmor.color = cArmorText;
                    StartCoroutine(ShowArmor());
                } else if (showProtection) {
                    cProtection.a = 0f;
                    cProtectionText.a = 0f;
                    imgProtection.color = cProtection;
                    textProtection.color = cProtectionText;
                    StartCoroutine(ShowProtection());
                }
                goArmor.SetActive(showArmor);
                goProtection.SetActive(showProtection);
            } else if (lastArmorStatus && lastProtectionStatus) {
                if (!showArmor && !showProtection) {
                    if (currentArmorShown == 1) {
                        StartCoroutine(HideArmor());
                    } else if (currentArmorShown == 2) {
                        StartCoroutine(HideProtection());
                    }
                    currentArmorShown = 0;
                } else if (showArmor && showProtection) {
                    return;
                } else if (showArmor) {
                    StartCoroutine(ShowArmor());
                    StartCoroutine(HideProtection());
                    currentArmorShown = 0;
                } else if (showProtection) {
                    StartCoroutine(ShowProtection());
                    StartCoroutine(HideArmor());
                    currentArmorShown = 0;
                }
            } else if (lastArmorStatus) {
                if (showArmor && showProtection) {
                    goProtection.SetActive(true);
                    currentArmorShown = 1;
                    cProtection.a = 0f;
                    cProtectionText.a = 0f;
                    imgProtection.color = cProtection;
                    textProtection.color = cProtectionText;
                } else if (!showArmor && !showProtection) {
                    StartCoroutine(HideArmor());
                } else if (showProtection) {
                    goProtection.SetActive(true);
                    currentArmorShown = 1;
                    cProtection.a = 0f;
                    cProtectionText.a = 0f;
                    imgProtection.color = cProtection;
                    textProtection.color = cProtectionText;
                    StartCoroutine(HideArmor());
                    StartCoroutine(ShowProtection());
                }
            } else if (lastProtectionStatus) {
                if (showArmor && showProtection) {
                    goArmor.SetActive(true);
                    currentArmorShown = 2;
                    cArmor.a = 0f;
                    cArmorText.a = 0f;
                    imgArmor.color = cArmor;
                    textArmor.color = cArmorText;
                } else if (!showArmor && !showProtection) {
                    StartCoroutine(HideProtection());
                } else if (showArmor) {
                    goArmor.SetActive(true);
                    currentArmorShown = 2;
                    cArmor.a = 0f;
                    cArmorText.a = 0f;
                    imgArmor.color = cArmor;
                    textArmor.color = cArmorText;
                    StartCoroutine(HideProtection());
                    StartCoroutine(ShowArmor());
                }
            }
        }
        public void TileCursorEnter() {
            if (creature != null && !cBattle.HasAnimationsInProgress) {
                goHealthText.SetActive(true);
                //goDmgFilter.SetActive(true);
            }
        }
        public void TileCursorExit() {
            goHealthText.SetActive(false);
            //goDmgFilter.SetActive(false);
        }
        public void TileClicked() {
            cBattle.TileClicked(this);
        }
        private Color GetColorFromType(TileHighlightType type) {
            switch (type) {
                case TileHighlightType.BLUE:
                    return TILE_HIGHLIGHT_BLUE;
                case TileHighlightType.GREEN:
                    return TILE_HIGHLIGHT_GREEN;
                case TileHighlightType.RED:
                    return TILE_HIGHLIGHT_RED;
            }
            return Color.black;
        }
        public void UpdateHighlightColor(TileHighlightType type) {
            if (goHighlight.activeSelf) {
                Color c = imgHighlight.color;
                Color color = GetColorFromType(type);
                c.r = color.r;
                c.g = color.g;
                c.b = color.b;
                imgHighlight.color = c;
            }
        }
        public void ShowHighlight(TileHighlightType type) {
            goHighlight.SetActive(true);
            Color color = GetColorFromType(type);
            imgHighlight.color = new Color(color.r, color.g, color.b, HIGHLIGHT_BREATHING_FLOOR);
            highlightBreathInc = true;
        }
        public void HideHighlight() {
            goHighlight.SetActive(false);
        }
        protected void animateHighlightBreathing() {
            if (imgHighlight.color.a > HIGHLIGHT_BREATHING_CEIL) {
                highlightBreathInc = false;
            } else if (imgHighlight.color.a < HIGHLIGHT_BREATHING_FLOOR) {
                highlightBreathInc = true;
            }
            Color c = imgHighlight.color;
            c.a += (highlightBreathInc ? (HIGHLIGHT_BREATHING_SPEED) : (-HIGHLIGHT_BREATHING_SPEED)) * Time.deltaTime;
            imgHighlight.color = c;
        }
        protected void AnimateArmorIconChanging() {
            if (curArmorShowDelay > ARMOR_SHOW_TIME_DELAY) {
                Color cArmor = imgArmor.color, cArmorText = textArmor.color;
                Color cProtection = imgProtection.color, cProtectionText = textProtection.color;
                float fadeValue = ARMOR_FADE_SPEED * Time.deltaTime;
                cArmor.a += currentArmorShown == 1 ? (-fadeValue) : (fadeValue);
                cArmorText.a += currentArmorShown == 1 ? (-fadeValue) : (fadeValue);
                cProtection.a += currentArmorShown == 1 ? (fadeValue) : (-fadeValue);
                cProtectionText.a += currentArmorShown == 1 ? (fadeValue) : (-fadeValue);
                if (currentArmorShown == 1 && cArmor.a < 0.02f) {
                    currentArmorShown = 2;
                    curArmorShowDelay = 0f;
                } else if (currentArmorShown == 2 && cProtection.a < 0.02f) {
                    currentArmorShown = 1;
                    curArmorShowDelay = 0f;
                }
                imgArmor.color = cArmor;
                textArmor.color = cArmorText;
                imgProtection.color = cProtection;
                textProtection.color = cProtectionText;
            } else {
                curArmorShowDelay += Time.deltaTime;
            }
        }
        void Update() {
            if (goHighlight.activeSelf) {
                animateHighlightBreathing();
            }
            if (showProtection && showArmor) {
                AnimateArmorIconChanging();
            }
            if (!goPopup.activeSelf && popupQueue.Count > 0) {
                StartCoroutine(ShowPopup());
            }
        }
        public void ShowPopup(TilePopupAttributes atr) {
            if (popupQueue.Count == 0) {
                cBattle.RegisterAnimationInProgress();
            }
            popupQueue.Enqueue(atr);
        }
        protected IEnumerator ShowPopup() {
            goPopup.SetActive(true);
            TilePopupAttributes atr = popupQueue.Peek();//.Dequeue();
            imgPopup.sprite = atr.Image;
            imgPopup.color = Color.white;
            textPopup.text = atr.Text;
            textPopup.color = atr.TextColor;
            //textOutline.effectColor = atr.TextOutlineColor;
            yield return new WaitForSeconds(TilePopupAttributes.SHOW_DELAY);
            StartCoroutine(HidePopup());
        }
        protected IEnumerator HidePopup() {
            Color cImg = imgPopup.color, cText = textPopup.color;
            float fadeValue = TilePopupAttributes.FADE_SPEED * Time.deltaTime;
            cImg.a -= fadeValue;
            cText.a -= fadeValue;
            if (cImg.a > 0.02f) {
                imgPopup.color = cImg;
                textPopup.color = cText;
                yield return new WaitForSeconds(0.02f);
            }
            goPopup.SetActive(false);
            popupQueue.Dequeue();
            if (popupQueue.Count == 0) {
                cBattle.AnimationFinished();
                if (creature != null && creature.Health.Hp <= 0) {
                    cBattle.KillCreature(creature);
                }
            }
        }
        public void CreatureKilled() {
            Reset();
        }
        #endregion
    }
}
