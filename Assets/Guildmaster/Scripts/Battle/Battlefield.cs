﻿namespace Assets.Guildmaster.Scripts.Battle {
    using Mech;
    using UnityEngine;
    using System.Collections.Generic;

    public class Battlefield : MonoBehaviour {
        public BattleTile[,] bTiles;
        public const int gridSizeY = Party.MaxHeight * 2;
        public const int gridSizeX = Party.MaxWidth;

        public void Initialize(/*Party topParty, Party botParty, bool topAI, bool botAI*/) {
            var tiles = GetComponentsInChildren<BattleTile>(true);
            bTiles = new BattleTile[2 * Party.MaxHeight, Party.MaxWidth];
            gameObject.SetActive(true);
            
            for (int i = 0; i < Party.MaxHeight; ++i) {
                int atkIdx = (Party.MaxHeight - 1 - i);
                int atkOffset = atkIdx * Party.MaxWidth;
                int defIdx = (i + Party.MaxHeight);
                int defOffset = defIdx * Party.MaxWidth;
                for (int j = 0; j < Party.MaxWidth; ++j) {
                    //attackers
                    //var creature = topParty.GetCreature(j, i);
                    int idx = atkOffset + j;
                    bTiles[atkIdx, j] = tiles[idx];
                    bTiles[atkIdx, j].Initialize(new BattlePosition(j, atkIdx));
                    bTiles[atkIdx, j].Set(null/*creature == null ? null : new BattleCreature(creature, !topAI)*/);
                    //defenders
                    //creature = botParty.GetCreature(j, i);
                    idx = defOffset + j;
                    bTiles[defIdx, j] = tiles[idx];
                    bTiles[defIdx, j].Initialize(new BattlePosition(j, defIdx));
                    bTiles[defIdx, j].Set(null/*creature == null ? null : new BattleCreature(creature, !topAI)*/);
                }
            }
        }

        public void HideSelectionHighlight() {
            for (int i = 0; i < gridSizeY; ++i) {
                for (int j = 0; j < gridSizeX; ++j) {
                    bTiles[i, j].HideHighlight();
                }
            }
        }

        public BattleTile GetTile(BattlePosition pos) {
            return bTiles[pos.y, pos.x];
        }


        public List<BattleTile> GetTileRow(int idx) {
            List<BattleTile> row = new List<BattleTile>(gridSizeX);
            for (int i = 0; i < gridSizeX; ++i) {
                row.Add(bTiles[idx, i]);
            }
            return row;
        }

        public List<BattleTile> GetCharacterRow(int rowIdx, bool isOriginPlayer, TargetType filter = TargetType.Everyone) {
            List<BattleTile> row = new List<BattleTile>(gridSizeX);
            for (int i = 0; i < gridSizeX; ++i) {
                if (bTiles[rowIdx, i].Creature != null && (
                        filter == TargetType.Everyone ||
                        (filter & TargetType.Ally) > 0 && bTiles[rowIdx, i].Creature.IsPlayers == isOriginPlayer ||
                        (filter & TargetType.Enemy) > 0 && bTiles[rowIdx, i].Creature.IsPlayers != isOriginPlayer)) {
                    row.Add(bTiles[rowIdx, i]);
                }
            }
            return row;
        }

        /// <summary>
        /// Returns characters from battlefield according to given filter for source tile.
        /// </summary>
        /// <param name="origin">base tile to define Ally/Enemy side</param>
        /// <param name="filter">ALLY, ENEMY or EVERY</param>
        /// <returns></returns>
        public List<BattleTile> GetCharacters(BattleTile origin, TargetType filter = TargetType.Everyone) {
            List<BattleTile> selection = new List<BattleTile>();
            for (int i = 0; i < gridSizeY; ++i) {
                for (int j = 0; j < gridSizeX; ++j) {
                    if (bTiles[i, j].Creature != null && (
                        filter == TargetType.Everyone ||
                        filter == TargetType.Ally && bTiles[i, j].Creature.IsPlayers == origin.Creature.IsPlayers ||
                        filter == TargetType.Enemy && bTiles[i, j].Creature.IsPlayers != origin.Creature.IsPlayers)) {
                        selection.Add(bTiles[i, j]);
                    }
                }
            }
            return selection;
        }

        public List<BattleTile> GetMelee(BattleTile origin, TargetType filter = TargetType.Everyone) {
            int xMin = (origin.Pos.x > 0) ? (origin.Pos.x - 1) : (0);
            int yMin = (origin.Pos.y > 0) ? (origin.Pos.y - 1) : (0);
            int xMax = (origin.Pos.x < gridSizeX - 1) ? (origin.Pos.x + 2) : (gridSizeX);
            int yMax = (origin.Pos.y < gridSizeY - 1) ? (origin.Pos.y + 2) : (gridSizeY);
            List<BattleTile> selection = new List<BattleTile>();
            bool isPlayers = origin.Creature.IsPlayers;
            for (int i = yMin; i < yMax; ++i) {
                for (int j = xMin; j < xMax; ++j) {
                    BattleCreature creature = bTiles[i, j].Creature;
                    if (creature != null /*&& creature.IsAlive*/ && (
                            filter == TargetType.Everyone ||
                            filter == TargetType.MeleeAlly && isPlayers == creature.IsPlayers ||
                            filter == TargetType.MeleeEnemy && isPlayers != creature.IsPlayers)) {
                        selection.Add(bTiles[i, j]);
                    }
                }
            }
            //Get every enemy from front row if no targets was found
            if (selection.Count == 0) {
                selection = GetCharacterRow((isPlayers ? 1 : 2), isPlayers, filter);
            }
            if (selection.Count == 0) { //if no targets was found in the nearest tiles - check out the farest ones
                int[] y = { 0, gridSizeY - 1 };
                for (int i = 0; i < y.Length; ++i) {
                    for (int j = xMin; j < xMax; ++j) {
                        BattleCreature creature = bTiles[y[i], j].Creature;
                        if (creature != null /*&& creature.IsAlive*/ && (
                                filter == TargetType.Everyone ||
                                filter == TargetType.MeleeAlly && isPlayers == creature.IsPlayers ||
                                filter == TargetType.MeleeEnemy && isPlayers != creature.IsPlayers)) {
                            selection.Add(bTiles[y[i], j]);
                        }
                    }
                }
            }
            if (selection.Count == 0) {
                selection = GetCharacters(origin, filter);
            }
            return selection;
        }

        public List<BattleTile> GetMovementPossibilities(BattleCreature creature) {
            List<BattleTile> tiles = new List<BattleTile>();
            int yMin = creature.Position.IsPlayersPart ? (Party.MaxHeight) : 0;
            int yMax = creature.Position.IsPlayersPart ? (Party.MaxHeight << 1) : (Party.MaxHeight);
            int creatureSpeed = creature.ActionsLeft;
            for (int i = yMin; i < yMax; ++i) {
                for (int j = 0; j < Party.MaxWidth; ++j) {
                    if (LineDistanceBetweenTiles(new BattlePosition(j, i), creature.Position) <= creatureSpeed && bTiles[i, j].Creature == null) {
                        tiles.Add(bTiles[i, j]);
                    }
                }
            }
            return tiles;
        }

        public List<BattleTile> GetTiles(BattleTile origin, TargetType filter = TargetType.Everyone) {
            bool isPlayers = origin.Creature.IsPlayers;
            List<BattleTile> selection = new List<BattleTile>();
            int yMin = ((filter == TargetType.Enemy && isPlayers) || (filter == TargetType.Ally && !isPlayers)) ? (Party.MaxHeight) : 0;
            int yMax = ((filter == TargetType.Ally && isPlayers) || (filter == TargetType.Enemy && !isPlayers)) ? (Party.MaxHeight) : (Party.MaxHeight << 1);
            for (int i = yMin; i < yMax; ++i) {
                for (int j = 0; j < gridSizeX; ++j) {
                    if (bTiles[i, j].Creature == null) {
                        selection.Add(bTiles[i, j]);
                    }
                }
            }
            return selection;
        }

        public void MoveTileContent(BattleTile from, BattleTile to) {
            BattleCreature creature = from.Creature;
            to.Set(creature);
            from.Reset();
        }

        public static int LineDistanceBetweenTiles(BattlePosition a, BattlePosition b) {
            return Mathf.Abs(a.x - b.x) + Mathf.Abs(a.y - b.y);
        }
    }
}
