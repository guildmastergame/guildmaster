﻿namespace Assets.Guildmaster.Scripts.Battle {
    using Mech.Entities.Creatures;
    using Mech;
    using Mech.Abilities;
    using Mech.Abilities.Effects;
    using Mech.Attributes;
    using Mech.Battle;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    [Serializable]
    public class BattleCreature : ICreature {
        #region Fields
        protected Creature creature;
        protected Health health;
        protected Initiative ini;
        protected List<Effect> effects;
        protected List<AbilitySlot> abSlots;
        protected int actionsLeft;
        protected bool isPlayers;
        private int mainActionIdx;
        private bool waitingUsed;
        /// <summary>
        /// Reference to battle tile where creature located
        /// </summary>
        private BattleTile bTile;
        #endregion
        #region Accessors
        public string Name { get { return creature.Name.ToString(); } }
        public string Desc { get { return creature.Desc.ToString(); } }
        public Health Health { get { return health; } }
        public Initiative Ini { get { return ini; } }
        public List<Effect> Effects { get { return effects; } }
        public int ActionsLeft { get { return actionsLeft; } set { actionsLeft = value > 0 ? value : 0; } }
        public Sprite ImagePortrait { get { return creature.ImagePortrait; } }
        public bool IsPlayers { get { return isPlayers; } }
        public AbilityActive MainAbility { get { return mainActionIdx == -1 ? null : (abSlots[mainActionIdx].Ability as AbilityActive); } }
        public bool WaitingUsed { get { return waitingUsed; } set { waitingUsed = value; } }
        public BattlePosition Position { get { return bTile.Pos; } }
        public List<AbilitySlot> AbilitySlots { get { return abSlots; } }
        public BattleTile Tile { get { return bTile; } }
        #endregion
        #region Methods
        public BattleCreature(Creature creature, bool isPlayers) {
            this.creature = creature;
            this.isPlayers = isPlayers;
            var slots = creature.AbilitySlots;
            this.abSlots = new List<AbilitySlot>(slots.Count);
            effects = new List<Effect>();
            for (int i = 0; i < slots.Count; ++i) {
                this.abSlots.Add(slots[i]);
            }
            this.health = creature.Health; //no need to clone because Health is a struct
            this.ini = creature.Ini; //no need to clone because Initiative is a struct
            this.mainActionIdx = creature.GetMainActionIdx();
        }
        public void Heal(int hp) {
            this.health.Heal(hp);
            bTile.ShowPopup(new TilePopupAttributes(hp, 0, false, false, false, true, false));
            bTile.RefreshHealth();
        }
        /// <summary>
        /// Apply temporary effect to a creature
        /// </summary>
        /// <param name="e"></param>
        public void ApplyEffect(Effect e) {
            if (e.Type == EffectType.Freeze || e.Type == EffectType.Stun || e.Type == EffectType.Poison || e.Type == EffectType.Bleed) {
                //Try to refresh effect of same type
                for (int i = 0; i < effects.Count; ++i) {
                    if (effects[i].Type == e.Type) {
                        if (e.Life == 0) {
                            effects[i].Life = 0;
                            return;
                        } else if (effects[i].Life != 0) {
                            effects[i].Life = effects[i].Life + e.Life;
                            return;
                        }
                    }
                }
            }
            effects.Add(e);
            bTile.RefreshEffects();
        }
        /// <summary>
        /// Should be called only from BattleTile, when creature is moved or placed on the battlefield
        /// </summary>
        /// <param name="tile"></param>
        public void SetTile(BattleTile tile) {
            this.bTile = tile;
        }
        public List<T> GetPassiveEffectsOfType<T>(EffectType type) where T : Effect {
            List<T> efx = new List<T>();
            for (int i = 0; i < abSlots.Count; ++i) {
                var abEFx = abSlots[i].Ability.GetPassiveEffectsOfType<T>(type);
                for (int j = 0; j < abEFx.Count; ++j) {
                    efx.Add(abEFx[j]);
                }
            }
            return efx;
        }
        public List<T> GetTemporaryEffectsOfType<T>(EffectType type) where T : Effect {
            List<T> efx = new List<T>();
            for (int i = 0; i < effects.Count; ++i) {
                if (effects[i].Type == type) {
                    efx.Add(effects[i] as T);
                }
            }
            return efx;
        }
        public void ApplyDamage(int finalDamage, int dmgAbsorbedByArmor, DamageType type, bool isCritical) {
            health.ApplyDamage(finalDamage);
            bTile.ShowPopup(new TilePopupAttributes(finalDamage + dmgAbsorbedByArmor, type, false, false, isCritical, false, false));
            bTile.RefreshHealth();
        }
        /// <returns>Sum of values of all armors of given type</returns>
        public int GetCurrentArmor(DamageType armorType = DamageType.Physical) {
            int armor = 0;
            List<EffectArmor> armors = GetPassiveEffectsOfType<EffectArmor>(EffectType.Armor);
            for (int i = 0; i < armors.Count; ++i) {
                if (armors[i].Armor.Type == armorType) {
                    armor += armors[i].Armor.Value;
                }
            }
            List<EffectArmor> tempArmors = GetTemporaryEffectsOfType<EffectArmor>(EffectType.Armor);
            for (int i = 0; i < tempArmors.Count; ++i) {
                if (tempArmors[i].Armor.Type == armorType) {
                    armor += tempArmors[i].Armor.Value;
                }
            }
            return armor;
        }
        public void TurnStarted() {
            bTile.ShowHighlight(TileHighlightType.BLUE);
            bool isTurnPassed = false;
            for (int i = 0; i < effects.Count; ++i) {
                if (effects[i].Type == EffectType.Freeze || effects[i].Type == EffectType.Stun) {
                    isTurnPassed = true;
                }
                effects[i].TurnStarted(this);
            }
            if (isTurnPassed) {
                actionsLeft = 0;
            }
        }
        public void TurnFinished() {
            bTile.HideHighlight();
            for (int i = 0; i < effects.Count; ++i) {
                effects[i].TurnFinished(this);
            }
        }
        public void RoundStarted() {
            actionsLeft = creature.Actions;//Refresh creature actions count
            //Modify actions according to all modifiers
            var actionMods = GetPassiveEffectsOfType<EffectActionMod>(EffectType.ActionMod);
            for (int i = 0; i < actionMods.Count; ++i) {
                actionMods[i].Apply(bTile, bTile, AbilityType.Passive);
            }
            // Go through all temporary effect and tell them that round had started(cooldown reduction)
            for (int i = 0; i < effects.Count; ++i) {
                effects[i].RoundStarted(this);
            }
            //Update armor restoration
            var armors = GetPassiveEffectsOfType<EffectArmor>(EffectType.Armor);
            for (int i = 0; i < armors.Count; ++i) {
                armors[i].RestoreArmor();
            }
            Tile.Refresh();
        }
        public void RoundFinished() {
            for (int i = 0; i < effects.Count; ++i) {
                if(effects[i].RoundFinished(this)) {
                    effects.RemoveAt(i--); //Remove this effect. it's lifetime == 0
                }
            }
            DecreaseCooldowns();
            bTile.Refresh();
        }
        public void ActionCommited(AbilityType abilityType) {
            if ((abilityType & (AbilityType.Quick | AbilityType.Toggle)) == 0) {
                --actionsLeft;
            }
        }
        public void DecreaseCooldowns(int rounds = 1) {
            for (int i = 0; i < abSlots.Count; ++i) {
                abSlots[i].Ability.DecreaseCooldown(rounds);
            }
        }
        #endregion
    }
}
