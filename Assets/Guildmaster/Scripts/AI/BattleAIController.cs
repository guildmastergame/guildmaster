﻿namespace Assets.Guildmaster.Scripts.AI {
    using Mech.Battle;
    using Battle;
    using Control;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using Mech.Abilities;

    public class BattleAIController {
        protected BattleController bc;
        protected BattleTargetSelector ts;

        public const float TIME_BEFORE_TARGET_HIGHLIGHT = 0.1f;
        public const float TIME_BEFORE_TARGET_SELECT = 0.75f;
        public const float TIME_BEFORE_TARGET_CLICK = 2.0f;

        public BattleAIController(BattleController bc) {
            this.bc = bc;
            ts = new BattleTargetSelector(bc);
        }

        public void MakeTurn(BattleCreature creature) {
            //bc.EndTurn();
            if (creature.Health.Hp <= 0) {
                Debug.Log("I am already dead!");
                return;
            } else if (creature.ActionsLeft < 1) {
                Debug.Log("Creature has no actions left: " + creature.ToString());
            }
            var ab = creature.AbilitySlots[0].Ability as AbilityActive;
            if (ab != null && ab.Available(creature.Position.Type)) {
                var param = new BattleTargetSelector.TargetSelectionParameters(creature.Tile, ab, ab.Target);
                if (!ts.Select(ref param)) {
                    bc.ChangeMode(PlayerBattleMode.TARGET_SELECTION);
                    //targets weren't chosen automatically. Lets choose them randomly
                    if (param.allowed.Count == 0) {
                        Debug.Log("No targets awailable");
                        bc.EndTurn();
                        return;
                    }
                    BattleTile chosenTile = param.allowed[Random.Range(0, param.allowed.Count)];
                    bc.StartCoroutine(HighlightTargetsCoroutine(param, TIME_BEFORE_TARGET_HIGHLIGHT));
                    bc.StartCoroutine(SelectTargetsCoroutine(chosenTile, param, TIME_BEFORE_TARGET_SELECT));
                    bc.StartCoroutine(ClickTargetCoroutine(chosenTile, param, TIME_BEFORE_TARGET_CLICK));
                } else {
                    bc.StartCoroutine(UseAbilityCoroutine(param, TIME_BEFORE_TARGET_CLICK));
                }
            } else {
                Debug.Log("Action is unavailable");
                bc.EndTurn();
                return;
            }

        }

        protected IEnumerator HighlightTargetsCoroutine(BattleTargetSelector.TargetSelectionParameters param, float delay) {
            yield return new WaitForSeconds(delay);
            ts.HighlightTilesAllowedToSelect(param);
        }

        protected IEnumerator SelectTargetsCoroutine(BattleTile chosenTile, BattleTargetSelector.TargetSelectionParameters param, float delay) {
            yield return new WaitForSeconds(delay);
            bc.TileCursorEnter(chosenTile, param);
        }

        protected IEnumerator ClickTargetCoroutine(BattleTile chosenTile, BattleTargetSelector.TargetSelectionParameters param, float delay) {
            yield return new WaitForSeconds(delay);
            ts.TileClicked(chosenTile, ref param);
            bc.ApplyAbilityToTargets(param);
        }

        protected IEnumerator UseAbilityCoroutine(BattleTargetSelector.TargetSelectionParameters param, float delay) {
            yield return new WaitForSeconds(delay);
            bc.ApplyAbilityToTargets(param);
        }
    }
}
