﻿namespace Assets.Guildmaster.Scripts.Editor {
    using Mech.Items;
    using Mech;
    using System.Linq;
    using System.Collections.Generic;
    using UnityEditor;
    using UnityEngine;
    using Mech.Abilities;
    using Utility;
    using System.Runtime.Serialization.Formatters.Binary;
    using System.IO;

    public class CreatureEditor : EditorWindow {
        #region Fields
        private static CreatureEditor instance = null;
        private const float SelectionListWidth = 300f;
        private Vector2 infoScroll;
        private Vector2 listScroll;
        private List<Creature> creatures;
        private int selectedIdx = -1;
        private string newKey = string.Empty;
        private Sprite tempSprite;
        private Texture2D textureEmpty = new Texture2D(1, 1);
        #endregion

        #region Accessors
        private float InfoAreaWidth { get; set; }
        #endregion

        #region Methods
        [MenuItem("Guildmaster/Creature Editor")]
        public static void Open() {
            if (instance != null) {
                instance.Close();
                instance = null;
            }
            instance = CreateInstance<CreatureEditor>();
            instance.Initialize();
            instance.Show();
        }

        private void Initialize() {
            if (!SpriteVault.IsInitialized) {
                SpriteVault.Initialize();
            }
            LoadFromFile(CreatureVault.DefaultFilePath);
            selectedIdx = -1;
        }

        private void LoadFromFile(string filepath) {
            if (!File.Exists(filepath)) {
                creatures = new List<Creature>();
                return;
            }
            BinaryFormatter bformatter = new BinaryFormatter();
            bformatter.Binder = new VersionDeserializationBinder();
            var stream = new FileStream(filepath, FileMode.Open);
            creatures = bformatter.Deserialize(stream) as List<Creature>;
            if(creatures == null) {
                creatures = new List<Creature>();
            }
            stream.Close();
        }

        private void SaveToFile(string filepath) {
            if (filepath == string.Empty)
                return;
            BinaryFormatter bformatter = new BinaryFormatter();
            bformatter.Binder = new VersionDeserializationBinder();
            var stream = new FileStream(filepath, FileMode.Create);
            bformatter.Serialize(stream, creatures);
            stream.Close();
        }

        private void DrawCreatureList() {
            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("Save")) {
                SaveToFile(CreatureVault.DefaultFilePath);
            }
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.BeginHorizontal();
            newKey = EditorGUILayout.TextField("New key:", newKey);
            if (GUILayout.Button("Create") && !creatures.Any(x => x.CreatureKey == newKey)) {
                selectedIdx = creatures.Count;
                creatures.Add(new Creature(newKey));
            }
            EditorGUILayout.EndHorizontal();
            for(int i = 0; i < creatures.Count; ++i) {
                EditorGUILayout.BeginHorizontal();
                if (GUILayout.Button("-", GUILayout.Width(30f), GUILayout.Height(48f))) {
                    if(selectedIdx >= i) {
                        --selectedIdx;
                    }
                    creatures.RemoveAt(i--);
                    continue;
                }
                GUILayout.Box(creatures[i].TexturePortrait, GUILayout.Width(48f), GUILayout.Height(48f));
                if (GUILayout.Button(creatures[i].CreatureKey, GUILayout.Height(48f))) {
                    selectedIdx = i;
                }
                EditorGUILayout.EndHorizontal();
            }
        }

        private void DrawCreatureInfo(Creature c) {
            EditorGUILayout.BeginHorizontal();
            tempSprite = c.ImagePortrait;
            Texture2D texture = tempSprite == null ? null : tempSprite.texture;
            texture = EditorGUILayout.ObjectField(texture, typeof(Texture2D), false, GUILayout.Width(150f), GUILayout.Height(100f)) as Texture2D;
            if (texture != null && texture.name != c.ImageKey) {
                c.ImageKey = texture.name;
            }
            EditorGUILayout.BeginVertical();
            EditorGUILayout.LabelField("Creature Key", c.CreatureKey);
            c.NameKey = EditorGUILayout.TextField("Name Key", c.NameKey);
            c.DescKey = EditorGUILayout.TextField("Desc Key", c.DescKey);
            EditorGUILayout.EndVertical();
            EditorGUILayout.EndHorizontal();

            var hp = c.Health;
            EditorGUILayout.BeginHorizontal();
            hp.Hp = EditorGUILayout.IntField("HP", hp.Hp);
            hp.MaxHp = EditorGUILayout.IntField("Max HP", hp.MaxHp);
            EditorGUILayout.EndHorizontal();
            c.Health = hp;
            var ini = c.Ini;
            ini.Value = EditorGUILayout.FloatField("Initiative", ini.Value);
            c.Ini = ini;
            var slots = c.AbilitySlots;
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.BeginVertical(GUILayout.Width(InfoAreaWidth / 2));
            for (int i = 0; i < slots.Count; ++i) {
                if(i == 3) {
                    EditorGUILayout.EndVertical();
                    EditorGUILayout.BeginVertical(GUILayout.Width(InfoAreaWidth / 2));
                }
                EditorGUILayout.BeginHorizontal();
                if (GUILayout.Button("-", GUILayout.Width(30f))) {
                    slots.RemoveAt(i--);
                    continue;
                }
                slots[i].AllowedItems = (ItemType)(EditorGUILayout.EnumPopup(new GUIContent("Allowed Items"), (EditorItemFilterType)slots[i].AllowedItems));
                EditorGUILayout.EndHorizontal();
                //Default Ability
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Default Ability", slots[i].DefaultAbility == null ? "none" : slots[i].DefaultAbility.NameKey);
                tempSprite = slots[i].DefaultAbility == null ? null : slots[i].DefaultAbility.Image;
                GUILayout.Box(tempSprite == null ? textureEmpty : SpriteUtility.GetTexture(tempSprite), GUILayout.Width(64f), GUILayout.Height(64f));
                EditorGUILayout.EndHorizontal();
                EditorGUILayout.BeginHorizontal();
                if (GUILayout.Button("New")) {
                    AbilityEditor.Open(new AbilityPassive(), slots[i].AbilityChanged);
                }
                if (slots[i].DefaultAbility != null && GUILayout.Button("Edit")) {
                    AbilityEditor.Open(slots[i].DefaultAbility, slots[i].AbilityChanged);
                }
                if (GUILayout.Button("Reset")) {
                    slots[i].AbilityChanged(null);
                }
                EditorGUILayout.EndHorizontal();
                //Item
                if (slots[i].AllowedItems != 0) {
                    EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField("Equiped Item", slots[i].Item == null ? "none" : slots[i].Item.NameKey);
                    tempSprite = slots[i].Item == null ? null : slots[i].Item.Image;
                    GUILayout.Box(tempSprite == null ? textureEmpty : SpriteUtility.GetTexture(tempSprite), GUILayout.Width(64f), GUILayout.Height(64f));
                    EditorGUILayout.EndHorizontal();
                    EditorGUILayout.BeginHorizontal();
                    if (GUILayout.Button("Select")) {
                        SelectItemEditorPopup.Init(slots[i].ItemChanged, (EditorItemFilterType)slots[i].AllowedItems);
                    }
                    if (slots[i].Item != null && GUILayout.Button("Edit")) {
                        ItemEditor.Open(slots[i].Item.ItemKey, slots[i].ItemChanged);
                    }
                    if (GUILayout.Button("Reset")) {
                        slots[i].ItemChanged(null);
                    }
                    EditorGUILayout.EndHorizontal();
                } else {
                    slots[i].Item = null;
                }
                EditorGUILayout.Space();
            }
            EditorGUILayout.EndVertical();
            EditorGUILayout.EndHorizontal();
            if(slots.Count < 6 && GUILayout.Button("Add Slot")) {
                slots.Add(new AbilitySlot(null));
            }
        }

        void OnGUI() {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.BeginVertical(GUILayout.Width(SelectionListWidth));
            listScroll = EditorGUILayout.BeginScrollView(listScroll, false, false);
            DrawCreatureList();
            EditorGUILayout.EndScrollView();
            EditorGUILayout.EndVertical();
            if (selectedIdx >= 0) {
                InfoAreaWidth = position.width - 20f - SelectionListWidth;
                infoScroll = EditorGUILayout.BeginScrollView(infoScroll, false, false, GUILayout.Width(InfoAreaWidth + 20f));
                DrawCreatureInfo(creatures[selectedIdx]);
                EditorGUILayout.EndScrollView();
            }
            EditorGUILayout.EndHorizontal();
        }

        void OnDestroy() {
            //SaveToFile(CreatureVault.DefaultFilePath);
        }
        #endregion
    }
}
