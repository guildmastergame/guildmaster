﻿using Assets.Guildmaster.Scripts;
using Assets.Guildmaster.Scripts.Mech;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class SelectCreatureEditorPopup : EditorWindow {
    #region Fields
    private const float WindowWidth = 600f;
    private const float WindowHeight = 400f;
    private const float CreatureIconSize = 64f;
    private SetCreatureCallback callback = null;
    private Vector2 scrollList;
    private List<Creature> creatures;
    private static Texture2D TextureEmpty = new Texture2D(1, 1);
    private int partyPosX, partyPosY;
    private Creature selectedCreature;
    public delegate void SetCreatureCallback(Creature creature, int partyPosX, int partyPosY);
    #endregion

    #region Methods
    public static void Init(SetCreatureCallback callback, int partyPosX, int partyPosY, string selectedCreatureKey = null) {
        var window = CreateInstance<SelectCreatureEditorPopup>();
        CreatureVault.Initialize();
        window.creatures = CreatureVault.Creatures;
        window.selectedCreature = selectedCreatureKey!= null ? CreatureVault.Get(selectedCreatureKey) : null;
        window.callback = callback;
        window.partyPosX = partyPosX;
        window.partyPosY = partyPosY;
        window.position = new Rect(Screen.width / 2, Screen.height / 2, WindowWidth, WindowHeight);
        window.titleContent = new GUIContent("Select creature");
        window.ShowUtility();
    }

    private void ApplyChanges() {
        if(callback != null && callback.Target != null) {
            callback(selectedCreature, partyPosX, partyPosY);
        }
    }



    private void DrawItemInfo() {
        if (selectedCreature == null) {
            return;
        }
        EditorGUILayout.BeginHorizontal();
        GUILayout.Box(selectedCreature.TexturePortrait, GUILayout.Width(150f), GUILayout.Height(150f));
        EditorGUILayout.BeginVertical();
        EditorGUILayout.LabelField("Item Key", selectedCreature.CreatureKey);
        EditorGUILayout.LabelField("Name Key", selectedCreature.NameKey);
        EditorGUILayout.LabelField("Desc Key", selectedCreature.DescKey);
        EditorGUILayout.EndVertical();
        EditorGUILayout.EndHorizontal();
        if (GUILayout.Button("Select")) {
            ApplyChanges();
            Close();
        }
    }

    private void SelectCreature(int idx) {
        selectedCreature = creatures[idx];
    }

    private void DrawItemList() {
        EditorGUILayout.BeginHorizontal();
        int itemsPerLine = (int)(position.width / CreatureIconSize) - 1;
        if (GUILayout.Button("None", GUILayout.Width(CreatureIconSize), GUILayout.Height(CreatureIconSize))) {
            selectedCreature = null;
            ApplyChanges();
            Close();
        }
        for (int i = 1; i <= creatures.Count; ++i) {
            if (i % itemsPerLine == 0) {
                EditorGUILayout.EndHorizontal();
                EditorGUILayout.BeginHorizontal();
            }
            if (GUILayout.Button(creatures[i-1].TexturePortrait, GUILayout.Width(CreatureIconSize), GUILayout.Height(CreatureIconSize))) {
                SelectCreature(i-1);
            }
        }
        EditorGUILayout.EndHorizontal();
    }

    void OnGUI() {
        EditorGUILayout.BeginVertical();
        EditorGUILayout.LabelField("Select creature");
        scrollList = EditorGUILayout.BeginScrollView(scrollList, GUILayout.Height(250f));
        DrawItemList();
        EditorGUILayout.EndScrollView();
        EditorGUILayout.LabelField("Creature info");
        DrawItemInfo();
        EditorGUILayout.EndVertical();
    }

    void OnLostFocus() {
        this.Close();
    }
    #endregion
}
