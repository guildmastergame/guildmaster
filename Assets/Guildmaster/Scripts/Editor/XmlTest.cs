﻿//using System.Collections.Generic;
//using UnityEngine;
//using UnityEditor;
//using System.Xml;
//using System.Xml.Serialization;
//using System.IO;

//namespace Assets.Guildmaster.Scripts.Editor {

//    public class QuestContainerTypeA : QuestContainer {
//        public bool aData;

//        public QuestContainerTypeA() {
//            aData = true;
//            name = "container A";
//            type = QuestType.TypeA;
//        }

//    }

//    public class QuestContainerTypeB : QuestContainer {
//        public float bData;

//        public QuestContainerTypeB() {
//            bData = 14.88f;
//            type = QuestType.TypeB;
//        }
//    }

//    public enum QuestType {
//        TypeA,
//        TypeB
//    }
//    [XmlInclude(typeof(QuestContainerTypeA)), XmlInclude(typeof(QuestContainerTypeB))]
//    public class QuestContainer {
//        [XmlAttribute("id")]
//        public int id;
//        public string name;
//        public QuestType type;
//    }

//    [XmlRoot("QuestCollection")]
//    public class QuestTreeContainer {
//        [XmlArray("Quests"), XmlArrayItem("Quest")]
//        public List<QuestContainer> quests = new List<QuestContainer>();

//        public QuestTreeContainer() {
//            quests.Add(new QuestContainerTypeA());
//            quests.Add(new QuestContainerTypeB());
//        }

//        public static QuestTreeContainer Deserialize(string filepath) {
//            var serializer = new XmlSerializer(typeof(QuestTreeContainer));
//            var stream = new FileStream(filepath, FileMode.Open);
//            var container = serializer.Deserialize(stream) as QuestTreeContainer;
//            stream.Close();
//            return container;
//        }

//        public void Serialize(string filepath) {
//            var serializer = new XmlSerializer(typeof(QuestTreeContainer));
//            var stream = new FileStream(filepath, FileMode.Create);
//            serializer.Serialize(stream, this);
//            stream.Close();
//        }
//    }

//    public class XmlTest {
//        const string filepath = "test.xml";

//        [MenuItem("XmlTest/Save")]
//        static void Save() {
//            QuestTreeContainer qtc = new QuestTreeContainer();
//            qtc.Serialize(filepath);
//        }
//        [MenuItem("XmlTest/Load")]
//        static void Load() {
//            var qtc = QuestTreeContainer.Deserialize(filepath);
//            Debug.Log("OK");
//        }
//    }
//}
