﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using Assets.Guildmaster.Scripts.Utility;
using Assets.Guildmaster.Scripts.Mech.Quests.Dialogues;
using System.IO;
using System;
using System.Runtime.Serialization.Formatters.Binary;

public class DialogueBuilder : EditorWindow {
    private enum MenuActionEnum {
        AddInfoNode,
        DeleteNode
    }

    [Serializable]
    public class DialogueContainer {
        public float panX;
        public float panY;
        public List<DialogueBuilderNode.DialogueBuilderNodeContainer> nodes;

        public DialogueContainer() {
            panX = 0;
            panY = 0;
            nodes = null;
        }

        public DialogueContainer(DialogueBuilder db) {
            panX = db.panX;
            panY = db.panY;
            nodes = new List<DialogueBuilderNode.DialogueBuilderNodeContainer>(db.windows.Count);
            for(int i = 0; i < db.windows.Count; ++i) {
                nodes.Add(new DialogueBuilderNode.DialogueBuilderNodeContainer(db.windows[i]));
            }
        }

        public void InitializeDialogueBuilder(DialogueBuilder db) {
            db.panX = panX;
            db.panY = panY;
            db.windows = new List<DialogueBuilderNode>(nodes.Count);
            for(int i = 0; i < nodes.Count; ++i) {
                db.windows.Add(new DialogueBuilderNode(nodes[i]));
            }
            db.Repaint();
        }
    }

    static DialogueBuilder window;

    public Rect _handleArea;
    private bool _nodeOption, _options, _handleActive, _action;
    private static Texture2D _resizeHandle, _aaLine;
    private GUIContent _icon;
    private float _winMinX, _winMinY;
    private int _mainwindowID;

    private Vector2 mousePos;
    private bool makeTransitionMode = false;
    private float panX;
    private float panY;
    private List<DialogueBuilderNode> windows = new List<DialogueBuilderNode>();
    private DialogueBuilderNode selectedNode = null;
    private DialogueChoice selectedDialogueChoice = null;
    private const float PanMaxX = 10000f;
    private const float PanMaxY = 10000f;
    private bool scrollWindow = false;
    private Vector2 scrollStartMousePos;
    private Vector2 scrollStartPan;
    private int deleteNodeWithIdx = -1;
    private string filepath;

    //[MenuItem("Guildmaster/Dialogue Builder")]
    static void Init(string filepath) {
        if(!Language.IsInitialized()) {
            Language.Initialize();
        }
        window = GetWindow<DialogueBuilder>(typeof(DialogueBuilder));
        window.titleContent = new GUIContent("Dialogue Builder");
        window.ShowNodes();
        window.filepath = filepath;
    }

    [MenuItem("Guildmaster/Dialogue Builder/New Dialogue...")]
    public static string NewDialogue() {
        string path = EditorUtility.SaveFilePanel("Create new dialogue", "", "Dialogue", "dlg");
        Init(path);
        window.SaveDialogue();
        return path;
    }

    [MenuItem("Guildmaster/Dialogue Builder/Open Dialogue...")]
    public static string OpenDialogue() {
        string path = EditorUtility.OpenFilePanel("Open dialogue file", "", "dlg");
        if (path != string.Empty) {
            OpenDialogue(path);
            return path;
        }
        return null;
    }

    public static string GetDialogueFilepath() {
        string path = EditorUtility.OpenFilePanel("Open dialogue file", "", "dlg");
        if (path != string.Empty) {
            return path;
        }
        return null;
    }

    public static void OpenDialogue(string filepath) { 
        Init(filepath);
        var c = LoadDialogue(filepath);
        c.InitializeDialogueBuilder(window);
        window.Repaint();
    }

    private void SaveDialogue() {
        if (filepath == string.Empty)
            return;
        DialogueContainer c = new DialogueContainer(this);
        BinaryFormatter bformatter = new BinaryFormatter();
        bformatter.Binder = new VersionDeserializationBinder();
        var stream = new FileStream(filepath, FileMode.Create);
        bformatter.Serialize(stream, c);
        stream.Close();
    }

    public static DialogueContainer LoadDialogue(string filepath) {
        if (filepath == string.Empty)
            return null;
        if(!File.Exists(filepath)) {
            Debug.LogError(string.Format("Dialogue file path doesn't exist: {0}", filepath));
            return null;
        }
        BinaryFormatter bFormatter = new BinaryFormatter();
        bFormatter.Binder = new VersionDeserializationBinder();
        var stream = new FileStream(filepath, FileMode.Open);
        DialogueContainer c = bFormatter.Deserialize(stream) as DialogueContainer;
        stream.Close();
        return c;
    }

    public static void StartTransition(DialogueBuilderNode node, DialogueChoice c) {
        window.makeTransitionMode = true;
        window.selectedNode = node;
        window.selectedDialogueChoice = c;
    }

    private void ShowNodes() {
        _winMinX = 300f;
        _winMinY = 400f;
        windows = new List<DialogueBuilderNode>();
        windows.Add(new DialogueBuilderNode(0, new Rect(30, 30, _winMinX, _winMinY)));
        windows.Add(new DialogueBuilderNode(1, new Rect(410, 210, _winMinX, _winMinY)));

        _resizeHandle = AssetDatabase.LoadAssetAtPath("Assets/Resources/EditorIcons/resizeHandle.png", typeof(Texture2D)) as Texture2D;
        _aaLine = AssetDatabase.LoadAssetAtPath("Assets/NodeEditor/Icons/aa1x5.png", typeof(Texture2D)) as Texture2D;
        _icon = new GUIContent(_resizeHandle);
        _mainwindowID = GUIUtility.GetControlID(FocusType.Native); //grab primary editor window controlID
    }

    private void MenuActionCallback(object obj) {
        switch ((MenuActionEnum)obj) {
            case MenuActionEnum.AddInfoNode:
                windows.Add(new DialogueBuilderNode(windows.Count, new Rect(mousePos.x - panX - 150f, mousePos.y - panY - 200f, 300, 400)));
                break;
            case MenuActionEnum.DeleteNode:
                if(deleteNodeWithIdx > -1) {
                    windows.RemoveAt(deleteNodeWithIdx);
                    for (int i = 0; i < windows.Count; ++i) {
                        windows[i].NodeDeleted(deleteNodeWithIdx);
                    }
                    deleteNodeWithIdx = -1;
                }
                break;
        }
    }

    private int FindWindowAboveMouse(Vector2 mousePos) {
        Rect r;
        for (int i = 0; i < windows.Count; ++i) {
            r = windows[i].Rect;
            r.x += panX; r.y += panY;
            if (r.Contains(mousePos)) {
                return i;
            }
        }
        return -1;
    }

    void OnGUI() {
        DialogueBuilderNode.InitializeStyles();
        Event e = Event.current;
        mousePos = e.mousePosition;

        GUI.color = new Color(0, 0, 0, 0.7f);
        GUI.Box(new Rect(panX, panY, PanMaxX, PanMaxY), "");
        GUI.color = Color.white;

        //UI
        if (GUI.Button(new Rect(10, 10, 80, 25), "Save")) {
            SaveDialogue();
        }

        if (e.button == 1 && !makeTransitionMode) {
            if (e.type == EventType.MouseDown) {
                
                int selectedIdx = FindWindowAboveMouse(mousePos);

                //for (int i = 0; i < windows.Count; ++i) {
                //    if (windows[i].Rect.Contains(mousePos)) {
                //        selectedIdx = i;
                //        break;
                //    }
                //}
                if (selectedIdx == -1) {//Empty space clicked
                    GenericMenu menu = new GenericMenu();
                    menu.AddItem(new GUIContent("Add Info Node"), false, MenuActionCallback, MenuActionEnum.AddInfoNode);
                    menu.ShowAsContext();
                } else {
                    GenericMenu menu = new GenericMenu();
                    menu.AddItem(new GUIContent("Delete Node"), false, MenuActionCallback, MenuActionEnum.DeleteNode);
                    deleteNodeWithIdx = selectedIdx;
                    menu.ShowAsContext();
                }
                e.Use();
            }
        } else if (e.button == 0 && e.type == EventType.MouseDown && makeTransitionMode) {
            int selectedIndex = FindWindowAboveMouse(mousePos);
            if (selectedIndex == -1) { //no window clicked
                makeTransitionMode = false; //stop transition
                selectedNode = null;
            } else if (!windows[selectedIndex].Equals(selectedNode)) {
                //if there is a click on a window and it's not the window that the transition started from
                selectedNode.SetChoiceData(selectedDialogueChoice, windows[selectedIndex]);
                makeTransitionMode = false;
                selectedNode = null;
                selectedDialogueChoice = null;
            }
            e.Use();
        } else if (e.button == 0 && e.type == EventType.MouseDown && !makeTransitionMode) {
            //if there is a left click and we are not in a transition mode
            int selectedIndex = FindWindowAboveMouse(mousePos);
            if (selectedIndex != -1) {

            }
        }
        
        GUI.BeginGroup(new Rect(panX, panY, PanMaxX, PanMaxY));
        //Draw curves for every node
        for (int i = 0; i < windows.Count; ++i) {
            windows[i].DrawCurves();
        }

        BeginWindows();
        for (int i = 0; i < windows.Count; i++) {
            windows[i].Rect = GUI.Window(i, windows[i].Rect, DrawNodeWindow, windows[i].WindowTitle);
        }
        EndWindows();
        GUI.EndGroup();

        //if we are in a transition mode and there is a selected node
        if (makeTransitionMode && selectedNode != null) {
            //draw the curve from the selected node to the mouse position
            Rect mouseRect = new Rect(e.mousePosition.x, e.mousePosition.y, 10, 10);
            Rect source = selectedNode.Rect;
            source.x += panX;
            source.y += panY;
            DrawNodeCurve(source, mouseRect, Color.blue);
            Repaint();
        }
        //if drag extends inner window bounds _handleActive remains true as event gets lost to parent window
        if ((Event.current.rawType == EventType.MouseUp) && (GUIUtility.hotControl != _mainwindowID)) {
            GUIUtility.hotControl = 0;
        }
        if (e.button == 2) {
            if (e.type == EventType.MouseDown) {
                scrollStartMousePos = e.mousePosition;
                scrollStartPan = new Vector2(panX, panY);
                scrollWindow = true;
            } else if (e.type == EventType.MouseUp) {
                scrollWindow = false;
            }
        }
        if (scrollWindow) {
            Vector2 mouseDiff = e.mousePosition - scrollStartMousePos;
            panX = Mathf.Clamp(scrollStartPan.x + mouseDiff.x, position.width - 100 - PanMaxX, 100);
            panY = Mathf.Clamp(scrollStartPan.y + mouseDiff.y, position.height - 100 - PanMaxY, 100);
            Repaint();
        }
    }

    private void DrawNodeWindow(int id) {
        if (GUIUtility.hotControl == 0) {  //mouseup event outside parent window?
            _handleActive = false; //make sure handle is deactivated
        }
        float _cornerX = windows[id].Rect.width;
        float _cornerY = windows[id].Rect.height;
        GUILayout.BeginArea(new Rect(1, 16, _cornerX - 3, _cornerY - 10));
        if(windows[id].DrawWindow()) {
            Repaint();
            return;
        }
        GUILayout.EndArea();
        GUILayout.BeginArea(new Rect(1, _cornerY - 16, _cornerX - 3, 14));
        GUILayout.BeginHorizontal(EditorStyles.toolbarTextField, GUILayout.ExpandWidth(true));
        GUILayout.FlexibleSpace();
        //grab corner area based on content reference
        _handleArea = GUILayoutUtility.GetRect(_icon, GUIStyle.none);
        GUI.DrawTexture(new Rect(_handleArea.xMin + 6, _handleArea.yMin - 3, 20, 20), _resizeHandle); //hacky placement
        _action = (Event.current.type == EventType.MouseDown) || (Event.current.type == EventType.MouseDrag);
        if (!_handleActive && _action) {
            if (_handleArea.Contains(Event.current.mousePosition, true)) {
                _handleActive = true; //active when cursor is in contact area
                GUIUtility.hotControl = GUIUtility.GetControlID(FocusType.Native); //set handle hot
            }
        }
        EditorGUIUtility.AddCursorRect(_handleArea, MouseCursor.ResizeUpLeft);
        GUILayout.EndHorizontal();
        GUILayout.EndArea();
        //resize window
        if (_handleActive && (Event.current.type == EventType.MouseDrag)) {
            ResizeNode(id, Event.current.delta.x, Event.current.delta.y);
            Repaint();
            Event.current.Use();
        }
        //enable drag for node
        if (!_handleActive) {
            GUI.DragWindow();
        }
    }

    private void ResizeNode(int id, float deltaX, float deltaY) {
        var r = windows[id].Rect;
        if ((r.width + deltaX) > 200) {
            r.xMax += deltaX;
        }
        if ((r.height + deltaY) > 200) {
            r.yMax += deltaY;
        }
        windows[id].Rect = r;
    }

    public static void DrawNodeCurve(Rect start, Rect end, Color? color = null) {
        Vector3 startPos = new Vector3(start.x + start.width, start.y + start.height / 2, 0);
        Vector3 endPos = new Vector3(end.x, end.y + end.height / 2, 0);
        Vector3 startTan = startPos + Vector3.right * 50;
        Vector3 endTan = endPos + Vector3.left * 50;
        Handles.DrawBezier(startPos, endPos, startTan, endTan, color.HasValue ? color.Value : Color.black, _aaLine, 5f);
    }

    public static DialogueBuilderNode GetNode(int idx) {
        return window.windows[idx];
    }
}
