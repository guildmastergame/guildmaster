﻿using Assets.Guildmaster.Scripts.Mech.Quests.Dialogues;
using Assets.Guildmaster.Scripts.Utility;
using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;


public class DialogueBuilderNode : ScriptableObject {
    [Serializable]
    public class DialogueBuilderNodeContainer {
        public SerializableRect windowRect;
        public string title;
        public DialogueNodeType type;
        public int id;
        public string text;
        public List<DialogueChoice> choices = new List<DialogueChoice>();

        public DialogueBuilderNodeContainer() {

        }

        public DialogueBuilderNodeContainer(DialogueBuilderNode node) {
            this.windowRect = new SerializableRect(node.Rect);
            this.title = node.Title;
            this.type = node.Type;
            this.id = node.Id;
            this.text = node.Text;
            this.choices = node.choices;
        }
    }
    private static GUIStyle TextAreaStyle = null;
    private static GUIStyle LabelStyle = null;
    protected Rect windowRect;
    protected string title;
    protected DialogueNodeType type;
    protected int id;
    protected string text;
    protected List<DialogueChoice> choices = new List<DialogueChoice>();
    protected List<Rect> choiceRects = new List<Rect>();

    public Rect Rect { get { return windowRect; } set { windowRect = value; } }
    public string WindowTitle { get { return string.Format("{0}[#{1}]", title, id); } }
    public string Title { get { return title; } }
    public int Id { get { return id; } }
    public string Text { get { return text; } }
    public DialogueNodeType Type { get { return type; } }

    private void ChangeChoiceType(int idx, DialogueChoiceType type) {
        switch (type) {
            case DialogueChoiceType.Info:
                choices[idx] = new DialogueChoiceInfo();
                break;
            case DialogueChoiceType.Check:
                choices[idx] = new DialogueChoiceCheck();
                break;
            case DialogueChoiceType.Chance:
                choices[idx] = new DialogueChoiceChance();
                break;
        }
    }

    private float DrawChoiceInfo(DialogueChoiceInfo c, int choiceIdx) {
        float wh = 0f; //window height for auto height recalculation
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Next Node", c.NextNodeIdx == (-1) ? "None" : string.Format("#{0}", c.NextNodeIdx));
        if(GUILayout.Button("Set", GUILayout.Width(50f))) {
            DialogueBuilder.StartTransition(this, c);
        }
        if (Event.current.type == EventType.Repaint) {
            var r = GUILayoutUtility.GetLastRect();
            wh += r.height;
            r.x += windowRect.x;
            r.y += windowRect.y;
            choiceRects[choiceIdx] = r;
        }
        EditorGUILayout.EndHorizontal();
        return wh;
    }

    private float DrawChoiceCheck(DialogueChoiceCheck c, int idx) {
        return 0;
    }

    private float DrawChoiceChance(DialogueChoiceChance c, int idx) {
        return 0;
    }

    private float DrawChoice(int idx) {
        switch (choices[idx].Type) {
            case DialogueChoiceType.Info:
                return DrawChoiceInfo(choices[idx] as DialogueChoiceInfo, idx);
            case DialogueChoiceType.Check:
                return DrawChoiceCheck(choices[idx] as DialogueChoiceCheck, idx);
            case DialogueChoiceType.Chance:
                return DrawChoiceChance(choices[idx] as DialogueChoiceChance, idx);
            default:
                return 0f;
        }
    }

    private void RecalculateWindowHeight(ref float wh) {
        if (Event.current.type == EventType.Repaint) {
            wh += GUILayoutUtility.GetLastRect().height;
        }
    }

    /// <returns>True if repaint needed</returns>
    public bool DrawWindow() {
        float wh = 50f; //window height
        title = EditorGUILayout.TextField("Title", title);
        RecalculateWindowHeight(ref wh);
        type = (DialogueNodeType) EditorGUILayout.EnumPopup("Type", type);
        RecalculateWindowHeight(ref wh);
        text = EditorGUILayout.TextArea(text, TextAreaStyle, GUILayout.Width(windowRect.width), GUILayout.Height(150f));
        RecalculateWindowHeight(ref wh);
        for (int i = 0; i < choices.Count; ++i) {
            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("-", GUILayout.Width(25))) {
                choices.RemoveAt(i--);
                EditorGUILayout.EndHorizontal();
                windowRect.height -= 75f;
                return true;
            }
            choices[i].Text.Key = EditorGUILayout.TextField(new GUIContent("TextKey"), choices[i].Text.Key);
            EditorGUILayout.EndHorizontal();
            RecalculateWindowHeight(ref wh);
            wh += 8;
            EditorGUILayout.LabelField(choices[i].Text.ToString(), GUILayout.Width(windowRect.width - 5));
            RecalculateWindowHeight(ref wh);
            var newType = (DialogueChoiceType)EditorGUILayout.EnumPopup("Type", choices[i].Type);
            RecalculateWindowHeight(ref wh);
            if (choices[i].Type != newType) {
                ChangeChoiceType(i, newType);
            }
            wh += DrawChoice(i);
        }
        if(GUILayout.Button("Add Choice")) {
            choices.Add(new DialogueChoiceInfo(new TrString(), -1));
            choiceRects.Add(new Rect());
            windowRect.height += 75f;
            return true;
        }
        RecalculateWindowHeight(ref wh);
        if (wh > 150f) {
            windowRect.height = wh;
        }
        return false; 
    }

    public void NodeDeleted(int nodeIdx) {
        if(id > nodeIdx) {
            --id;
        }
        for (int i = 0; i < choices.Count; ++i) {
            choices[i].NodeDeleted(nodeIdx);
        }
    }

    private void DrawCurvesInfo(DialogueChoiceInfo c, int idx) {
        if (c.NextNodeIdx == -1)
            return;
        var node = DialogueBuilder.GetNode(c.NextNodeIdx);
        Color color = node.Type == DialogueNodeType.Common ? Color.white : node.Type == DialogueNodeType.EndSuccess ? Color.green : Color.red;
        DialogueBuilder.DrawNodeCurve(choiceRects[idx], node.Rect, color);
    }

    public void DrawCurves() {
        if(choices == null) {
            return;
        }
        for(int i = 0; i < choices.Count; ++i) {
            if (choices[i].Type == DialogueChoiceType.Info) {
                DrawCurvesInfo(choices[i] as DialogueChoiceInfo, i);
            }
        }
    }

    public DialogueBuilderNode(int id, Rect windowRect) {
        this.id = id;
        this.windowRect = windowRect;
        type = id == 0 ? DialogueNodeType.Start : DialogueNodeType.Common;
    }

    public DialogueBuilderNode(DialogueBuilderNodeContainer c) {
        this.id = c.id;
        this.text = c.text;
        this.type = c.type;
        this.windowRect = c.windowRect.Rect;
        this.title = c.title;
        this.choices = c.choices;
        this.choiceRects = new List<Rect>(c.choices.Count);
        for(int i = 0; i < c.choices.Count; ++i) {
            this.choiceRects.Add(new Rect());
        }
    }

    /// <summary>
    /// Should be called once only from OnGUI method
    /// </summary>
    public static void InitializeStyles() {
        if(TextAreaStyle == null) {
            TextAreaStyle = GUI.skin.textArea;
            TextAreaStyle.wordWrap = true;
            LabelStyle = GUI.skin.label;
            LabelStyle.wordWrap = true;
        }
    }

    public void SetChoiceData(DialogueChoice choice, DialogueBuilderNode destination) {
        switch (choice.Type) {
            case DialogueChoiceType.Info:
                SetChoiceDataInfo(choice as DialogueChoiceInfo, destination.Id);
                break;
        }
    }

    private void SetChoiceDataInfo(DialogueChoiceInfo choice, int idx) {
        for(int i = 0; i < choices.Count; ++i) {
            if(choices[i].Type == DialogueChoiceType.Info && choices[i].Equals(choice)) {
                var c = choices[i] as DialogueChoiceInfo;
                c.NextNodeIdx = idx;
                break;
            }
        }
    }
}
