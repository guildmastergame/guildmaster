﻿
using Assets.Guildmaster.Scripts;
using Assets.Guildmaster.Scripts.Mech.Abilities;
using Assets.Guildmaster.Scripts.Mech.Abilities.Effects;
using UnityEditor;
using UnityEngine;
using Assets.Guildmaster.Scripts.Mech.Attributes;
using Assets.Guildmaster.Scripts.Mech;
using System.Collections.Generic;

public class AbilityEditor : EditorWindow {
    public enum EditorAbilityType {
        Passive = AbilityType.Passive,
        Toggle = AbilityType.Toggle,
        Active = AbilityType.Action,
    }

    public enum EditorActiveAbilityType {
        Action = AbilityType.Action,
        QuickAction = AbilityType.QuickAction,
        QuickActionMagic = AbilityType.QuickAction | AbilityType.Magic,
        QuickAttackMelee = AbilityType.Attack | AbilityType.Melee | AbilityType.QuickAction,
        QuickAttackShot = AbilityType.Attack | AbilityType.Shot | AbilityType.QuickAction,
        QuickAttackMagic = AbilityType.Attack | AbilityType.Magic | AbilityType.QuickAction,
        AttackMelee = AbilityType.Attack | AbilityType.Melee | AbilityType.Action,
        AttackShot = AbilityType.Attack | AbilityType.Shot | AbilityType.Action,
        AttackMagic = AbilityType.Attack | AbilityType.Magic | AbilityType.Action,
        ActionMagic = AbilityType.Action | AbilityType.Magic,   
    }

    #region Fields
    private const float SelectionGridWidth = 300f;
    private const float AutosaveTimeSeconds = 5f;
    private const EditorAbilityType AbilityTypeMask = EditorAbilityType.Active | EditorAbilityType.Passive | EditorAbilityType.Toggle;
    private float autosave;
    private Ability ability;
    private AbilityChangedCallback callback;
    public delegate void AbilityChangedCallback(Ability ability);
    private Vector2 scrollWindow;
    private List<bool> foldoutEffectsActive;
    private List<bool> foldoutEffectsPassive;
    #endregion

    #region Methods
    public static void Open(Ability ability, AbilityChangedCallback callback) {
        if(!SpriteVault.IsInitialized)
            SpriteVault.Initialize();
            var instance = CreateInstance<AbilityEditor>();
            instance.Initialize(ability, callback);
            instance.Show();
    }

    private void Initialize(Ability ability, AbilityChangedCallback callback) {
        if(ability.Target  == 0) {
            switch (((EditorAbilityType)ability.Type & AbilityTypeMask)) {
                case EditorAbilityType.Active:
                    ability.Target = TargetType.Enemy;
                    break;
                default:
                    ability.Target = TargetType.Self;
                    break;
            }
        }
        InitializeFoldouts(ability);
        this.ability = ability;
        this.callback = callback;
    }

    private void InitializeFoldouts(Ability ability) {
        var active = ability as AbilityActive;
        if (active != null) {
            foldoutEffectsActive = new List<bool>(active.Actives.Count);
            for(int i = 0; i < active.Actives.Count; ++i) {
                foldoutEffectsActive.Add(false);
            }
            foldoutEffectsPassive = new List<bool>(active.Passives.Count);
            for (int i = 0; i < active.Passives.Count; ++i) {
                foldoutEffectsPassive.Add(false);
            }
            return;
        }
        var passive = ability as AbilityPassive;
        if (passive != null) {
            foldoutEffectsPassive = new List<bool>(passive.Passives.Count);
            for (int i = 0; i < passive.Passives.Count; ++i) {
                foldoutEffectsPassive.Add(false);
            }
            return;
        }
        var toggle = ability as AbilityToggle;
        if (toggle != null) {
            foldoutEffectsActive = new List<bool>(toggle.Activated.Count);
            for (int i = 0; i < toggle.Activated.Count; ++i) {
                foldoutEffectsActive.Add(false);
            }
            foldoutEffectsPassive = new List<bool>(toggle.Deactivated.Count);
            for (int i = 0; i < toggle.Deactivated.Count; ++i) {
                foldoutEffectsPassive.Add(false);
            }
            return;
        }
    }

    private void SaveChanges() {
        if (callback.Target != null) {
            callback(ability);
        } else {
            Close();
        }
    }

    void Update() {
        autosave += Time.deltaTime;
        if(autosave > AutosaveTimeSeconds) {
            Debug.Log("Autosaving ability changes");
            SaveChanges();
            autosave = 0f;
        }
    }

    void OnDestroy() {
        SaveChanges();
    }

    void OnGUI() {
        if(ability == null) {
            return;
        }
        scrollWindow = EditorGUILayout.BeginScrollView(scrollWindow, GUILayout.Height(position.height - 20f));
        EditorGUILayout.BeginVertical(GUILayout.Width(position.width - 20f));
        DrawBasicInfo(ability);
        switch (ability.Type) {
            case AbilityType.Toggle: 
                DrawToggle(ability as AbilityToggle);
                break;
            case AbilityType.Passive:
                DrawCooldownSetup(ability);
                DrawPassive(ability as AbilityPassive);
                break;
            default:
                DrawCooldownSetup(ability);
                DrawActive(ability as AbilityActive);
                DrawPassive(ability as AbilityPassive);
                break;
        }
        EditorGUILayout.EndVertical();
        EditorGUILayout.EndScrollView();
    }

    private void DrawCooldownSetup(Ability ab) {
        EditorGUILayout.BeginHorizontal();
        ab.CurrentCooldown = EditorGUILayout.IntField("Starting Cooldown", ab.CurrentCooldown);
        ab.Cooldown = EditorGUILayout.IntField("Cooldown", ab.Cooldown);
        EditorGUILayout.EndHorizontal();
    }

    private void DrawActive(AbilityActive ab) {
        ab.Type = (AbilityType)EditorGUILayout.EnumPopup((EditorActiveAbilityType)ab.Type);
        EditorGUILayout.LabelField("Active Effects");
        var efx = ab.Actives;
        for (int i = 0; i < efx.Count; ++i) {
            EditorGUILayout.BeginHorizontal();
            foldoutEffectsActive[i] = EditorGUILayout.Foldout(foldoutEffectsActive[i], efx[i].Type.ToString());
            if (GUILayout.Button("Remove", GUILayout.Width(80f))) {
                foldoutEffectsActive.RemoveAt(i);
                ab.Actives.RemoveAt(i--);
                continue;
            }
            EditorGUILayout.EndHorizontal();
            if (foldoutEffectsActive[i]) {
                ab.EditActiveEffect(i, EditEffectUI(efx[i]));
                EditorGUILayout.Space();
            }
        }
        if (GUILayout.Button("Add Effect")) {
            ab.AddActiveEffect(new EffectDamage(new Damage(), Chance.Never, 0f));
            foldoutEffectsActive.Add(true);
        }
    }

    private void DrawPassive(AbilityPassive ab) {
        EditorGUILayout.LabelField("Passive Effects");
        var efx = ab.Passives;
        for (int i = 0; i < efx.Count; ++i) {
            EditorGUILayout.BeginHorizontal();
            foldoutEffectsPassive[i] = EditorGUILayout.Foldout(foldoutEffectsPassive[i], efx[i].Type.ToString());
            if (GUILayout.Button("Remove", GUILayout.Width(80f))) {
                foldoutEffectsPassive.RemoveAt(i);
                ab.Passives.RemoveAt(i--);
                continue;
            }
            EditorGUILayout.EndHorizontal();
            if (foldoutEffectsPassive[i]) {
                ab.EditPassiveEffect(i, EditEffectUI(efx[i]));
                EditorGUILayout.Space();
            }
        }
        if (GUILayout.Button("Add Effect")) {
            var effect = new EffectActionMod(0);
            effect.Trigger = 0;
            ab.AddPassiveEffect(effect);
            foldoutEffectsPassive.Add(true);
        }
    }

    private void DrawToggle(AbilityToggle ab) {
        EditorGUILayout.LabelField("Activated Effects");
        var efx = ab.Activated;
        for (int i = 0; i < efx.Count; ++i) {
            EditorGUILayout.BeginHorizontal();
            foldoutEffectsActive[i] = EditorGUILayout.Foldout(foldoutEffectsActive[i], efx[i].Type.ToString());
            if (GUILayout.Button("Remove", GUILayout.Width(80f))) {
                foldoutEffectsActive.RemoveAt(i);
                ab.Activated.RemoveAt(i--);
                continue;
            }
            EditorGUILayout.EndHorizontal();
            if (foldoutEffectsActive[i]) {
                ab.EditActivatedEffect(i, EditEffectUI(efx[i]));
                EditorGUILayout.Space();
            }
        }
        if (GUILayout.Button("Add Effect")) {
            var effect = new EffectActionMod(0);
            effect.Trigger = 0;
            ab.AddActivatedEffect(effect);
            foldoutEffectsActive.Add(true);
        }
        EditorGUILayout.LabelField("Deactivated Effects");
        efx = ab.Deactivated;
        for (int i = 0; i < efx.Count; ++i) {
            EditorGUILayout.BeginHorizontal();
            foldoutEffectsPassive[i] = EditorGUILayout.Foldout(foldoutEffectsPassive[i], efx[i].Type.ToString());
            if (GUILayout.Button("Remove", GUILayout.Width(80f))) {
                foldoutEffectsPassive.RemoveAt(i);
                ab.Deactivated.RemoveAt(i--);
                continue;
            }
            EditorGUILayout.EndHorizontal();
            
            if (foldoutEffectsPassive[i]) {
                ab.EditDeactivatedEffect(i, EditEffectUI(efx[i]));
                EditorGUILayout.Space();
            }
        }
        if (GUILayout.Button("Add Effect")) {
            var effect = new EffectActionMod(0);
            effect.Trigger = 0;
            ab.AddDeactivatedEffect(effect);
            foldoutEffectsPassive.Add(true);
        }
    }

    private EditorAbilityType GetEditorAbilityType(AbilityType type) {
        switch (type) {
            case AbilityType.Passive:
                return EditorAbilityType.Passive;
            case AbilityType.Toggle:
                return EditorAbilityType.Toggle;
            default:
                return EditorAbilityType.Active;
        }
    }

    private void DrawBasicInfo(Ability ab) {
        EditorGUILayout.BeginHorizontal();
        Sprite icon = SpriteVault.GetSpriteAbility(ab.ImageKey);
        icon = EditorGUILayout.ObjectField(icon, typeof(Sprite), false, GUILayout.Width(64f), GUILayout.Height(64f)) as Sprite;
        if (icon != null) {
            ab.ImageKey = icon.name;
        }
        EditorGUILayout.BeginVertical();
        ab.NameKey = EditorGUILayout.TextField("Name Key", ab.NameKey);
        ab.DescKey = EditorGUILayout.TextField("Desc Key", ab.DescKey);
        ab.AvailablePos = (BattlePositionType)EditorGUILayout.EnumPopup("Available on position:", ab.AvailablePos);
        var type = (EditorAbilityType)EditorGUILayout.EnumPopup(GetEditorAbilityType(ab.Type));
        if ((int)type != ((int)ab.Type & (int)AbilityTypeMask)) {
            switch (type) {
                case EditorAbilityType.Active:
                    ability = new AbilityActive(ab);
                    ability.Target = TargetType.Enemy;
                    break;
                case EditorAbilityType.Passive:
                    ability = new AbilityPassive(ab);
                    ability.Target = TargetType.Self;
                    break;
                case EditorAbilityType.Toggle:
                    ability = new AbilityToggle(ab);
                    ability.Target = TargetType.Self;
                    break;
            }
            InitializeFoldouts(ability);
        }
        EditorGUILayout.EndVertical();
        EditorGUILayout.EndHorizontal();
        ab.Target = (TargetType)EditorGUILayout.EnumPopup("Target", ab.Target);
    }

    private Effect EditEffectUI(Effect e) {
        var type = (EffectType)EditorGUILayout.EnumPopup("Type", e.Type);
        if(e.Type != type) {
            return Effect.CreateEffectOfType(type) ?? e;
        }
        e.EditEffectOnGUI();
        return e;
    }

    #endregion
}
