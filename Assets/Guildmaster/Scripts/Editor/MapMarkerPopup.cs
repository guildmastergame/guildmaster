﻿namespace Assets.Guildmaster.Scripts.Editor {
    using UnityEditor;
    using UnityEngine;
    using Utility;
    
    public class MapMarkerPopup : EditorWindow {
        #region Fields
        private const float WindowWidth = 600f;
        private const float WindowHeight = 400f;
        private SetMarkerPosition callback = null;
        private static Texture2D TextureEmpty = new Texture2D(1, 1);
        private int posX, posY;
        private static Texture2D TextureMap = Resources.Load<Texture2D>("Sprites/World/world_map_base");
        private static Texture2D TextureMarker = SpriteUtility.GetTexture(Resources.LoadAll<Sprite>("Sprites/World/map_pointers")[1]);
        private static Color[] mapPixels = TextureMap.GetPixels(0);
        private int offsetX = 0, offsetY = 0;
        private Vector2? mousePressed;
        private int clipWidth, clipHeight;
        public delegate void SetMarkerPosition(int posX, int posY);
        #endregion

        #region Accessors
        public int PosX { get { return posX; } set { posX = value > 0 ? value : 0; } }
        public int PosY { get { return posY; } set { posY = value > 0 ? value : 0; } }
        #endregion

        #region Methods
        public static void Init(SetMarkerPosition callback, int posX, int posY) {
            var window = CreateInstance<MapMarkerPopup>();
            window.callback = callback;
            window.posX = posX;
            window.posY = posY;
            window.position = new Rect(Screen.width / 2, Screen.height / 2, WindowWidth, WindowHeight);
            window.titleContent = new GUIContent("Select place for map marker");
            window.ShowUtility();
            window.clipWidth = (int)WindowWidth - 40;
            window.clipHeight = (int)WindowHeight - 50;
            window.CenterOnMarker();
        }

        private void ApplyChanges() {
            if (callback != null && callback.Target != null) {
                callback(posX, posY);
            }
        }

        private void CenterOnMarker() {
            offsetX = Mathf.Clamp(PosX - clipWidth / 2, 0, TextureMap.width - clipWidth);
            offsetY = Mathf.Clamp(PosY - clipHeight / 2, 0, TextureMap.height - clipHeight);
        }

        void OnGUI() {
            EditorGUILayout.BeginHorizontal();
            var px = EditorGUILayout.IntField("PosX:", PosX);
            if (px != PosX) {
                PosX = px;
                Repaint();
            }
            var py = EditorGUILayout.IntField("PosY:", PosY);
            if (py != PosY) {
                PosY = py;
                Repaint();
            }
            if (GUILayout.Button("Center")) {
                CenterOnMarker();
                return;
            }
            EditorGUILayout.EndHorizontal();
            if (Event.current.type == EventType.MouseDown) {
                if (Event.current.button == 0) {
                    PosX = offsetX + (int)Event.current.mousePosition.x - 20;
                    PosY = offsetY - (int)Event.current.mousePosition.y + 30 + clipHeight;
                    Repaint();
                } else {
                    mousePressed = Event.current.mousePosition;
                }
            }
            if (Event.current.type == EventType.MouseUp) {
                mousePressed = null;
            }
            if (mousePressed.HasValue && Event.current.type == EventType.MouseDrag) {
                offsetX += (int)mousePressed.Value.x - (int)Event.current.mousePosition.x;
                offsetX = Mathf.Clamp(offsetX, 0, TextureMap.width - clipWidth);
                offsetY += (int)Event.current.mousePosition.y - (int)mousePressed.Value.y;
                offsetY = Mathf.Clamp(offsetY, 0, TextureMap.height - clipHeight);
                //Debug.Log("OFFSET: " + offsetX + " " + offsetY);
                mousePressed = Event.current.mousePosition;
                Event.current.Use();
                Repaint();
            }

            

            clipWidth = Screen.width - 40;
            clipHeight = Screen.height - 50;
            Texture2D clippedMap = new Texture2D(clipWidth, clipHeight, TextureFormat.ARGB32, false);
            Color[] destinationPixels = clippedMap.GetPixels(0);
            for (int y = 0; y < clipHeight; ++y) {
                for (int x = 0; x < clipWidth; ++x) {
                    int idxClip = (y * clipWidth + x);
                    int idxMap = (y + offsetY) * TextureMap.width + x + offsetX;
                    destinationPixels[idxClip] = mapPixels[idxMap];
                }
            }

            clippedMap.SetPixels(destinationPixels, 0);
            clippedMap.Apply();
            EditorGUI.DrawPreviewTexture(new Rect(20, 30, clippedMap.width, clippedMap.height), clippedMap);
            //EditorGUI.DrawTextureTransparent(new Rect(100, 100, 32, 32), TextureMarker);
            GUI.DrawTexture(new Rect(20 - 16 - offsetX + PosX, 30 - 16 + offsetY + clipHeight - PosY, 32, 32), TextureMarker);
        }

        void OnLostFocus() {
            this.Close();
        }

        void OnDestroy() {
            callback(PosX, PosY);
        }
        #endregion
    }

}
