﻿namespace Assets.Guildmaster.Scripts.Editor {
    using Mech;
    using Mech.Quests;
    using Mech.Quests.Dialogues;
    using Utility;
    using System;
    using System.Collections.Generic;
    using UnityEditor;
    using UnityEngine;
    public class QuestEditorNode /*: ScriptableObject*/ {
        [Serializable]
        public class QuestEditorNodeContainer {
            public SerializableRect windowRect;
            public int id;
            public Quest quest;
            public string dialogueFilepath;

            public QuestEditorNodeContainer() {

            }

            public QuestEditorNodeContainer(QuestEditorNode node) {
                node.quest.PrepareToSerialization();
                this.windowRect = new SerializableRect(node.Rect);
                this.id = node.Id;
                this.quest = node.Quest;
                this.dialogueFilepath = node.dialogueFilepath;
            }
        }

        #region Fields
        private static GUIStyle TextAreaStyle = null;
        private static Texture2D TextureEmpty = new Texture2D(1, 1);
        protected Rect windowRect;
        protected int id;
        protected Quest quest;
        protected DialogueBuilder.DialogueContainer dialogueContainer = null;
        protected string dialogueFilepath;
        /// <summary>
        /// Transition rectangles to draw curves from them
        /// </summary>
        private List<Rect> tranRects = new List<Rect>();
        #endregion

        #region Accessors
        public Rect Rect { get { return windowRect; } set { windowRect = value; } }
        public string WindowTitle { get { return string.Format("{0}[#{1}]", quest.Name.ToString(), id); } }
        public int Id { get { return id; } }
        public Quest Quest { get { return quest; } }
        #endregion

        #region Methods
        /// <summary>
        /// Should be called once only from OnGUI method
        /// </summary>
        public static void InitializeStyles() {
            if (TextAreaStyle == null) {
                TextAreaStyle = GUI.skin.textArea;
                TextAreaStyle.wordWrap = true;
            }
        }
        public void ChangeQuestType(QuestType type) {
            switch (type) {
                case QuestType.Dialogue:
                    quest = new QuestDialogue(Quest);
                    break;
                case QuestType.Skirmish:
                    quest = new QuestSkirmish(Quest);
                    break;
                case QuestType.Scout:
                    quest = new QuestScout(Quest);
                    break;
                case QuestType.BlindSkirmish:
                    quest = new QuestBlindSkirmish(Quest);
                    break;
                case QuestType.Hunt:
                    quest = new QuestHunt(Quest);
                    break;
                case QuestType.Start:
                    quest = new QuestStart(Quest);
                    break;
            }
        }
        private void RecalculateWindowHeight(ref float wh) {
            if (Event.current.type == EventType.Repaint) {
                wh += GUILayoutUtility.GetLastRect().height;
            }
        }
        private void RecalculateTranRect(int idx) {
            if (Event.current.type == EventType.Repaint) {
                var r = GUILayoutUtility.GetLastRect();
                r.x += windowRect.x;
                r.y += windowRect.y + 15;
                tranRects[idx] = r;
            }
        }
        private float DrawStart(QuestStart q) {
            float wh = 0;
            q.DaysPassed = EditorGUILayout.IntField("Days Passed:", q.DaysPassed);
            RecalculateWindowHeight(ref wh);
            q.RewardMoney = EditorGUILayout.IntField("Reward Money:", q.RewardMoney);
            RecalculateWindowHeight(ref wh);
            q.RewardFame = EditorGUILayout.IntField("Reward Fame:", q.RewardFame);
            RecalculateWindowHeight(ref wh);
            return wh;
        }
        private float DrawDialogue(QuestDialogue q) {
            float wh = 0;
            EditorGUILayout.TextArea(string.Format("Dialogue: {0}", dialogueContainer == null ? "none" : dialogueFilepath), TextAreaStyle, GUILayout.Height(50f));
            RecalculateWindowHeight(ref wh);
            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("Set", GUILayout.Width(windowRect.width / 3))) {
                dialogueFilepath = DialogueBuilder.GetDialogueFilepath();
                if (dialogueFilepath != string.Empty) {
                    dialogueContainer = LoadDialogueData(ref q, dialogueFilepath);
                }
            }
            if (GUILayout.Button("New", GUILayout.Width(windowRect.width / 3))) {
                dialogueFilepath = DialogueBuilder.NewDialogue();
                if (dialogueFilepath != string.Empty) {
                    dialogueContainer = LoadDialogueData(ref q, dialogueFilepath);
                }
            }
            if (dialogueContainer != null && GUILayout.Button("Edit", GUILayout.Width(windowRect.width / 3))) {
                DialogueBuilder.OpenDialogue(dialogueFilepath);
            }
            EditorGUILayout.EndHorizontal();
            RecalculateWindowHeight(ref wh);
            return wh;
        }
        private float DrawSkirmish(QuestSkirmish q) {
            float wh = 0f;
            var foes = q.Foes;
            for (int partyIdx = 0; partyIdx < foes.Count; ++partyIdx) {
                if (GUILayout.Button("Delete Party")) {
                    foes.RemoveAt(partyIdx--);
                    continue;
                }
                RecalculateWindowHeight(ref wh);
                for (int i = 0; i < Party.MaxHeight; ++i) {
                    EditorGUILayout.BeginHorizontal();
                    GUILayout.Space(5f);
                    for (int j = 0; j < Party.MaxWidth; ++j) {
                        var creature = foes[partyIdx].GetCreature(j, i);
                        if (GUILayout.Button(creature == null ? TextureEmpty : creature.TexturePortrait, GUILayout.Width(windowRect.width / 5 - 5f), GUILayout.Height(60f))) {
                            SelectCreatureEditorPopup.Init(foes[partyIdx].SetCreature, j, i, creature == null ? null : creature.CreatureKey);
                        }
                    }
                    EditorGUILayout.EndHorizontal();
                    RecalculateWindowHeight(ref wh);
                    GUILayout.Space(5f);
                    wh += 8f;
                }
            }
            if (GUILayout.Button("Create party")) {
                foes.Add(new Party());
            }
            RecalculateWindowHeight(ref wh);
            return wh;
        }
        private float DrawScout(QuestScout q) {
            float wh = 0f;
            q.TurnsRequired = EditorGUILayout.IntField("Turns required:", q.TurnsRequired);
            RecalculateWindowHeight(ref wh);
            return wh;
        }
        private float DrawQuestDetails() {
            switch (quest.Type) {
                case QuestType.Start:
                    return DrawStart(quest as QuestStart);
                case QuestType.Dialogue:
                    return DrawDialogue(quest as QuestDialogue);
                case QuestType.Skirmish:
                    return DrawSkirmish(quest as QuestSkirmish);
                case QuestType.Scout:
                    return DrawScout(quest as QuestScout);
                default:
                    return 0;
            }
        }
        /// <returns>True if repaint needed</returns>
        public bool DrawWindow() {
            if (quest.Type == QuestType.Start) {
                GUI.DrawTexture(new Rect(0, 0, windowRect.xMax, windowRect.yMax), QuestEditor.Texture1x1green, ScaleMode.StretchToFill);
            } else if (quest.Type == QuestType.Hunt || quest.Type == QuestType.Skirmish || quest.Type == QuestType.BlindSkirmish) {
                GUI.DrawTexture(new Rect(0, 0, windowRect.xMax, windowRect.yMax), QuestEditor.Texture1x1red, ScaleMode.StretchToFill);
            }
            float wh = 50f; //window height
            quest.NameKey = EditorGUILayout.TextField("NameKey", quest.NameKey);
            RecalculateWindowHeight(ref wh);
            quest.DescKey = EditorGUILayout.TextField("DescKey", quest.DescKey);
            RecalculateWindowHeight(ref wh);
            EditorGUILayout.TextArea(quest.Desc.ToString(), TextAreaStyle, GUILayout.Width(windowRect.width), GUILayout.Height(100f));
            RecalculateWindowHeight(ref wh);
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(string.Format("Next Success Node: {0}", (quest.NextIdxSuccess == -1 ? "exit" : quest.NextIdxSuccess.ToString())), GUILayout.Width(150f));
            GUILayout.FlexibleSpace();
            if (GUILayout.Button("Reset", GUILayout.Width(50f))) {
                quest.NextIdxSuccess = -1;
            }
            if (GUILayout.Button("Set", GUILayout.Width(50f))) {
                QuestEditor.StartTransition(this, 0);
            }
            RecalculateTranRect(0);
            EditorGUILayout.EndHorizontal();
            RecalculateWindowHeight(ref wh);
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(string.Format("Next Fail Node: {0}", (quest.NextIdxFail == -1 ? "exit" : quest.NextIdxFail.ToString())), GUILayout.Width(150f));
            GUILayout.FlexibleSpace();
            if (GUILayout.Button("Reset", GUILayout.Width(50f))) {
                quest.NextIdxFail = -1;
            }
            if (GUILayout.Button("Set", GUILayout.Width(50f))) {
                QuestEditor.StartTransition(this, 1);
            }
            RecalculateTranRect(1);
            EditorGUILayout.EndHorizontal();
            RecalculateWindowHeight(ref wh);
            var type = (QuestType)EditorGUILayout.EnumPopup("Type", quest.Type);
            if (type != quest.Type) {
                ChangeQuestType(type);
                return true;
            }
            RecalculateWindowHeight(ref wh);
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(quest.MapMarkerPosition == Vector2.zero ? "Marker: none" : "Marker: [" + quest.MapMarkerPosition.x + "; " + quest.MapMarkerPosition.y + "]", GUILayout.Width(150f));
            GUILayout.FlexibleSpace();
            if (quest.MapMarkerPosition != Vector2.zero && GUILayout.Button("Reset", GUILayout.Width(50f))) {
                quest.MapMarkerPosition = Vector2.zero;
            }
            if (quest.MapMarkerPosition == Vector2.zero && GUILayout.Button("Parent", GUILayout.Width(50f))) {
                var parentQuest = QuestEditor.GetQuestParentById(Id);
                quest.MapMarkerPosition = parentQuest == null ? Vector2.zero : parentQuest.MapMarkerPosition;
            }
            if (GUILayout.Button("Set", GUILayout.Width(50f))) {
                MapMarkerPopup.Init(quest.SetMarkerCallback, (int)quest.MapMarkerPosition.x, (int)quest.MapMarkerPosition.y);
            }
            EditorGUILayout.EndHorizontal();
            RecalculateWindowHeight(ref wh);
            wh += DrawQuestDetails();
            if (wh > 150f) {
                windowRect.height = wh;
            }
            return false;
        }
        public void NodeDeleted(int nodeIdx) {
            if (id > nodeIdx) {
                --id;
            }
            if (quest.NextIdxFail == nodeIdx) {
                quest.NextIdxFail = -1;
            } else if (quest.NextIdxFail > nodeIdx) {
                --quest.NextIdxFail;
            }
            if (quest.NextIdxSuccess == nodeIdx) {
                quest.NextIdxSuccess = -1;
            } else if (quest.NextIdxSuccess > nodeIdx) {
                --quest.NextIdxSuccess;
            }
        }
        public void DrawCurves() {
            if (quest == null) {
                return;
            }
            if (quest.NextIdxSuccess != -1) {
                var node = QuestEditor.GetNode(quest.NextIdxSuccess);
                QuestEditor.DrawNodeCurve(tranRects[0], node.Rect, Color.green);
            }
            if (quest.NextIdxFail != -1) {
                var node = QuestEditor.GetNode(quest.NextIdxFail);
                QuestEditor.DrawNodeCurve(tranRects[1], node.Rect, Color.red);
            }
        }
        public QuestEditorNode(int id, Rect windowRect) {
            this.id = id;
            this.windowRect = windowRect;
            quest = new QuestDialogue(new TrString(), new TrString(), new List<DialogueNode>(), Vector2.zero, -1, -1);
            tranRects.Add(new Rect());
            tranRects.Add(new Rect());
        }
        public QuestEditorNode(QuestEditorNodeContainer c) {
            this.id = c.id;
            this.windowRect = c.windowRect.Rect;
            this.quest = c.quest;
            this.quest.PrepareAfterSerialization();
            this.dialogueFilepath = c.dialogueFilepath;
            tranRects.Add(new Rect());
            tranRects.Add(new Rect());
            var dQuest = quest as QuestDialogue;
            if (dQuest != null) {
                dialogueContainer = LoadDialogueData(ref dQuest, dialogueFilepath);
            }
            
        }
        public static DialogueBuilder.DialogueContainer LoadDialogueData(ref QuestDialogue quest, string dialogueFilepath) {
            var dc = DialogueBuilder.LoadDialogue(dialogueFilepath);
            if(dc != null) {
                var nodes = new List<DialogueNode>(dc.nodes.Count);
                for (int i = 0; i < dc.nodes.Count; ++i) {
                    var node = dc.nodes[i];
                    nodes.Add(new DialogueNode(new TrString(node.text), node.type, node.choices));
                }
                quest.Nodes = nodes;
            }
            return dc;
        }
        public void SetTransitionData(int questTransitionId, QuestEditorNode questEditorNode) {
            if (questTransitionId == 0) {
                quest.NextIdxSuccess = questEditorNode.Id;
            } else if (questTransitionId == 1) {
                quest.NextIdxFail = questEditorNode.Id;
            }
        }
        #endregion
    }
}