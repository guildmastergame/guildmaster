﻿using Assets.Guildmaster.Scripts;
using Assets.Guildmaster.Scripts.Mech.Items;
using Assets.Guildmaster.Scripts.Utility;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class SelectItemEditorPopup : EditorWindow {
    #region Fields
    private const float WindowWidth = 600f;
    private const float WindowHeight = 600f;
    private const float ItemIconSize = 64f;
    public delegate void SetItem(Item item);
    private SetItem setCallback = null;
    private Item selectedItem = null;
    private Vector2 scrollList;
    private List<Item> items;
    private static Texture2D TextureEmpty = new Texture2D(1, 1);
    #endregion

    #region Methods
    public static void Init(SetItem callback, EditorItemFilterType allowed, string selectedItemKey = null) {
        var window = CreateInstance<SelectItemEditorPopup>();
        ItemVault.Initialize();
        var itemList = ItemVault.Items;
        window.items = new List<Item>();
        for (int i = 0; i < itemList.Count; ++i) {
            if (((ItemType)allowed & itemList[i].Type) == itemList[i].Type) {
                window.items.Add(itemList[i]);
            }
        }
        window.selectedItem = selectedItemKey != null ? ItemVault.Get(selectedItemKey) : null;
        window.setCallback = callback;
        window.position = new Rect(Screen.width / 2, Screen.height / 2, WindowWidth, WindowHeight);
        window.titleContent = new GUIContent("Select item");
        window.ShowUtility();
    }

    private void DrawItemInfo() {
        if(selectedItem == null) {
            return;
        }
        EditorGUILayout.BeginHorizontal();
        GUILayout.Box(SpriteUtility.GetTexture(selectedItem.Image), GUILayout.Width(150f), GUILayout.Height(150f));
        EditorGUILayout.BeginVertical();
        EditorGUILayout.LabelField("Item Key", selectedItem.ItemKey);
        EditorGUILayout.LabelField("Name Key", selectedItem.NameKey);
        EditorGUILayout.LabelField("Desc Key", selectedItem.DescKey);
        EditorGUILayout.EnumPopup(selectedItem.Type);
        //Item Ability
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Item Ability", selectedItem.Ability == null ? "none" : selectedItem.Ability.NameKey);
        var tempSprite = selectedItem.Ability == null ? null : selectedItem.Ability.Image;
        GUILayout.Box(tempSprite == null ? TextureEmpty : SpriteUtility.GetTexture(tempSprite), GUILayout.Width(64f), GUILayout.Height(64f));
        EditorGUILayout.EndHorizontal();
        if (selectedItem.Ability != null && GUILayout.Button("Edit Ability")) {
            AbilityEditor.Open(selectedItem.Ability, null);
        }
        EditorGUILayout.EndVertical();
        EditorGUILayout.EndHorizontal();        
        if(GUILayout.Button("Select")) {
            if (setCallback.Target != null) {
                setCallback(selectedItem);
            }
            Close();
        }
    }

    private void SelectItem(int idx) {
        selectedItem = items[idx];
    }

    private void DrawItemList() {
        EditorGUILayout.BeginHorizontal();
        int itemsPerLine = (int)(position.width / ItemIconSize) - 1; 
        for (int i = 0; i < items.Count; ++i) {
            if(i % itemsPerLine == 0 && i != 0) {
                EditorGUILayout.EndHorizontal();
                EditorGUILayout.BeginHorizontal();
            }
            if (GUILayout.Button(SpriteUtility.GetTexture(items[i].Image), GUILayout.Width(ItemIconSize), GUILayout.Height(ItemIconSize))) {
                SelectItem(i);
            }
        }
        EditorGUILayout.EndHorizontal();
    }

    void OnGUI() {
        EditorGUILayout.BeginVertical();
        EditorGUILayout.LabelField("Select item");
        scrollList = EditorGUILayout.BeginScrollView(scrollList, GUILayout.Height(250f));
        DrawItemList();
        EditorGUILayout.EndScrollView();
        EditorGUILayout.LabelField("Item info");
        DrawItemInfo();
        EditorGUILayout.EndVertical();
    }
    #endregion Methods
}
