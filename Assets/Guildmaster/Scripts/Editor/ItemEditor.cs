﻿namespace Assets.Guildmaster.Scripts.Editor {
    using Mech.Abilities;
    using Mech.Items;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Runtime.Serialization.Formatters.Binary;
    using UnityEditor;
    using UnityEngine;
    using Utility;

    public class ItemEditor : EditorWindow {
        #region Fields
        private static ItemEditor instance = null;
        private const float SelectionListWidth = 300f;
        private Vector2 infoScroll;
        private Vector2 listScroll;
        private List<Item> items;
        private int selectedIdx = -1;
        private string newKey = string.Empty;
        private Sprite tempSprite;
        private Texture2D textureEmpty = new Texture2D(1, 1);
        public delegate void UpdateEditedItem(Item item);
        private UpdateEditedItem editCallback = null;
        private int editItemId = -1;
        #endregion

        [MenuItem("Guildmaster/Item Editor")]
        public static void Open() {
            Open(null, null);
        }
        public static void Open(string selectedItemKey, UpdateEditedItem editCallback) {
            if (instance == null) {
                instance = CreateInstance<ItemEditor>();
                instance.Initialize();
                instance.editCallback = editCallback;
                if (selectedItemKey != null) {
                    for(int i = 0; i < instance.items.Count; ++i) {
                        if(selectedItemKey == instance.items[i].ItemKey) {
                            instance.selectedIdx = instance.editItemId = i;
                            break;
                        }
                    }
                }
                instance.Show();
            } else {
                instance.Focus();
            }
        }

        private void Initialize() {
            if (!SpriteVault.IsInitialized) {
                SpriteVault.Initialize();
            }
            LoadFromFile(ItemVault.DefaultFilePath);
            selectedIdx = -1;
        }

        private void LoadFromFile(string filepath) {
            if(!File.Exists(filepath)) {
                items = new List<Item>();
                return;
            }
            BinaryFormatter bformatter = new BinaryFormatter();
            bformatter.Binder = new VersionDeserializationBinder();
            var stream = new FileStream(filepath, FileMode.Open);
            items = bformatter.Deserialize(stream) as List<Item>;
            if(items == null) {
                items = new List<Item>();
            }
            stream.Close();
        }

        private void SaveToFile(string filepath) {
            if (filepath == string.Empty)
                return;
            BinaryFormatter bformatter = new BinaryFormatter();
            bformatter.Binder = new VersionDeserializationBinder();
            var stream = new FileStream(filepath, FileMode.Create);
            bformatter.Serialize(stream, items);
            stream.Close();
        }

        private void TryUpdateEditedItem() {
            if (editItemId != -1 && editCallback != null) {
                if (editCallback.Target != null) {
                    editCallback(items[editItemId]);
                }
                editItemId = -1;
                editCallback = null;
            }
        }

        private void SelectItem(int idx) {
            selectedIdx = idx;
            TryUpdateEditedItem();
        }

        private void DrawItemList() {
            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("Save")) {
                SaveToFile(ItemVault.DefaultFilePath);
            }
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.BeginHorizontal();
            newKey = EditorGUILayout.TextField("New key:", newKey);
            if (GUILayout.Button("Create") && !items.Any(x => x.ItemKey == newKey)) {
                selectedIdx = items.Count;
                items.Add(new Item(newKey));
            }
            EditorGUILayout.EndHorizontal();
            for (int i = 0; i < items.Count; ++i) {
                EditorGUILayout.BeginHorizontal();
                if (GUILayout.Button("-", GUILayout.Width(30f), GUILayout.Height(48f))) {
                    if (selectedIdx >= i) {
                        --selectedIdx;
                    }
                    items.RemoveAt(i--);
                    continue;
                }
                GUILayout.Box(SpriteUtility.GetTexture(items[i].Image), GUILayout.Width(48f), GUILayout.Height(48f));
                if (GUILayout.Button(items[i].ItemKey, GUILayout.Height(48f))) {
                    SelectItem(i);
                }
                EditorGUILayout.EndHorizontal();
            }
        }

        private void DrawItemInfo(Item item) {
            EditorGUILayout.BeginHorizontal();
            tempSprite = item.Image;
            Texture2D texture = tempSprite == null ? null : tempSprite.texture;
            texture = EditorGUILayout.ObjectField(texture, typeof(Texture2D), false, GUILayout.Width(128f), GUILayout.Height(128f)) as Texture2D;
            if (texture != null && texture.name != item.ImageKey) {
                item.ImageKey = texture.name;
            }
            EditorGUILayout.BeginVertical();
            EditorGUILayout.LabelField("Item Key", item.ItemKey);
            item.NameKey = EditorGUILayout.TextField("Name Key", item.NameKey);
            item.DescKey = EditorGUILayout.TextField("Desc Key", item.DescKey);
            var ttype = (ItemType)EditorGUILayout.EnumPopup((EditorItemType)item.Type);
            if(ttype != item.Type) {
                item.Type = ttype;
            }
            EditorGUILayout.EndVertical();
            EditorGUILayout.EndHorizontal();
            //Item Ability
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Item Ability", item.Ability == null ? "none" : item.Ability.NameKey);
            tempSprite = item.Ability == null ? null : item.Ability.Image;
            GUILayout.Box(tempSprite == null ? textureEmpty : SpriteUtility.GetTexture(tempSprite), GUILayout.Width(64f), GUILayout.Height(64f));
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("New")) {
                AbilityEditor.Open(new AbilityPassive(), item.AbilityChanged);
            }
            if (item.Ability != null && GUILayout.Button("Edit")) {
                AbilityEditor.Open(item.Ability, item.AbilityChanged);
            }
            if (GUILayout.Button("Reset")) {
                item.AbilityChanged(null);
            }
            EditorGUILayout.EndHorizontal();

        }

        void OnGUI() {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.BeginVertical(GUILayout.Width(SelectionListWidth));
            listScroll = EditorGUILayout.BeginScrollView(listScroll, false, false);
            DrawItemList();
            EditorGUILayout.EndScrollView();
            EditorGUILayout.EndVertical();
            if (selectedIdx >= 0) {
                infoScroll = EditorGUILayout.BeginScrollView(infoScroll, false, false);
                EditorGUILayout.BeginVertical(GUILayout.Width(position.width - 10f - SelectionListWidth));
                DrawItemInfo(items[selectedIdx]);
                EditorGUILayout.EndVertical();
                EditorGUILayout.EndScrollView();
            }
            EditorGUILayout.EndHorizontal();
        }

        void OnDestroy() {
            SaveToFile(ItemVault.DefaultFilePath);
            TryUpdateEditedItem();
        }
    }
}

