﻿namespace Assets.Guildmaster.Scripts.Editor {
    using Mech.Quests;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Runtime.Serialization.Formatters.Binary;
    using UnityEditor;
    using UnityEngine;
    using Utility;
    public class QuestEditor : EditorWindow {
        private enum MenuActionEnum {
            AddStartNode,
            AddDialogueNode,
            AddSkirmishNode,
            AddBlindSkirmishNode,
            AddHuntNode,
            AddScoutNode,
            DeleteNode
        }
        [Serializable]
        public class QuestContainer {
            public float panX;
            public float panY;
            public List<QuestEditorNode.QuestEditorNodeContainer> nodes;

            public QuestContainer() {
                panX = 0;
                panY = 0;
                nodes = null;
            }

            public QuestContainer(QuestEditor qe) {
                panX = qe.panX;
                panY = qe.panY;
                nodes = new List<QuestEditorNode.QuestEditorNodeContainer>(qe.windows.Count);
                for (int i = 0; i < qe.windows.Count; ++i) {
                    nodes.Add(new QuestEditorNode.QuestEditorNodeContainer(qe.windows[i]));
                }
            }

            public void InitializeDialogueBuilder(QuestEditor qe) {
                qe.panX = panX;
                qe.panY = panY;
                qe.windows = new List<QuestEditorNode>(nodes.Count);
                for (int i = 0; i < nodes.Count; ++i) {
                    qe.windows.Add(new QuestEditorNode(nodes[i]));
                }
                qe.Repaint();
            }
        }
        #region Fields
        static QuestEditor window;
        public static Texture2D Texture1x1green;
        public static Texture2D Texture1x1red;
        public Rect _handleArea;
        private bool _nodeOption, _options, _handleActive, _action;
        private static Texture2D _resizeHandle, _aaLine;
        private GUIContent _icon;
        private float _winMinX, _winMinY;
        private int _mainwindowID;
        private Vector2 mousePos;
        private bool makeTransitionMode = false;
        private float panX;
        private float panY;
        private List<QuestEditorNode> windows = new List<QuestEditorNode>();
        private QuestEditorNode selectedNode = null;
        private int questTransitionId = -1;
        private const float PanMaxX = 10000f;
        private const float PanMaxY = 10000f;
        private bool scrollWindow = false;
        private Vector2 scrollStartMousePos;
        private Vector2 scrollStartPan;
        private int deleteNodeWithIdx = -1;
        private string filepath;
        #endregion
        #region Methods
        static void Init(string filepath) {
            if (!Language.IsInitialized()) {
                Language.Initialize();
            }
            if (!SpriteVault.IsInitialized) {
                SpriteVault.Initialize();
            }
            if (!CreatureVault.IsInitialized) {
                CreatureVault.Initialize();
            }
            window = GetWindow<QuestEditor>(typeof(QuestEditor));
            window.titleContent = new GUIContent("Quest Editor");
            window.InitNewNodes();
            window.filepath = filepath;
            Texture1x1green = new Texture2D(1, 1, TextureFormat.RGBA32, false);
            Texture1x1green.SetPixel(0, 0, new Color(0.6f, 0.756f, 0.6f));
            Texture1x1green.Apply();
            Texture1x1red = new Texture2D(1, 1, TextureFormat.RGBA32, false);
            Texture1x1red.SetPixel(0, 0, new Color(0.756f, 0.6f, 0.6f));
            Texture1x1red.Apply();
        }
        [MenuItem("Guildmaster/Quest Editor/New Quest...")]
        static void NewQuest() {
            string path = EditorUtility.SaveFilePanel("Create new quest", "", "Quest", "qst");
            Init(path);
            window.SaveQuest();
        }
        [MenuItem("Guildmaster/Quest Editor/Open Quest...")]
        static void OpenQuest() {
            string path = EditorUtility.OpenFilePanel("Open quest file", "", "qst");
            Init(path);
            window.LoadQuest();
        }
        /// <summary>
        /// Returns first parent of quest with given id
        /// </summary>
        /// <param name="id">Id of quest which parent should be found</param>
        /// <returns>First available parent of quest with given id</returns>
        public static Quest GetQuestParentById(int id) {
            for(int i = 0; i < window.windows.Count; ++i) {
                if(window.windows[i].Quest.NextIdxFail == id || window.windows[i].Quest.NextIdxSuccess == id) {
                    return window.windows[i].Quest;
                }
            }
            return null;
        }
        private void SaveQuest() {
            if (filepath == string.Empty)
                return;
            var c = new QuestContainer(this);
            BinaryFormatter bformatter = new BinaryFormatter();
            bformatter.Binder = new VersionDeserializationBinder();
            var stream = new FileStream(filepath, FileMode.Create);
            bformatter.Serialize(stream, c);
            stream.Close();
        }
        private void ExportTree() {
            string path = EditorUtility.SaveFilePanel("Export Quest Tree", "", "QuestTree", "qtree");
            QuestTreeContainer qtc = new QuestTreeContainer();
            qtc.nodes = new List<Mech.Quests.QuestContainer>(windows.Count);
            for (int i = 0; i < windows.Count; ++i) {
                qtc.nodes.Add(Mech.Quests.QuestContainer.PackEditorNode(windows[i].Quest));
            }
            //todo: optional nodes
            qtc.Serialize(path);
        }
        private void LoadQuest() {
            if (filepath == string.Empty)
                return;
            BinaryFormatter bformatter = new BinaryFormatter();
            bformatter.Binder = new VersionDeserializationBinder();
            var stream = new FileStream(filepath, FileMode.Open);
            var c = bformatter.Deserialize(stream) as QuestContainer;
            stream.Close();
            c.InitializeDialogueBuilder(this);
            //window.Repaint();
            window.Show();
        }
        public static void StartTransition(QuestEditorNode node, int trasitionId) {
            window.makeTransitionMode = true;
            window.selectedNode = node;
            window.questTransitionId = trasitionId;
        }
        private void InitNewNodes() {
            _winMinX = 300f;
            _winMinY = 400f;
            windows = new List<QuestEditorNode>();
            windows.Add(new QuestEditorNode(0, new Rect(30, 30, _winMinX, _winMinY)));
            windows.Add(new QuestEditorNode(1, new Rect(410, 210, _winMinX, _winMinY)));

            _resizeHandle = AssetDatabase.LoadAssetAtPath("Assets/Resources/EditorIcons/resizeHandle.png", typeof(Texture2D)) as Texture2D;
            _aaLine = AssetDatabase.LoadAssetAtPath("Assets/NodeEditor/Icons/aa1x5.png", typeof(Texture2D)) as Texture2D;
            _icon = new GUIContent(_resizeHandle);
            _mainwindowID = GUIUtility.GetControlID(FocusType.Native); //grab primary editor window controlID
        }
        private void MenuActionCallback(object obj) {
            var posRect = new Rect(mousePos.x - panX - 150f, mousePos.y - panY - 200f, 300, 400);
            switch ((MenuActionEnum)obj) {
                case MenuActionEnum.AddStartNode:
                    windows.Add(new QuestEditorNode(windows.Count, posRect));
                    windows[windows.Count - 1].ChangeQuestType(QuestType.Start);
                    break;
                case MenuActionEnum.AddDialogueNode:
                    windows.Add(new QuestEditorNode(windows.Count, posRect));
                    break;
                case MenuActionEnum.AddSkirmishNode:
                    windows.Add(new QuestEditorNode(windows.Count, posRect));
                    windows[windows.Count - 1].ChangeQuestType(QuestType.Skirmish);
                    break;
                case MenuActionEnum.AddScoutNode:
                    windows.Add(new QuestEditorNode(windows.Count, posRect));
                    windows[windows.Count - 1].ChangeQuestType(QuestType.Scout);
                    break;
                case MenuActionEnum.DeleteNode:
                    if (deleteNodeWithIdx > -1) {
                        windows.RemoveAt(deleteNodeWithIdx);
                        for (int i = 0; i < windows.Count; ++i) {
                            windows[i].NodeDeleted(deleteNodeWithIdx);
                        }
                        deleteNodeWithIdx = -1;
                    }
                    break;
            }
        }
        private int FindWindowAboveMouse(Vector2 mousePos) {
            Rect r;
            for (int i = 0; i < windows.Count; ++i) {
                r = windows[i].Rect;
                r.x += panX;
                r.y += panY;
                if (r.Contains(mousePos)) {
                    return i;
                }
            }
            return -1;
        }
        void OnGUI() {
            QuestEditorNode.InitializeStyles();
            Event e = Event.current;
            mousePos = e.mousePosition;

            GUI.color = new Color(0, 0, 0, 0.7f);
            GUI.Box(new Rect(panX, panY, PanMaxX, PanMaxY), "");
            GUI.color = Color.white;

            //UI
            if (GUI.Button(new Rect(10, 10, 80, 25), "Save")) {
                SaveQuest();
            }
            if (GUI.Button(new Rect(100, 10, 80, 25), "Export Tree")) {
                ExportTree();
            }

            if (e.button == 1 && !makeTransitionMode) {
                if (e.type == EventType.MouseDown) {
                    int selectedIdx = FindWindowAboveMouse(mousePos);
                    if (selectedIdx == -1) {//Empty space clicked
                        GenericMenu menu = new GenericMenu();
                        menu.AddItem(new GUIContent("Add Dialogue Node"), false, MenuActionCallback, MenuActionEnum.AddDialogueNode);
                        menu.AddItem(new GUIContent("Add Scout Node"), false, MenuActionCallback, MenuActionEnum.AddScoutNode);
                        menu.AddItem(new GUIContent("Add Skirmish Node"), false, MenuActionCallback, MenuActionEnum.AddSkirmishNode);
                        menu.AddItem(new GUIContent("Add Blind Skirmish Node"), false, MenuActionCallback, MenuActionEnum.AddBlindSkirmishNode);
                        menu.AddItem(new GUIContent("Add Hunt Node"), false, MenuActionCallback, MenuActionEnum.AddHuntNode);
                        menu.AddItem(new GUIContent("Add Start Node"), false, MenuActionCallback, MenuActionEnum.AddStartNode);
                        menu.ShowAsContext();
                    } else {
                        GenericMenu menu = new GenericMenu();
                        menu.AddItem(new GUIContent("Delete Node"), false, MenuActionCallback, MenuActionEnum.DeleteNode);
                        deleteNodeWithIdx = selectedIdx;
                        menu.ShowAsContext();
                    }
                    e.Use();
                }
            } else if (e.button == 0 && e.type == EventType.MouseDown && makeTransitionMode) {
                int selectedIndex = FindWindowAboveMouse(mousePos);
                if (selectedIndex == -1) { //no window clicked
                    makeTransitionMode = false; //stop transition
                    selectedNode = null;
                } else if (!windows[selectedIndex].Equals(selectedNode)) {
                    //if there is a click on a window and it's not the window that the transition started from
                    //selectedNode.SetChoiceData(selectedDialogueChoice, windows[selectedIndex]);
                    selectedNode.SetTransitionData(questTransitionId, windows[selectedIndex]);
                    makeTransitionMode = false;
                    selectedNode = null;
                    questTransitionId = -1;
                }
                e.Use();
            } else if (e.button == 0 && e.type == EventType.MouseDown && !makeTransitionMode) {
                //if there is a left click and we are not in a transition mode
                int selectedIndex = FindWindowAboveMouse(mousePos);
                if (selectedIndex != -1) {

                }
            }

            GUI.BeginGroup(new Rect(panX, panY, PanMaxX, PanMaxY));
            //Draw curves for every node
            for (int i = 0; i < windows.Count; ++i) {
                windows[i].DrawCurves();
            }

            BeginWindows();
            for (int i = 0; i < windows.Count; i++) {
                windows[i].Rect = GUI.Window(i, windows[i].Rect, DrawNodeWindow, windows[i].WindowTitle);
            }
            EndWindows();
            GUI.EndGroup();

            //if we are in a transition mode and there is a selected node
            if (makeTransitionMode && selectedNode != null) {
                //draw the curve from the selected node to the mouse position
                Rect mouseRect = new Rect(e.mousePosition.x, e.mousePosition.y, 10, 10);
                Rect source = selectedNode.Rect;
                source.x += panX;
                source.y += panY;
                DrawNodeCurve(source, mouseRect, Color.blue);
                Repaint();
            }
            //if drag extends inner window bounds _handleActive remains true as event gets lost to parent window
            if ((Event.current.rawType == EventType.MouseUp) && (GUIUtility.hotControl != _mainwindowID)) {
                GUIUtility.hotControl = 0;
            }
            if (e.button == 2) {
                if (e.type == EventType.MouseDown) {
                    scrollStartMousePos = e.mousePosition;
                    scrollStartPan = new Vector2(panX, panY);
                    scrollWindow = true;
                } else if (e.type == EventType.MouseUp) {
                    scrollWindow = false;
                }
            }
            if (scrollWindow) {
                Vector2 mouseDiff = e.mousePosition - scrollStartMousePos;
                panX = Mathf.Clamp(scrollStartPan.x + mouseDiff.x, position.width - 100 - PanMaxX, 100);
                panY = Mathf.Clamp(scrollStartPan.y + mouseDiff.y, position.height - 100 - PanMaxY, 100);
                Repaint();
            }
        }
        private void DrawNodeWindow(int id) {
            if (GUIUtility.hotControl == 0) {  //mouseup event outside parent window?
                _handleActive = false; //make sure handle is deactivated
            }
            float _cornerX = windows[id].Rect.width;
            float _cornerY = windows[id].Rect.height;
            GUILayout.BeginArea(new Rect(1, 16, _cornerX - 3, _cornerY - 10));
            if (windows[id].DrawWindow()) {
                Repaint();
                return;
            }
            GUILayout.EndArea();
            GUILayout.BeginArea(new Rect(1, _cornerY - 16, _cornerX - 3, 14));
            GUILayout.BeginHorizontal(EditorStyles.toolbarTextField, GUILayout.ExpandWidth(true));
            GUILayout.FlexibleSpace();
            //grab corner area based on content reference
            _handleArea = GUILayoutUtility.GetRect(_icon, GUIStyle.none);
            GUI.DrawTexture(new Rect(_handleArea.xMin + 6, _handleArea.yMin - 3, 20, 20), _resizeHandle); //hacky placement
            _action = (Event.current.type == EventType.MouseDown) || (Event.current.type == EventType.MouseDrag);
            if (!_handleActive && _action) {
                if (_handleArea.Contains(Event.current.mousePosition, true)) {
                    _handleActive = true; //active when cursor is in contact area
                    GUIUtility.hotControl = GUIUtility.GetControlID(FocusType.Native); //set handle hot
                }
            }
            EditorGUIUtility.AddCursorRect(_handleArea, MouseCursor.ResizeUpLeft);
            GUILayout.EndHorizontal();
            GUILayout.EndArea();
            //resize window
            if (_handleActive && (Event.current.type == EventType.MouseDrag)) {
                ResizeNode(id, Event.current.delta.x, Event.current.delta.y);
                Repaint();
                Event.current.Use();
            }
            //enable drag for node
            if (!_handleActive) {
                GUI.DragWindow();
            }
        }
        private void ResizeNode(int id, float deltaX, float deltaY) {
            var r = windows[id].Rect;
            if ((r.width + deltaX) > 200) {
                r.xMax += deltaX;
            }
            if ((r.height + deltaY) > 200) {
                r.yMax += deltaY;
            }
            windows[id].Rect = r;
        }
        public static void DrawNodeCurve(Rect start, Rect end, Color? color = null) {
            Vector3 startPos = new Vector3(start.x + start.width, start.y + start.height / 2, 0);
            Vector3 endPos = new Vector3(end.x, end.y + end.height / 2, 0);
            Vector3 startTan = startPos + Vector3.right * 50;
            Vector3 endTan = endPos + Vector3.left * 50;
            Handles.DrawBezier(startPos, endPos, startTan, endTan, color.HasValue ? color.Value : Color.black, _aaLine, 5f);
        }
        public static QuestEditorNode GetNode(int idx) {
            return window.windows[idx];
        }
        #endregion
    }
}