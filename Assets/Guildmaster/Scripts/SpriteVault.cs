﻿namespace Assets.Guildmaster.Scripts {
    using UnityEngine;
    using System.Collections.Generic;
    public static class SpriteVault {
        #region Fields
        private static bool isInitialized = false;
        private static Dictionary<string, Sprite> abilities;
        private static Dictionary<string, Sprite> creatures;
        private static Dictionary<string, Sprite> items;
        private static Dictionary<string, Sprite> icons;
        private static Dictionary<string, Sprite> world;
        private static Sprite sprite;//Temporal variable. It is here to reduce code amount and don't define it every time in every method
        #endregion

        #region Accessors
        public static bool IsInitialized { get { return isInitialized; } }
        #endregion

        #region Methods
        public static void Initialize() {
            if(isInitialized) {
                Debug.LogWarning("SpriteVault already initialized");
                return;
            }
            isInitialized = true;
            Sprite[] sprites = Resources.LoadAll<Sprite>("Sprites/Abilities/");
            abilities = new Dictionary<string, Sprite>(sprites.Length);
            for (int i = 0; i < sprites.Length; ++i) {
                abilities.Add(sprites[i].name, sprites[i]);
            }
            sprites = Resources.LoadAll<Sprite>("Sprites/Creatures/");
            creatures = new Dictionary<string, Sprite>(sprites.Length);
            for (int i = 0; i < sprites.Length; ++i) {
                creatures.Add(sprites[i].name, sprites[i]);
            }
            sprites = Resources.LoadAll<Sprite>("Sprites/Items/");
            items = new Dictionary<string, Sprite>(sprites.Length);
            for (int i = 0; i < sprites.Length; ++i) {
                items.Add(sprites[i].name, sprites[i]);
            }
            sprites = Resources.LoadAll<Sprite>("Sprites/Icons/");
            icons = new Dictionary<string, Sprite>(sprites.Length);
            for (int i = 0; i < sprites.Length; ++i) {
                icons.Add(sprites[i].name, sprites[i]);
            }
            sprites = Resources.LoadAll<Sprite>("Sprites/World/");
            world = new Dictionary<string, Sprite>(sprites.Length);
            for (int i = 0; i < sprites.Length; ++i) {
                world.Add(sprites[i].name, sprites[i]);
            }
        }
        public static Sprite GetSpriteAbility(string key) {
            if (abilities.TryGetValue(key, out sprite))
                return sprite;
            return null;
        }
        public static Sprite GetSpriteCreature(string key) {
            if (creatures.TryGetValue(key, out sprite))
                return sprite;
            return null;
        }
        public static Sprite GetSpriteItem(string key) {
            if (items.TryGetValue(key, out sprite))
                return sprite;
            return null;
        }
        public static Sprite GetSpriteIcon(string key) {
            if (icons.TryGetValue(key, out sprite))
                return sprite;
            return null;
        }
        public static Sprite GetSpriteWorld(string key) {
            if (world.TryGetValue(key, out sprite))
                return sprite;
            return null;
        }
        /// <summary>
        /// Use this for your own risk when you don't know where is the sprite
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static Sprite Get(string key) {
            return GetSpriteAbility(key) ?? GetSpriteCreature(key) ?? GetSpriteIcon(key) ?? GetSpriteItem(key) ?? GetSpriteWorld(key);
        }
        #endregion
    }
}
