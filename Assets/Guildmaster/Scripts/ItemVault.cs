﻿
namespace Assets.Guildmaster.Scripts {
    using Mech.Items;
    using System.Collections.Generic;
    using System.IO;
    using System.Runtime.Serialization.Formatters.Binary;
    using Utility;

    public static class ItemVault {
        #region Fields
        public const string DefaultFilePath = "items.vault";
        private static Dictionary<string, Item> items;
        private static List<Item> itemList;
        private static Item item;
        #endregion

        #region Accessors
        public static List<Item> Items { get { return itemList; } }
        #endregion

        #region Methods
        public static void Initialize(string filepath = DefaultFilePath) {
            if (filepath == string.Empty)
                return;
            BinaryFormatter bformatter = new BinaryFormatter();
            bformatter.Binder = new VersionDeserializationBinder();
            var stream = new FileStream(filepath, FileMode.Open);
            itemList = bformatter.Deserialize(stream) as List<Item>;
            stream.Close();
            items = new Dictionary<string, Item>();
            if (itemList != null) {
                for (int i = 0; i < itemList.Count; ++i) {
                    items.Add(itemList[i].ItemKey, itemList[i]);
                }
            } else {
                itemList = new List<Item>();
            }
        }

        public static Item Get(string key) {
            if (items.TryGetValue(key, out item)) {
                return item;
            } else {
                return null;
            }
        }
        #endregion
    }
}
