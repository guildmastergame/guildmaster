﻿namespace Assets.Guildmaster.Scripts.Presentation.Battle {
    using Scripts.Battle;
    using UnityEngine;
    using UnityEngine.UI;

    public class InitiativePanel : MonoBehaviour {
        public GameObject[] goIniIcons;
        private Image[] imgIcons;
        public int Capacity { get { return imgIcons.Length; } }//how much creatures can be shown

        void Awake() {
            imgIcons = new Image[goIniIcons.Length];
            for(int i = 0; i < goIniIcons.Length; ++i) {
                imgIcons[i] = goIniIcons[i].GetComponent<Image>();
            }
        }

        public void Set(int idx, BattleCreature creature) {
            if (idx < 0 || idx >= Capacity) {
                Debug.LogError("Set creature attempt: out of bounds: " + idx);
                return;
            }
            goIniIcons[idx].SetActive(true);
            imgIcons[idx].sprite = creature.ImagePortrait;
        }
        public void Hide(int idx) {
            if (idx < 0 || idx >= Capacity) {
                Debug.LogError("Hide creature attempt: out of bounds: " + idx);
                return;
            }
            goIniIcons[idx].SetActive(false);
        }

    }
}
