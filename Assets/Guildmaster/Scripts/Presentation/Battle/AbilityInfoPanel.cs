﻿namespace Assets.Guildmaster.Scripts.Presentation.Battle {
    using Mech.Items;
    using Mech.Abilities;
    using UnityEngine;
    using UnityEngine.UI;
    using Mech.Abilities.Effects;
    using System.Collections.Generic;
    using Mech;
    using Scripts.Battle;
    using Mech.Entities.Creatures;

    public class AbilityInfoPanel : MonoBehaviour {
        //public AbilityPanel pAbilities;
        public Text textName;
        public Text textItemType;
        public Text textAbilityType;
        public Text textCooldown;
        public Text textTargetType;
        public Text textAvailablePosition;
        public GameObject goEfxLabel1;
        public GameObject goEfxLabel2;
        public GameObject[] goEfxDesc1;
        public GameObject[] goEfxDesc2;
        public GameObject goDescription;
        private Text textEfxLabel1;
        private Text textEfxLabel2;
        private Text[] textEfxDesc1;
        private Text[] textEfxDesc2;
        private Text textDescription;

        private ICreature curCreature;
        private int curAbilityIdx;
        public void Hide() {
            gameObject.SetActive(false);
        }

        public void Awake() {
            textEfxLabel1 = goEfxLabel1.GetComponent<Text>();
            textEfxLabel2 = goEfxLabel2.GetComponent<Text>();
            textEfxDesc1 = new Text[goEfxDesc1.Length];
            for (int i = 0; i < goEfxDesc1.Length; ++i) {
                textEfxDesc1[i] = goEfxDesc1[i].GetComponent<Text>();
            }
            textEfxDesc2 = new Text[goEfxDesc2.Length];
            for (int i = 0; i < goEfxDesc2.Length; ++i) {
                textEfxDesc2[i] = goEfxDesc2[i].GetComponent<Text>();
            }
            textDescription = goDescription.GetComponent<Text>();
        }

        /// <param name="first"> if true - first effects list. if false - second</param>
        private void InitializeEffects(bool first, List<Effect> efx, string labelTitle, bool isInActives) {
            if(efx.Count == 0) {
                return;
            }
            if (first) {
                goEfxLabel1.SetActive(true);
                textEfxLabel1.text = labelTitle;
            } else {
                goEfxLabel2.SetActive(true);
                textEfxLabel2.text = labelTitle;
            }
            var gos = first ? goEfxDesc1 : goEfxDesc2;
            var txt = first ? textEfxDesc1 : textEfxDesc2;
            int n = Mathf.Min(gos.Length, efx.Count);
            for (int i = 0; i < n; ++i) {
                gos[i].SetActive(true);
                txt[i].text = efx[i].GetDescription(curCreature, curAbilityIdx, isInActives) ;
            }
        }


        /// <param name="first"> if true - first effects list. if false - second</param>
        private void ResetEffects(bool first) {
            if(first) {
                goEfxLabel1.SetActive(false);
                textEfxLabel1.text = string.Empty;
            } else {
                goEfxLabel2.SetActive(false);
                textEfxLabel2.text = string.Empty;
            }
            var gos = first ? goEfxDesc1 : goEfxDesc2;
            var txt = first ? textEfxDesc1 : textEfxDesc2;
            for (int i = 0; i < gos.Length; ++i) {
                gos[i].SetActive(false);
                txt[i].text = string.Empty;
            }
        }

        private void InitializeItem(Item item) {
            var ab = item.Ability;
            InitializeAbility(ab, true);
            textItemType.text = item.Type.ToString();
            textName.text = item.Name.ToString();
            textDescription.text = item.Desc.ToString();
        }

        private void InitializeAbility(Ability ab, bool isItemAbility = false) {
            if (!isItemAbility) {
                textItemType.text = "Ability";
                textName.text = ab.Name.ToString();
                textDescription.text = ab.Desc.ToString();
            }
            textCooldown.text = ab.Cooldown == 0 ? string.Empty : string.Format("{0} turn{1}", ab.Cooldown, ab.Cooldown > 1 ? "s" : string.Empty);
            textAbilityType.text = ab.Type.ToString();
            textTargetType.text = string.Format("Target: {0}", ab.Target.ToString());
            textAvailablePosition.text = ab.AvailablePos == BattlePositionType.General ? string.Empty : ab.AvailablePos == BattlePositionType.Front ? "Front only" : "Rear only";
            ResetEffects(true);
            ResetEffects(false);
            switch (ab.Type) {
                case AbilityType.Passive:
                    InitializeEffects(true, (ab as AbilityPassive).Passives, "Passives", false);
                    break;
                case AbilityType.Toggle:
                    InitializeEffects(true, (ab as AbilityToggle).Activated, "Activated", false);
                    InitializeEffects(false, (ab as AbilityToggle).Deactivated, "Deactivated", false);
                    break;
                case AbilityType.Undefined:
                    Debug.LogError("AbilityInfoPanel.cs: InitializeAbility - AbilityType is Undefined");
                    break;
                default:
                    InitializeEffects(true, (ab as AbilityActive).Actives, "Actives", true);
                    InitializeEffects(false, (ab as AbilityActive).Passives, "Passives", false);
                    break;
            }
        }

        private void InitializeInfo(AbilitySlot abSlot, Vector3 iconPos) {
            gameObject.SetActive(true);
            transform.position = iconPos;
            if(abSlot.AllowedItems == 0) {
                InitializeAbility(abSlot.Ability);
            } else {
                InitializeItem(abSlot.Item);
            }
            //TODO: set position
        }

        public void InitializeInfo(int abilityIdx, AbilitySlot abSlot, AbilityPanel abPanel) {
            
            curCreature = (abPanel.BattleCreature == null ? (ICreature)abPanel.Creature : (ICreature)abPanel.BattleCreature);
            curAbilityIdx = abilityIdx;
            var iconPos = (abPanel.goAbilityIcons[abilityIdx].transform as RectTransform).position;
            iconPos.y = iconPos.y + 1f;
            InitializeInfo(abSlot, iconPos);
        }
    }
}
