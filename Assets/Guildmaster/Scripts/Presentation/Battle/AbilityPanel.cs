﻿namespace Assets.Guildmaster.Scripts.Presentation.Battle {
    using Control;
    using Mech;
    using Scripts.Battle;
    using UnityEngine;
    using UnityEngine.UI;

    public class AbilityPanel : MonoBehaviour {
        public BattleController cBattle;
        public AbilityInfoPanel pAbilityInfo;
        public GameObject[] goAbilityIcons;
        private Text[] textCDs;
        private Image[] imgIcons;
        private Button[] buttons;
        private BattleCreature bc;
        private Creature creature;

        public Creature Creature { get { return creature; } }
        public BattleCreature BattleCreature { get { return bc; } }

        void Awake() {
            textCDs = new Text[goAbilityIcons.Length];
            imgIcons = new Image[goAbilityIcons.Length];
            buttons = new Button[goAbilityIcons.Length];
            for (int i = 0; i < goAbilityIcons.Length; ++i) {
                textCDs[i] = goAbilityIcons[i].GetComponentInChildren<Text>();
                imgIcons[i] = goAbilityIcons[i].GetComponent<Image>();
                buttons[i] = goAbilityIcons[i].GetComponent<Button>();
            }
        }

        public void SetAbilities(BattleCreature creature) {
            this.bc = creature;
            this.creature = null;
            for (int i = 0; i < creature.AbilitySlots.Count; ++i) {
                if (creature.AbilitySlots[i] == null) {
                    continue;
                }
                var ab = creature.AbilitySlots[i].Ability;
                buttons[i].image.sprite = creature.AbilitySlots[i].Image;
                bool isAvailable = ab.Available(creature.Position.Type);
                buttons[i].interactable = isAvailable;
                if (!isAvailable) {
                    int cd = ab.CurrentCooldown;
                    if (cd > 0) {
                        textCDs[i].text = cd.ToString();
                        continue;
                    }
                }
                textCDs[i].text = string.Empty;
            }
            ShowPanel();
        }

        public void SetAbilities(Creature creature) {
            this.bc = null;
            this.creature = creature;
            for (int i = 0; i < creature.AbilitySlots.Count; ++i) {
                if (creature.AbilitySlots[i] == null) {
                    continue;
                }
                
                var ab = creature.AbilitySlots[i].Ability;
                buttons[i].image.sprite = creature.AbilitySlots[i].Image;
                bool isAvailable = true;//ab.Available(BattlePositionType.General);
                buttons[i].interactable = isAvailable;
                if (!isAvailable) {
                    int cd = ab.CurrentCooldown;
                    if (cd > 0) {
                        textCDs[i].text = cd.ToString();
                        continue;
                    }
                }
                textCDs[i].text = string.Empty;
            }
            ShowPanel();
        }

        public void ShowPanel() {
            var abSlots = bc == null ? creature.AbilitySlots : bc.AbilitySlots;
            for (int i = 0; i < abSlots.Count; ++i) {
                goAbilityIcons[i].SetActive(true);
            }
            for (int i = abSlots.Count; i < goAbilityIcons.Length; ++i) {
                goAbilityIcons[i].SetActive(false);
            }
        }
        public void Hide() {
            for (int i = 0; i < goAbilityIcons.Length; ++i) {
                goAbilityIcons[i].SetActive(false);
            }
        }
        /// <summary>
        /// Called from the ui
        /// </summary>
        /// <param name="idx">index of the ability button</param>
        public void ButtonClicked(int idx) {
            if (cBattle == null) {
                return;// it's not the battle. Don't do anything
            }
            cBattle.TryUseAbility(bc, idx);
        }
        /// <summary>
        /// Called from the ui
        /// </summary>
        /// <param name="idx">index of the ability button</param>
        public void ButtonHovered(int idx) {
            pAbilityInfo.InitializeInfo(idx, bc == null ? creature.AbilitySlots[idx] : bc.AbilitySlots[idx], this);
        }
        /// <summary>
        /// Called from the ui
        /// </summary>
        /// <param name="idx">index of the ability button</param>
        public void ButtonUnhovered(int idx) {
            pAbilityInfo.Hide();
        }

        private bool isLocked = false;

        public void Lock() {
            if (bc != null) {
                isLocked = true;
                var slots = bc.AbilitySlots;
                for (int i = 0; i < slots.Count; ++i) {
                    buttons[i].interactable = false;
                }
            }
        }

        public void Unlock() {
            if (bc != null && isLocked) {
                SetAbilities(bc);
            }
        }
    }
}
