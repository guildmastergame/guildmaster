﻿namespace Assets.Guildmaster.Scripts.Presentation {
    using Mech.Containers;
    using UnityEngine;
    using UnityEngine.EventSystems;
    using UnityEngine.UI;

    public class DragAndDropHandler : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IDropHandler {
        public static DragAndDropHandler dragging = null;
        public static Vector2 DEFAULT_ICON_SIZE = new Vector2(80, 100);
        protected static Transform OnTopParent = null;
        protected static Canvas OnTopParentCanvas;
        protected static Transform NativeObjectParent = null;
        protected static float SideOffset = 0f;
        /// <summary>
        /// Target draggable GameObject
        /// </summary>
        public GameObject goTarget;
        protected Image img;
        protected RectTransform tForm;
        protected bool isAvailable;
        protected Container container = null;
        protected Vector2? imgSize = null;


        void Awake() {
            img = goTarget.GetComponent<Image>();
            img.raycastTarget = false;
            tForm = goTarget.GetComponent<RectTransform>();
            isAvailable = false;
        }

        public void Initialize(bool isAvailable, Container container, Vector2? imgSize = null) {
            this.isAvailable = isAvailable;
            this.container = container;
            var entityImg = container.Image;
            if (entityImg != null && img != null) {
                img.sprite = entityImg;
            }
            if (isAvailable && tForm != null) {
                tForm.sizeDelta = imgSize.HasValue ? imgSize.Value : DEFAULT_ICON_SIZE;
            }
            goTarget.SetActive(false);
        }

        public void OnBeginDrag(PointerEventData eventData) {
            if (!isAvailable) {
                return;
            }
            goTarget.SetActive(true);
            dragging = this;//gameObject;           
            img.color = Color.white;
            if (isAvailable && imgSize.HasValue) {
                tForm.sizeDelta = imgSize.Value;
            }
            NativeObjectParent = tForm.parent;
            tForm.SetParent(OnTopParent);//.parent
        }

        public void OnDrag(PointerEventData eventData) {
            if (!isAvailable) {
                return;
            }
            Vector3 pos = tForm.localPosition;
            pos.x = eventData.position.x - SideOffset;
            pos.y = eventData.position.y - Screen.height;
            tForm.localPosition = pos;
        }

        public void OnDrop(PointerEventData eventData) {
            if (dragging == null) {
                return;
            }
            container.Swap(dragging.container);
        }

        public void OnEndDrag(PointerEventData eventData) {
            if (dragging == null) {
                return;
            }
            tForm.SetParent(NativeObjectParent);//.parent
            tForm.localPosition = Vector3.zero;
            img.color = new Color(1, 1, 1, 0);
            dragging = null;
            goTarget.SetActive(false);
        }

        public static void SetOnTopParent(RectTransform _OnTopParent, Canvas _OnTopParentCanvas) {
            OnTopParent = _OnTopParent;
            OnTopParentCanvas = _OnTopParentCanvas;
            SideOffset = (Screen.width - (OnTopParentCanvas.transform as RectTransform).sizeDelta.x) / 2f;
        }
    }
}
