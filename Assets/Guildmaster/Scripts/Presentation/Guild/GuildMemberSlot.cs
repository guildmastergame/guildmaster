﻿namespace Assets.Guildmaster.Scripts.Presentation.Guild {
    using Utility;
    using UnityEngine;
    using UnityEngine.UI;

    public class GuildMemberSlot : MonoBehaviour {
        protected static Color FADE_COLOR = new Color(0.5f, 0.5f, 0.5f, 1);
        protected static Color NO_CREATURE_COLOR = new Color(0.3f, 0.3f, 0.3f, 0.7f);
        public Image imgCreature;
        public Button button;
        public CustomEventTrigger eventTrigger;
        public DragAndDropHandler dndHandler;

        public void SetImage(Sprite sprite) {
            imgCreature.sprite = sprite;
            ResetImageColor();
        }

        public void Reset() {
            imgCreature.sprite = null;
            ResetImageColor();
        }

        public void FadeImageColor() {
            imgCreature.color = FADE_COLOR;
        }

        public void ResetImageColor() {
            if (imgCreature.sprite == null) {
                imgCreature.color = NO_CREATURE_COLOR;
            } else {
                imgCreature.color = Color.white;
            }
        }
    }
}
