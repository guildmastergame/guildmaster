﻿namespace Assets.Guildmaster.Scripts.Presentation.Guild {
    using Mech.Quests;
    using UnityEngine;
    using UnityEngine.UI;

    public class QuestObjectiveSlot : MonoBehaviour {
        public Image imgIcon;
        public Text textName;

        public void Show(Quest o) {
            gameObject.SetActive(true);
            imgIcon.sprite = SpriteVault.GetSpriteIcon("flag");
            textName.text = o.Name.ToString();
        }

        public void Hide() {
            gameObject.SetActive(false);
        }
    }
}
