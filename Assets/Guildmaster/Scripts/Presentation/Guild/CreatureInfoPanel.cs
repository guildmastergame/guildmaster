﻿namespace Assets.Guildmaster.Scripts.Presentation.Guild {
    using Mech;
    using Battle;
    using UnityEngine;
    using UnityEngine.UI;
    using Scripts.Battle;
    using Mech.Attributes;
    using Mech.Abilities;
    using Mech.Abilities.Effects;

    public class CreatureInfoPanel : MonoBehaviour {
        #region Fields
        public GameObject goParent;
        public GameObject goDamageIcon;
        public GameObject goInitiativeIcon;
        public GameObject goActionsIcon;
        public GameObject goHealthIcon;
        public GameObject goArmorIcon;
        public GameObject goProtectionIcon;
        public Image imgCreature;
        public Text textName;
        public AbilityPanel abPanel;
        public Text textScrollViewLabel;
        public GameObject goDescription;
        public GameObject[] goTempEffects;
        public GameObject spacerRightSide;
        private Text textDamage;
        private Text textInitiative;
        private Text textActions;
        private Text textHealth;
        private Text textArmor;
        private Text textProtection;
        private Text textDescription;
        private Text[] textTempEffects;
        private Creature lockedCreature = null;

        #endregion

        #region Accessors
        public bool IsHidden { get { return !goParent.activeSelf; } }
        #endregion

        #region Methods
        public void Awake() {
            textDamage = goDamageIcon.GetComponentInChildren<Text>();
            textInitiative = goInitiativeIcon.GetComponentInChildren<Text>();
            textActions= goActionsIcon.GetComponentInChildren<Text>();
            textHealth = goHealthIcon.GetComponentInChildren<Text>();
            textArmor = goArmorIcon.GetComponentInChildren<Text>();
            textProtection = goProtectionIcon.GetComponentInChildren<Text>();
            textDescription = goDescription.GetComponentInChildren<Text>();
            textTempEffects = new Text[goTempEffects.Length]; 
            for (int i = 0; i < goTempEffects.Length; ++i) {
                textTempEffects[i] = goTempEffects[i].GetComponent<Text>();
            }
        }

        public void Initialize(Creature c, bool lockCreature = false) {
            if (lockCreature) {
                this.lockedCreature = c;
            }
            if (c == null) {
                Reset();
                return;
            }
            gameObject.SetActive(true);
            textHealth.text = c.Health.ToString();
            var armor = c.GetCurrentArmor();
            if (armor > 0) {
                goArmorIcon.SetActive(true);
                textArmor.text = armor.ToString();
            } else {
                goArmorIcon.SetActive(false);
            }
            var protection = c.GetCurrentArmor(DamageType.All);
            if (protection > 0) {
                goProtectionIcon.SetActive(true);
                textProtection.text = protection.ToString();
            } else {
                goProtectionIcon.SetActive(false);
            }
            textInitiative.text = c.Ini.ToString();
            textActions.text = c.Actions.ToString();
            int mainActionIdx = c.GetMainActionIdx();
            if(mainActionIdx >= 0) {
                var mAbility = c.AbilitySlots[mainActionIdx].Ability as AbilityActive;
                var damageEfx = mAbility.GetActiveEffectsOfType<EffectDamage>(EffectType.Damage);
                if (damageEfx.Count > 0) {
                    //Pick a first one for simplicity now
                    textDamage.text = damageEfx[0].Damage.GetAsColoredString(c, mAbility.Type);
                    goDamageIcon.SetActive(true);
                } else { //it's not damage dealing ability. No we will just skip it 
                    goDamageIcon.SetActive(false);
                }
            } else { //creature has no active abilities 
                goDamageIcon.SetActive(false);
            }
            textName.text = c.Name.ToString();
            textDescription.text = c.Desc.ToString();
            imgCreature.sprite = c.ImagePortrait;
            var tempEfx = c.StartEffects;
            int n = Mathf.Min(goTempEffects.Length, tempEfx.Count);
            for (int i = 0; i < n; ++i) {
                goTempEffects[i].SetActive(true);
                textTempEffects[i].text = tempEfx[i].GetDescriptionAsTemporary(c);
            }
            for(int i = n; i < goTempEffects.Length; ++i) {
                goTempEffects[i].SetActive(false);
            }
            abPanel.SetAbilities(c);
        }
        
        /// <param name="hideAfterClear">true if creatureInfo should be hidden if no locked creature exists</param>
        /// <returns>true if creatureInfoPanel is hidden now</returns>
        public bool Clear(bool hideAfterClear = false) {
            if(lockedCreature == null) {
                Reset();
                if(hideAfterClear) {
                    Hide();
                    return true;
                }
            } else {
                Initialize(lockedCreature);
            }
            return false;
        }

        public void Reset() {
            gameObject.SetActive(false);
            lockedCreature = null;
        }

        public void Initialize(BattleCreature bc) {
            gameObject.SetActive(true);
            textHealth.text = bc.Health.ToString();
            textArmor.text = bc.GetCurrentArmor().ToString();
            textProtection.text = bc.GetCurrentArmor(DamageType.All).ToString();
            textInitiative.text = bc.Ini.ToString();
            textActions.text = bc.ActionsLeft.ToString();
            var mAbility = bc.MainAbility;
            if(mAbility != null) {
                var damageEfx = mAbility.GetActiveEffectsOfType<EffectDamage>(EffectType.Damage);
                if (damageEfx.Count > 0) {
                    //Pick a first one for simplicity now
                    textDamage.text = damageEfx[0].Damage.GetAsColoredString(bc, mAbility.Type);
                    goDamageIcon.SetActive(true);
                } else { //it's not damage dealing ability. No we will just skip it 
                    goDamageIcon.SetActive(false);
                }
            } else {
                goDamageIcon.SetActive(false);
            }
            textName.text = bc.Name;
            textDescription.text = bc.Desc;
            imgCreature.sprite = bc.ImagePortrait;
        }

        public void Show() {
            spacerRightSide.SetActive(false);
            goParent.SetActive(true);
            Reset();
        }

        public void Hide() {
            goParent.SetActive(false);
            spacerRightSide.SetActive(true);
        }
        #endregion
    }
}
