﻿namespace Assets.Guildmaster.Scripts.Presentation.Guild {
    using Mech;
    using UnityEngine;
    using UnityEngine.UI;

    public class RewardSlot : MonoBehaviour {
        public Image imgIcon;
        public Text textQuantity;

        public void Show(Loot loot) {
            imgIcon.sprite = loot.Image;
            textQuantity.text = loot.Quantity.ToString();
            gameObject.SetActive(true);
        }

        public void Hide() {
            gameObject.SetActive(false);
        }
    }
}
