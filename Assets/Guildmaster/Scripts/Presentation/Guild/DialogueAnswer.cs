﻿namespace Assets.Guildmaster.Scripts.Presentation.Guild {
    using Control;
    using Mech.Quests.Dialogues;
    using UnityEngine;
    using UnityEngine.UI;

    public class DialogueAnswer : MonoBehaviour {
        protected DialogueChoice choice;
        public DialogueController cDialogue = null;
        public Image imgIcon;
        public Text textAnswer;
        public Button button;

        public void ButtonPressed() {
            cDialogue.SetNodeIdx(choice.Select(cDialogue.GetParty()));
        }

        public void Show(DialogueChoice choice) {
            this.choice = choice;
            gameObject.SetActive(true);
            textAnswer.text = choice.Text.ToString();
        }

        public void Hide() {
            gameObject.SetActive(false);
        }
    }
}
