﻿namespace Assets.Guildmaster.Scripts.Presentation.Guild {
    using Mech.Entities.Creatures;
    using Mech;
    using UnityEngine;
    using UnityEngine.UI;
    using UnityEngine.EventSystems;
    using UnityEngine.Events;

    public class HireCreaturePanel : MonoBehaviour {
        protected Image imgCreature;
        protected Button buttonHire;
        protected Text textButtonHire;
        protected HiringSlot slot;

        public GameObject goImageCreature;
        public GameObject goButtonHire;
        public Sprite imgNoCreature;
        public EventTrigger eventTrigger;

        public HiringSlot HireSlot { get { return slot; } }
        public Creature Creature { get { return slot.Creature; } }
        public int Price { get { return slot.Price; } }

        /// <summary>
        /// Call it from Hire contolller before any operations over panel
        /// </summary>
        public void Initialize() {
            imgCreature = goImageCreature.GetComponent<Image>();
            buttonHire = goButtonHire.GetComponent<Button>();
            textButtonHire = goButtonHire.GetComponentInChildren<Text>();
        }

        public void SetHireCallbacks(UnityAction hirePressed, UnityAction<BaseEventData> portraitClick, UnityAction<BaseEventData> portraitHover, UnityAction<BaseEventData> portraitUnhover) {
            buttonHire.onClick.RemoveAllListeners();
            buttonHire.onClick.AddListener(hirePressed);
            eventTrigger.triggers.Clear();
            EventTrigger.Entry pClick = new EventTrigger.Entry();
            pClick.eventID = EventTriggerType.PointerClick;
            pClick.callback.AddListener(portraitClick);
            eventTrigger.triggers.Add(pClick);
            EventTrigger.Entry pHover = new EventTrigger.Entry();
            pHover.eventID = EventTriggerType.PointerEnter;
            pHover.callback.AddListener(portraitHover);
            eventTrigger.triggers.Add(pHover);
            EventTrigger.Entry pUnhover = new EventTrigger.Entry();
            pUnhover.eventID = EventTriggerType.PointerExit;
            pUnhover.callback.AddListener(portraitUnhover);
            eventTrigger.triggers.Add(pUnhover);
        }

        public void SetCreature(HiringSlot slot) {
            this.slot = slot;
            goImageCreature.SetActive(true);
            goButtonHire.SetActive(true);
            imgCreature.sprite = Creature.ImagePortrait;
            textButtonHire.text = Price.ToString();
        }

        public void ResetCreature() {
            imgCreature.sprite = imgNoCreature;
            goButtonHire.SetActive(false);
            slot = null;
        }

        public void UpdatePriceButton(int moneyAvailable) {
            buttonHire.interactable = Price <= moneyAvailable;
        }
    }
}
