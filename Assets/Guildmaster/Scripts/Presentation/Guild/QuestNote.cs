﻿namespace Assets.Guildmaster.Scripts.Presentation.Guild {
    using UnityEngine;
    using UnityEngine.UI;
    using Mech.Quests;

    /// <summary>
    /// Visual representation of quest letter on Quests panel
    /// </summary>
    public class QuestNote : MonoBehaviour {
        protected QuestTree qTree;
        public Button button;
        public Image imgNote;
        public Image imgStatus;

        public QuestTree QTree { get { return qTree; } }

        public void Show(QuestTree qTree) {
            this.qTree = qTree;
            //imgNote.sprite = q.Image;
            //switch (qTree.Status) {
            //    case QuestStatus.InProgress:
            //        imgStatus.color = Color.white;
            //        break;
            //    default:
            imgStatus.color = new Color(1, 1, 1, 0);
            //        break;
            //}
            gameObject.SetActive(true);
        }

        public void Hide() {
            qTree = null;
            gameObject.SetActive(false);
        }
    }
}
