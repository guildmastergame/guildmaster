﻿namespace Assets.Guildmaster.Scripts.Presentation.Guild {
    using Mech;
    using Mech.Quests;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.UI;
    using System.Linq;

    public class QuestObjectivePanel : MonoBehaviour {
        #region Fields
        protected QuestTree qTree;
        protected Quest qNode;
        public CreatureInfoSlot[] hostiles;
        public Button actionButton;
        public GameObject labelHostiles;
        public Text textObjective;
        public Text textActionButton;
        public Text textDescription;
        public GameObject spacerRightSide;
        #endregion
        #region Methods
        void Update() {
            if(qTree.Party.CheckIsEmpty()) {
                if (qTree.IsReadyToEmbark) {
                    qTree.IsReadyToEmbark = false;
                    SetupActionButton();
                } else if (actionButton.interactable) {
                    actionButton.interactable = false;
                }
            } else if(!actionButton.interactable) {
                actionButton.interactable = true;
            }
        }

        private void InitializeDialogue(QuestDialogue q) {
            HideHostiles();
        }

        private void InitializeSkirmish(QuestSkirmish q) {
            HideHostiles();
            labelHostiles.SetActive(true);
            List<Creature> uniqueCreatures = new List<Creature>();
            var foes = q.Foes[0]; //TODO: fix scouting foes info. Now it displays only foes from first option of fights.
            for (int i = 0; i < Party.MaxHeight; ++i) {
                int offset = i * Party.MaxWidth;
                for (int j = 0; j < Party.MaxWidth; ++j) {
                    var creature = foes.GetCreature(j, i);
                    if (creature != null && !uniqueCreatures.Any(c => string.Compare(c.ImageKey, creature.ImageKey) == 0)) {
                        uniqueCreatures.Add(creature);
                    } 
                }
            }
            int n = Mathf.Min(uniqueCreatures.Count, hostiles.Length);
            for(int i = 0; i < n; ++i) {
                hostiles[i].Show(uniqueCreatures[i]);
            }
        }

        private void HideHostiles() {
            labelHostiles.gameObject.SetActive(false);
            for (int i = 0; i < hostiles.Length; ++i) {
                hostiles[i].Hide();
            }
        }

        private void SetupActionButton() {
            if (!qTree.IsReadyToEmbark) {
                switch (qNode.Type) {
                    case QuestType.Dialogue:
                        textActionButton.text = "Talk";
                        break;
                    case QuestType.Skirmish:
                        textActionButton.text = "Fight";
                        break;
                }
            } else {
                textActionButton.text = "Recall";
            }
        }

        /// <summary>
        /// use the last shown quest tree by default
        /// </summary>
        public void Show() {
            spacerRightSide.SetActive(false);
            Show(qTree);
        }

        public void Show(QuestTree qTree) {
            this.qTree = qTree;
            this.qNode = qTree.CurrentNode;
            gameObject.SetActive(true);
            textObjective.text = qNode.Name.ToString();
            textDescription.text = qNode.Desc.ToString();
            switch (qNode.Type) {
                case QuestType.Dialogue:
                    InitializeDialogue(qNode as QuestDialogue);
                    break;
                case QuestType.Skirmish:
                    InitializeSkirmish(qNode as QuestSkirmish);
                    break;
            }
            SetupActionButton();
        }

        public void ActionButtonPressed() {
            qTree.IsReadyToEmbark = !qTree.IsReadyToEmbark;
            SetupActionButton();
        }

        public void Hide() {
            gameObject.SetActive(false);
            spacerRightSide.SetActive(true);
        }
        #endregion
    }
}
