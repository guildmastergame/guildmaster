﻿namespace Assets.Guildmaster.Scripts.Presentation.Guild {
    using Utility;
    using Control;
    using Mech;
    using Mech.Quests;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.UI;

    public class QuestInfoPanel : MonoBehaviour {
        #region Fields
        public GuildController gc;
        protected QuestTree qTree;
        [Tooltip("{0} - number of days left")]
        public string overdueFormat = "Last message received {0} days ago";
        public Text textName;
        public Text textReportStatus;
        public Text textDescription;
        public RewardSlot[] rewardSlots;
        public QuestObjectiveSlot[] objectiveSlots;
        public RectTransform trMap;

        #endregion


        #region Methods
        protected void ClampMapPosition() {
            if (!MapController.MapBounds.InBound(trMap.anchoredPosition)) {
                trMap.anchoredPosition = MapController.MapBounds.Clamp(trMap.anchoredPosition);
            }
        }

        protected void InitializeQuestData(QuestTree qTree) {
            qTree.RecalculateQuestStart();
            textName.text = qTree.Name;

            textDescription.text = qTree.GetDescription();
            textReportStatus.text = "No new reports";//string.Format(overdueFormat, q.Overdue);

            var qStart = qTree.CurrentStart;

            List<Loot> rewards = new List<Loot>(2);
            if (qStart.RewardMoney > 0) {
                rewards.Add(new Loot(LootType.Money, qStart.RewardMoney));
            }
            if (qStart.RewardFame > 0) {
                rewards.Add(new Loot(LootType.Fame, qStart.RewardFame));
            }
            int i;
            for (i = 0; i < rewards.Count; ++i) {
                rewardSlots[i].Show(rewards[i]);
            }
            for (; i < rewardSlots.Length; ++i) {
                rewardSlots[i].Hide();
            }

            //map
            trMap.anchoredPosition = new Vector2(trMap.sizeDelta.x / 2 - qStart.MapMarkerPosition.x, trMap.sizeDelta.y / 2 - qStart.MapMarkerPosition.y);
            ClampMapPosition();


            var objs = qTree.GetObjectives();
            for (i = 0; i < objs.Count; ++i) {
                objectiveSlots[i].Show(objs[i]);
            }
            for (; i < objectiveSlots.Length; ++i) {
                objectiveSlots[i].Hide();
            }
        }

        public void Show(QuestTree qTree) {
            this.qTree = qTree;
            gameObject.SetActive(true);
            InitializeQuestData(qTree);
        }
        
        public void Hide() {
            gameObject.SetActive(false);
        }

        public void ShowOnMap() {
            Hide();
            gc.ShowMap(qTree.CurrentNode.MapMarkerPosition);
        }
        #endregion

    }
}
