﻿namespace Assets.Guildmaster.Scripts.Presentation.Guild {
    using Mech;
    using UnityEngine;
    using UnityEngine.UI;

    public class CreatureInfoSlot : MonoBehaviour {
        protected Creature creature;
        public Image image;
        public Button button;

        public void Show(Creature creature) {
            gameObject.SetActive(true);
            this.creature = creature;
            image.sprite = creature.ImagePortrait;
        }

        public void Hide() {
            gameObject.SetActive(false);
        }
    }
}
