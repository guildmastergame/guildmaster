﻿namespace Assets.Guildmaster.Scripts.Presentation.Map {
    using Mech.Map;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.Events;

    public class MapMarkerPool : MonoBehaviour {
        public List<MapMarker> markers;

        public void ShowMarker(MapMarkerData marker, UnityAction clickCallback) {
            for (int i = 0; i < markers.Count; ++i) {
                if (!markers[i].IsActive) {
                    markers[i].Show(marker.Icon, marker.Position, clickCallback);
                    return;
                }
            }
            throw new System.Exception("Not enough markers in a pool!");
            //TODO: pool extension
        }

        public void Reset() {
            for(int i = 0; i < markers.Count; ++i) {
                markers[i].Hide();
            }
        }
    }
}
