﻿namespace Assets.Guildmaster.Scripts.Presentation.Map {
    using UnityEngine.UI;
    using UnityEngine;
    using UnityEngine.Events;

    public class MapMarker : MonoBehaviour {
        public Image image;
        public Button button;

        public bool IsActive { get; set; }

        public void Show(Sprite img, Vector3 pos, UnityAction clickCallback) {
            if (!gameObject.activeSelf) {
                Hide();
            }
            IsActive = true;
            gameObject.SetActive(true);
            transform.localPosition = pos;
            image.sprite = img;
            button.onClick.AddListener(clickCallback);
        }

        public void Hide() {
            IsActive = false;
            gameObject.SetActive(false);
            button.onClick.RemoveAllListeners();
        }
    }
}
