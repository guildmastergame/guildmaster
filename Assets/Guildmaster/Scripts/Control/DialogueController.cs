﻿namespace Assets.Guildmaster.Scripts.Control {
    using UnityEngine;
    using UnityEngine.UI;
    using Presentation.Guild;
    using Mech.Quests;
    using Mech;

    public class DialogueController : MonoBehaviour {
        protected QuestDialogue qd;
        protected Party party;
        public GuildController cGuild;
        public Image imgMain;
        public Text textDesc;
        public DialogueAnswer[] answers;

        public Party GetParty() {
            return party;
        }

        public void SetNodeIdx(int idx) {
            if(idx == -1) {
                cGuild.DialogueFinished(qd.CurNode);
                Hide();
                return;
            }
            qd.SetCurrentNodeIdx(idx);
            UpdateQuestInfo();
        }

        private void HideAnswers() {
            for(int i = 0; i < answers.Length; ++i) {
                answers[i].Hide();
            }
        }

        public void UpdateQuestInfo() {
            HideAnswers();
            var node = qd.CurNode;
            textDesc.text = node.Text.ToString();
            var choices = node.Choices;
            int n = Mathf.Min(choices.Count, answers.Length);
            for(int i = 0; i < n; ++i) {
                answers[i].Show(choices[i]);
            }

        }

        public void Show(QuestDialogue qd, Party party) {
            this.qd = qd;
            gameObject.SetActive(true);
            UpdateQuestInfo();
        }

        public void Hide() {
            gameObject.SetActive(false);
        }
    }
}
