﻿namespace Assets.Guildmaster.Scripts.Control {
    public enum GameState {
        Undefined = 0,
        Guild,
        Quest,
        Battle
    }
}
