﻿namespace Assets.Guildmaster.Scripts.Control {
    using Utility;
    using UnityEngine;
    using Mech.Quests;
    using System.Collections.Generic;
    using Mech;
    using Presentation;

    [System.Serializable]
    public class MasterController : MonoBehaviour {
        public GuildController cGuild = null;
        public RectTransform dndOnTopParent = null;
        public Canvas dndOnTopCanvas = null;
        private GameState gameState;

        private List<QuestTree> qTrees = new List<QuestTree>();

        public GameState GameState { get { return gameState; } }

        void Awake() {
            DragAndDropHandler.SetOnTopParent(dndOnTopParent, dndOnTopCanvas);
            Language.Initialize();
            SpriteVault.Initialize();
            ItemVault.Initialize();
            CreatureVault.Initialize();
        }

        void Start() {
            AwakeTestGuild();
        }

        private void AwakeTestGuild() {
            gameState = GameState.Guild;
            cGuild.GameStarted(new GameData(cGuild));
            //battle test
        }

        //public void EmbarkQuest(int idx, Party party) {
        //    gameState = GameState.Quest;
        //    if (qTrees[idx].Status == QuestTreeStatus.Ready) {
        //        qTrees[idx].Embark(party);
        //    }
        //}

        public void TurnFinished() {
            for(int i = 0; i < qTrees.Count; ++i) {
                qTrees[i].TurnFinished();
            }
        }

        public void TurnStarted() {
            for (int i = 0; i < qTrees.Count; ++i) {
                qTrees[i].TurnStarted();
            }
        }

    }
}
