﻿namespace Assets.Guildmaster.Scripts.Control {
    using Utility;
    using Presentation.Map;
    using UnityEngine;
    using Mech.Quests;
    using UnityEngine.Events;

    public class MapController : MonoBehaviour {
        public GuildController gc = null;
        /// <summary>
        /// Map base point. the middle of the map should be here.
        /// </summary>
        public Vector2 identity;
        /// <summary>
        /// target game camera
        /// </summary>
        public Camera gCam;
        public MapMarkerPool markerPool;
        public GameObject goFadeImage;
        public GameObject goMapPanel;

        protected Vector2 size;
        protected Vector3 scale;
        protected bool lmbPressed;
        protected Vector3 dragPointPos;
        protected Vector3 dragPos;
        private RectTransform tForm;
        private bool mapFaded;

        public static BoundBox MapBounds;

        void Awake() {
            mapFaded = true;
            tForm = GetComponent<RectTransform>();
            tForm.position = identity;
            size = tForm.rect.size;
            scale = tForm.localScale;
            lmbPressed = false;
            float vertExtent = 100f * gCam.orthographicSize;
            float horzExtent = vertExtent * Screen.width / Screen.height;
            // Calculations assume map is position at the origin
            int minX = (int)(horzExtent / 2 + identity.x - size.x / 2.0f);
            int maxX = (int)(identity.x + size.x / 2.0f - horzExtent / 2);
            int minY = (int)((vertExtent + identity.y - size.y / 2) - vertExtent / 2);
            int maxY = (int)((identity.y + size.y / 2.0 - vertExtent) - vertExtent / 2);
            MapBounds = new BoundBox(minX, minY, maxX - minX, maxY - minY);
        }

        void Update() {
            if (mapFaded) {
                return;
            }
            if (Input.GetMouseButtonDown(0)) {
                dragPointPos = gCam.ScreenToWorldPoint(Input.mousePosition);
                dragPos = tForm.position;
                lmbPressed = true;
            } else if (Input.GetMouseButtonUp(0)) {
                lmbPressed = false;
            }
            if (lmbPressed) {
                DragFromPoint(gCam.ScreenToWorldPoint(Input.mousePosition));
            }
        }


        protected void ClampPosition() {
            if (!MapBounds.InBound(tForm.anchoredPosition)) {
                tForm.anchoredPosition = MapBounds.Clamp(tForm.anchoredPosition);
            }
        }

        protected void DragFromPoint(Vector3 point) {
            Vector3 transition = point - dragPointPos;
            Vector3 target = dragPos + transition;
            tForm.position = Vector3.Lerp(tForm.position, target, 0.5f);
            ClampPosition();
        }

        protected void QuestMarkerClicked(int idx) {
            gc.ShowQuestObjective(gc.gd.qTrees[idx]);
            //var obj = gc.gd.quests[idx].CurrentObjective as ObjectiveBattle;
            //if (obj.ExecutiveParty == null) {
            //    obj.ExecutiveParty = new Mechanics.Party();
            //}
            //gc.ShowPartyScreen(obj.ExecutiveParty);
            //gc.panelFightInfo.Show(gc.gd.quests[idx], obj);
        }

        public void Show(Vector2? mapPosition = null) {
            if (goMapPanel != null) {
                goMapPanel.SetActive(true);
            }
            lmbPressed = false;//Prevent lmb press check from previous game modes
            mapFaded = false;
            goFadeImage.SetActive(false);

            markerPool.Reset();

            var quests = gc.gd.qTrees; //Refresh quests
            for (int i = 0; i < quests.Count; ++i) {
                if (quests[i].Status == QuestTreeStatus.InProgress || quests[i].Status == QuestTreeStatus.Ready) {
                    int _i = i;
                    UnityAction callback = new UnityAction(() => QuestMarkerClicked(_i));
                    markerPool.ShowMarker(new Mech.Map.MapMarkerData(quests[i], tForm.sizeDelta), callback);
                }
            }

            if (mapPosition.HasValue) {
                tForm.anchoredPosition = new Vector2(tForm.sizeDelta.x / 2 - mapPosition.Value.x, tForm.sizeDelta.y / 2 - mapPosition.Value.y);
                ClampPosition();
            }
        }

        public void Hide() {
            if (goMapPanel != null) {
                goMapPanel.SetActive(false);
            }
            mapFaded = true;
            goFadeImage.SetActive(true);
        }
    }
}
