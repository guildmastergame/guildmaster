﻿namespace Assets.Guildmaster.Scripts.Control {
    using Presentation.Guild;
    using Mech;
    using UnityEngine;
    using UnityEngine.UI;
    using Mech.Containers;
    using UnityEngine.Events;

    public class GuildMembersController : MonoBehaviour {
        #region Fields
        //protected CreatureFilterEnum currentFilter = CreatureFilterEnum.All;
        [SerializeField]
        protected GuildController gc = null;
        /// <summary>
        /// is creature/tile over this index matches the current filters and exists at all then it is ready to be manipulated
        /// </summary>
        protected bool[] MatchFilter = new bool[Guild.MAX_MEMBERS];
        public CreatureInfoPanel pCreatureInfo;
        public GridLayoutGroup gridLayout = null;
        public GuildMemberSlot[] memberSlots = null;
        public GameObject goMembersParent = null;
        private QuestObjectivePanel pQuestObj = null;
        #endregion
        #region Accessors
        public GuildController GuildController { set { gc = value; } }
        #endregion

        void Awake() {
            for (int i = 0; i < memberSlots.Length; ++i) {
                memberSlots[i].SetImage(null);
                memberSlots[i].button.onClick.RemoveAllListeners();
                /*
                    http://forum.unity3d.com/threads/how-to-addlistener-featuring-an-argument.266934/
                    Q: Is the parameter passed into the delegate stored as a reference value?
                    A: Not quite. It's actually being captured in a "closure" that contains all that is needed for the delegate/anonymous method to be called at a later time. It's as if the variable "i" was silently converted to a field in an object that was then silently passed to the   delegates/anonymous methods when they were finally invoked.
                */
                int _i = i;
                memberSlots[i].button.onClick.AddListener(new UnityAction(() => GuildMemberClicked(_i)));
                memberSlots[i].eventTrigger.pointerEnter = new UnityAction(() => GuildMemberHovered(_i));
                memberSlots[i].eventTrigger.pointerExit = new UnityAction(() => GuildMemberUnhovered(_i));
            }
        }

        private void GuildMemberClicked(int idx) {
            var member = gc.Guild.GetMember(idx);
            if (member != null) {
                if(pQuestObj != null && pCreatureInfo.IsHidden) {
                    pQuestObj.Hide();
                    pCreatureInfo.Show();
                }
                pCreatureInfo.Initialize(member, true);
            } else if(pQuestObj != null && !pCreatureInfo.IsHidden) {
                pCreatureInfo.Hide();
                pQuestObj.Show();
            }
        }

        private void GuildMemberHovered(int idx) {
            var member = gc.Guild.GetMember(idx);
            if(member != null) {
                if(pCreatureInfo != null && pCreatureInfo.IsHidden) {
                    if (pQuestObj != null) {
                        pQuestObj.Hide();
                    }
                    pCreatureInfo.Show();
                }
                pCreatureInfo.Initialize(member);
            }
        }

        private void GuildMemberUnhovered(int idx) {
            if (pCreatureInfo.Clear(true) && pQuestObj != null) {
                pQuestObj.Show();
            }
        }

        public void UpdateSlot(int idx, Container c) {
            CreatureContainer creature = c as CreatureContainer;
            memberSlots[idx].SetImage(creature.IsEmpty ? null : creature.Creature.ImagePortrait);
            MatchFilter[idx] = !creature.IsEmpty && IsCreatureMatchesFilter(creature.Creature);
            if (!MatchFilter[idx]) {
                memberSlots[idx].FadeImageColor();
            }
            memberSlots[idx].dndHandler.Initialize(!creature.IsEmpty && MatchFilter[idx], creature, gridLayout.cellSize);
        }

        public void Show(QuestObjectivePanel pQuestObj = null) {
            if (gc.Guild == null) { //misscall from update event
                return;
            }
            this.pQuestObj = pQuestObj;
            gameObject.SetActive(true);
            pCreatureInfo.Show();

            //goMembersParent.SetActive(true);
            var creatures = gc.Guild.Members;

            for (int i = 0; i < creatures.Length; ++i) {
                UpdateSlot(i, creatures[i]);
            }
        }

        public void Hide() {
            gameObject.SetActive(false);
            pCreatureInfo.Hide();
            //goMembersParent.SetActive(false);
        }

        protected bool IsCreatureMatchesFilter(Creature cc) {
            //if (((currentFilter & CreatureFilterEnum.Heroes) == 0 && cc.Type == EntityType.Hero) //if this is hero and heroes isn't allowed
            // || ((currentFilter & CreatureFilterEnum.Creatures) == 0 && cc.Type == EntityType.Creature)) { //or if this is creature and creatures isn't allowed
            //    return false;
            //}
            return true; //otherwise - this creature is ok to be manipulated
        }

        //public void FilterMembers_Heroes() {
        //    ApplyFilterToMemberList(CreatureFilterEnum.Heroes);
        //}

        //public void FilterMembers_Creatures() {
        //    ApplyFilterToMemberList(CreatureFilterEnum.Creatures);
        //}

        //public void ApplyFilterToMemberList(CreatureFilterEnum filter) {
        //    if (currentFilter == filter) {
        //        currentFilter = CreatureFilterEnum.All;
        //    } else {
        //        currentFilter = filter;
        //    }
        //    Show();
        //}
    }
}
