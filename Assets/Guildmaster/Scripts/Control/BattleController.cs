﻿namespace Assets.Guildmaster.Scripts.Control {
    using Presentation.Battle;
    using Battle;
    using Mech;
    using Mech.Quests;
    using UnityEngine;
    using Mech.Battle;
    using UnityEngine.UI;
    using AI;
    using Mech.Abilities;

    public enum PlayerBattleMode {
        NORMAL = 0,
        TARGET_SELECTION = 1,
        ANIMATION_PLAYING = 2, //Animation is still playing
        BATTLE_FINISHED = 5
    }

    public enum BattleResult {
        Draw,
        /// <summary>
        /// Player won
        /// </summary>
        Victory,
        /// <summary>
        /// player lost
        /// </summary>
        Defeat,
        PlayerRetreat,
        EnemyRetreat
    }

    public class BattleController : MonoBehaviour {
        #region Fields
        public GuildController cGuild;
        public AbilityPanel abilityPanel;
        public Battlefield bf;
        public InitiativeQueue iniQueue;
        public InitiativePanel iniPanel;
        public Button buttonWait = null;
        public Button buttonEndTurn = null;
        private PlayerBattleMode mode;
        private int animationsInProgress;
        /// <summary>
        /// The battle tile of the creature which is doing turn right now
        /// </summary>
        private BattleCreature bcActive = null;
        private BattleAIController ai;
        private BattleTargetSelector targetSelector = null;
        private BattleTargetSelector.TargetSelectionParameters tsParams;
        private bool uiLocked = false;
        private QuestBattle questBattle;
        #endregion

        #region Accessors
        public bool HasAnimationsInProgress { get { return animationsInProgress > 0; } }
        #endregion

        private void BattleGameObjectsSetActive(bool active) {
            abilityPanel.gameObject.SetActive(active);
            iniPanel.gameObject.SetActive(active);
            buttonEndTurn.gameObject.SetActive(active);
            buttonWait.gameObject.SetActive(active);
            bf.gameObject.SetActive(active);
        }

        public void StartBattle(QuestBattle qb, Party player) {
            this.questBattle = qb;
            BattleGameObjectsSetActive(true);
            var foes = qb.GenerateFoes();
            BattleTile.BattleController = this;
            bf.Initialize();
            iniQueue = new InitiativeQueue(this);
            for(int i = 0; i < Party.MaxHeight; ++i) {
                for(int j = 0; j < Party.MaxWidth; ++j) {
                    var creature = foes.GetCreature(j, i);
                    if (creature != null) {
                        SpawnCreature(new BattleCreature(creature, false), new BattlePosition(j, Party.MaxHeight - 1 - i));
                    }
                    creature = player.GetCreature(j, i);
                    if(creature != null) {
                        SpawnCreature(new BattleCreature(creature, true), new BattlePosition(j, i + Party.MaxHeight));
                    }
                }
            }
            mode = PlayerBattleMode.NORMAL;
            targetSelector = new BattleTargetSelector(this);
            ai = new BattleAIController(this);
            iniQueue.NextRoundStarted();//Start first round
        }

        public void SpawnCreature(BattleCreature creature, BattlePosition pos) {
            bf.GetTile(pos).Set(creature);
            iniQueue.Insert(creature);
        }

        public void SelectCreature(BattleCreature selected) {
            if (selected != null) {
                if (selected.IsPlayers) {
                    if (selected != bcActive) {
                        bcActive = selected;
                        abilityPanel.SetAbilities(selected);
                        UnlockUI();
                    }
                } else {
                    bcActive = selected;
                    LockUI();
                    abilityPanel.Hide();
                    bcActive.TurnStarted();
                    if (animationsInProgress == 0 && selected.ActionsLeft > 0) {
                        ai.MakeTurn(selected);
                    } else {
                        EndTurn();
                    }
                }
                if (!bcActive.WaitingUsed) {
                    bcActive.TurnStarted();
                    if(bcActive.ActionsLeft == 0) {
                        EndTurn();
                    }
                    buttonWait.interactable = true;
                } else {
                    buttonWait.interactable = false;
                }
                HighlightMovementPossibilities(bcActive);
            }
        }

        public void HighlightMovementPossibilities(BattleCreature creature) {
            bcActive.Tile.ShowHighlight(TileHighlightType.BLUE);
            tsParams = new BattleTargetSelector.TargetSelectionParameters(creature.Tile);
            tsParams.allowed = bf.GetMovementPossibilities(creature);
            tsParams.targetType = TargetType.Tile;
            for (int i = 0; i < tsParams.allowed.Count; ++i) {
                tsParams.allowed[i].ShowHighlight(TileHighlightType.GREEN);
            }
            if(creature.IsPlayers) {
                UnlockUI();
            }
        }

        public void TryUseAbility(BattleCreature creature, int abilityIdx) {
            if(animationsInProgress > 0) {
                LockUI();
                return;
            }
            var ab = creature.AbilitySlots[abilityIdx].Ability;
            AbilityToggle abToggle = ab as AbilityToggle;
            if (abToggle != null) {
                abToggle.Toggle();
                creature.Tile.Refresh();
                abilityPanel.ButtonHovered(abilityIdx);
                //return true;
            }
            AbilityActive abActive = ab as AbilityActive;
            if (abActive != null && abActive.Available(creature.Position.Type)) {
                bf.HideSelectionHighlight();
                tsParams = new BattleTargetSelector.TargetSelectionParameters(creature.Tile, abActive);
                LockUI();
                if (targetSelector.Select(ref tsParams)) { // Targets received immediately - it is allowed tiles
                    ApplyAbilityToTargets(tsParams);
                } else {
                    targetSelector.HighlightTilesAllowedToSelect(tsParams);
                    ChangeMode(PlayerBattleMode.TARGET_SELECTION);
                }
            }
            bcActive.Tile.ShowHighlight(TileHighlightType.BLUE);
            //return false;
        }

        public void RoundStarted() {

        }
        public void RoundFinished() {
        }

        public void ChangeMode(PlayerBattleMode mode) {
            this.mode = mode;
        }

        /// <summary>
        /// Ends current creature's turn. Called from UI button
        /// </summary>
        public void EndTurn() {
            bf.HideSelectionHighlight();
            if (mode == PlayerBattleMode.BATTLE_FINISHED) {
                return;
            }
            if (animationsInProgress > 0) {
                LockUI();
                ChangeMode(PlayerBattleMode.ANIMATION_PLAYING);
                return;
            } else {
                abilityPanel.Hide();
                iniQueue.TurnFinished();
            }
        }

        /// <summary>
        /// Called from UI button
        /// </summary>
        public void WaitTurn() {
            if (!bcActive.WaitingUsed) {
                bf.HideSelectionHighlight();
                abilityPanel.Hide();
                //bf.HideSelectionHighlight();
                //btActive.ClearTargetSelection();
                iniQueue.Wait();
            }
        }

        private void TileClickedCheckMovement(BattleTile tile) {
            if (bcActive.ActionsLeft > 0 && Battlefield.LineDistanceBetweenTiles(tile.Pos, bcActive.Position) <= bcActive.ActionsLeft) {
                HighlightMovementPossibilities(bcActive);
                if (tsParams.allowed.Exists(t => t.Pos == tile.Pos)) {
                    MoveCreature(bcActive, tile);
                    if (bcActive.ActionsLeft == 0) {
                        EndTurn();
                    }
                }
            }
        }

        /// <summary>
        /// Battle tile was clicked or activated by tap
        /// </summary>
        public void TileClicked(BattleTile tile) {
            switch (mode) {
                case PlayerBattleMode.NORMAL:
                    TileClickedCheckMovement(tile);
                    break;
                case PlayerBattleMode.TARGET_SELECTION:
                    if (targetSelector.TileClicked(tile, ref tsParams)) {
                        //tsParams has all selected tiles now
                        if (tsParams.ability != null) {
                            ApplyAbilityToTargets(tsParams);
                        } else {
                            TileClickedCheckMovement(tile);
                        }
                        //tsParams.allowed.Clear();
                        //tsParams.selected.Clear();
                        //ChangeMode(PlayerBattleMode.ANIMATION_PLAYING);
                    } else {
                        ClearTargetSelection();
                    }
                    break;
            }
        }

        /// <summary>
        /// Denies any target selection if target selection was canceled
        /// </summary>
        public void ClearTargetSelection() {
            targetSelector.HideSelectionHighlight(tsParams);
            HighlightMovementPossibilities(bcActive);
            //ChangeMode(PlayerBattleMode.NORMAL);
        }

        public void ApplyAbilityToTargets(BattleTargetSelector.TargetSelectionParameters param) {
            bf.HideSelectionHighlight();
            BattleCreature creature = param.origin.Creature;
            AbilityActive ab = param.ability as AbilityActive;
            for (int i = 0; i < param.selected.Count; ++i) {
                ab.ApplyToTarget(param.selected[i], param.origin);
            }
            ActionCommited(ab.Type/*.ActionType*/, creature);
        }

        private void ActionCommited(AbilityType type, BattleCreature creature = null) {
            if (creature != null) {
                creature.ActionCommited(type);
                if (creature.ActionsLeft < 1) {
                    EndTurn();
                    return;
                }
            }
            bf.HideSelectionHighlight();
            abilityPanel.SetAbilities(bcActive);
            HighlightMovementPossibilities(bcActive);
            //if(mode != PlayerBattleMode.ANIMATION_PLAYING) {
            //    ChangeMode(PlayerBattleMode.NORMAL);
            //}
        }

        public void MoveCreature(BattleCreature creature, BattleTile destination) {
            if(uiLocked) {
                //If UI is locked we don't accept any movement while animation is playing.
                //This should fix this the dumb bug about freaky jumping on the target 
                //right after attack
                return;
            }
            bf.MoveTileContent(creature.Tile, destination);
            bcActive.ActionsLeft = Battlefield.LineDistanceBetweenTiles(creature.Tile.Pos, destination.Pos);
        }


        void Update() {
            switch (mode) {
                case PlayerBattleMode.ANIMATION_PLAYING:
                    if (animationsInProgress == 0) {
                        ChangeMode(PlayerBattleMode.NORMAL);
                        if (bcActive.ActionsLeft < 1) {
                            EndTurn();
                        } else if (!bcActive.IsPlayers) {
                            ai.MakeTurn(bcActive);
                        }
                    }
                    break;
                case PlayerBattleMode.NORMAL:
                    if (animationsInProgress > 0) {
                        LockUI();
                        ChangeMode(PlayerBattleMode.ANIMATION_PLAYING);
                    } else if (bcActive != null) {
                        if (bcActive.IsPlayers) {
                            UnlockUI();
                        }
                        if (bcActive.ActionsLeft < 1) {
                            EndTurn();
                        }
                    }
                    break;
                case PlayerBattleMode.TARGET_SELECTION:
                    if (animationsInProgress > 0) {
                        LockUI();
                        ChangeMode(PlayerBattleMode.ANIMATION_PLAYING);
                    }
                    break;
            }
        }
        public void RegisterAnimationInProgress() {
            if (animationsInProgress == 0) {
                LockUI();
            }
            ++animationsInProgress;
        }
        public void AnimationFinished() {
            --animationsInProgress;
            if(animationsInProgress == 0 && bcActive.IsPlayers) {
                UnlockUI();
            }
        }
        public void LockUI() {
            uiLocked = true;
            abilityPanel.Lock();
            buttonWait.gameObject.SetActive(false);
            buttonEndTurn.gameObject.SetActive(false);
        }
        public void UnlockUI() {
            uiLocked = false;
            abilityPanel.Unlock();
            buttonWait.gameObject.SetActive(true);
            buttonEndTurn.gameObject.SetActive(true);
        }

        public void KillCreature(BattleCreature creature) {
            iniQueue.Remove(creature);
            bf.GetTile(creature.Position).CreatureKilled();
            int playerCreatures = 0;
            int enemyCreatures = 0;
            var creaturesLeft = iniQueue.Creatures;
            for (int i = 0; i < creaturesLeft.Count; ++i) {
                if (creaturesLeft[i].IsPlayers) {
                    ++playerCreatures;
                } else {
                    ++enemyCreatures;
                }
            }
            if (playerCreatures == 0 && enemyCreatures == 0) {
                BattleFinished(BattleResult.Draw);
            } else if (playerCreatures == 0) {
                BattleFinished(BattleResult.Defeat);
            } else if (enemyCreatures == 0) {
                BattleFinished(BattleResult.Victory);
            }
        }

        /// <summary>
        /// Cursor(handle) is over battle tile
        /// </summary>
        public void TileCursorEnter(BattleTile tile, BattleTargetSelector.TargetSelectionParameters param) {
            switch (mode) {
                case PlayerBattleMode.TARGET_SELECTION:
                    //targetSelector.TileCursorEnter(tile, param == null ? tsParams : param);
                    targetSelector.TileCursorEnter(tile, param /*?? tsParams*/);
                    break;
            }
        }

        public void BattleFinished(BattleResult result) {
            mode = PlayerBattleMode.BATTLE_FINISHED;
            switch (result) {
                case BattleResult.Draw:

                    Debug.Log("Battle finished: draw");
                    break;
                case BattleResult.Victory:
                    Debug.Log("Battle finished: player wins");
                    //mc.popupOk.Show("Victory!", "Your newly minted party smoothly executes your orders. Failed killers are dead, but you need to get out before the arrival of the guards. You have no time to loot corpses, so you go on your way to the headquarters of the guild.", GoToGuildScreen);
                    break;
                case BattleResult.Defeat:
                    Debug.Log("Battle finished: player defeated");
                    //mc.popupOk.Show("Defeat!", "Your story is over at the very beginning. It seems that the guild will sink into oblivion on this continent. Or you should be replaced with more experienced leader. RIP, Guildmaster.", Application.Quit);
                    break;
            }
            BattleGameObjectsSetActive(false);
            //TODO: return a party too
            cGuild.BattleFinished(questBattle, result);
        }
    }
}
