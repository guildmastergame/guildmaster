﻿


namespace Assets.Guildmaster.Scripts.Control {
    using Mech.Quests;
    using Presentation.Guild;
    using UnityEngine;
    using UnityEngine.Events;
    using UnityEngine.UI;

    public class QuestController : MonoBehaviour {
        protected GuildController gc = null;
        protected Image[] imgQuestStatuses = null;
        public QuestInfoPanel questInfo = null;
        public QuestNote[] notes;

        void Awake() {
            for (int i = 0; i < notes.Length; ++i) {
                notes[i].button.onClick.RemoveAllListeners();
                int _i = i;
                notes[i].button.onClick.AddListener(new UnityAction(() => NoteClicked(_i)));
                notes[i].Hide();
            }
        }

        protected void NoteClicked(int idx) {
            ShowQuestDetails(notes[idx].QTree);
        }

        public void ShowQuestDetails(QuestTree qTree) {
            questInfo.Show(qTree);
        }

        public void Show() {
            gameObject.SetActive(true);
            var qTrees = gc.gd.qTrees;
            int notesShown = 0;
            for (int i = 0; i < qTrees.Count; ++i) {
                if (qTrees[i].Status == QuestTreeStatus.InProgress || qTrees[i].Status == QuestTreeStatus.Ready) {
                    notes[i].Show(qTrees[i]);
                    ++notesShown;
                }
            }
            for (int i = notesShown; i < notes.Length; ++i) {
                notes[i].Hide();
            }
        }

        public void Hide() {
            gameObject.SetActive(false);
        }

        public void Initialize(GuildController gc) {
            this.gc = gc;
            gc.gd.qTrees[0].StartQuest(gc.gd.date);
        }

        //public void AddQuest(Quest quest) {
        //    //quests.Add(quest);
        //}

        //public void RefuseQuest(Quest quest) {
        //    //quest.Status = QuestStatus.Refused;
        //    Show();
        //}

        //public void AcceptQuest(Quest quest) {
        //    //quest.Status = QuestStatus.Accepted;
        //    Show();
        //}
    }
}
