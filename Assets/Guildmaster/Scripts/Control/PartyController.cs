﻿namespace Assets.Guildmaster.Scripts.Control {
    using Presentation.Guild;
    using Mech;
    using UnityEngine;
    using UnityEngine.UI;
    using UnityEngine.Events;
    using Mech.Containers;
    using UnityEngine.EventSystems;

    public class PartyController : MonoBehaviour {
        protected Party party;
        public GridLayoutGroup gridLayout;
        public GuildMemberSlot[] partySlots = null;
        public CreatureInfoPanel pCreatureInfo = null;
        public QuestObjectivePanel pQuestObj = null;

        void Awake() {
            for (int i = 0; i < partySlots.Length; ++i) {
                partySlots[i].SetImage(null);
                partySlots[i].button.onClick.RemoveAllListeners();
                int _i = i;
                partySlots[i].button.onClick.AddListener(new UnityAction(() => PartyMemberClicked(_i)));
                //var pointerEnter = new EventTrigger.Entry();
                //pointerEnter.eventID = EventTriggerType.PointerEnter;
                //pointerEnter.callback.AddListener((eventData) => PartyMemberHovered(_i));
                partySlots[i].eventTrigger.pointerEnter = () => PartyMemberHovered(_i);
                //var pointerExit = new EventTrigger.Entry();
                //pointerExit.eventID = EventTriggerType.PointerExit;
                //pointerExit.callback.AddListener((eventData) => PartyMemberUnhovered(_i));
                partySlots[i].eventTrigger.pointerExit = () => PartyMemberUnhovered(_i);
            }
        }

        public void PartyMemberClicked(int idx) {
            var partyMember = party.GetCreature(idx % Party.MaxWidth, idx / Party.MaxWidth);
            if(partyMember != null) {
                if (pCreatureInfo.IsHidden) {
                    pQuestObj.Hide();
                    pCreatureInfo.Show();
                }
                pCreatureInfo.Initialize(partyMember, true);
            } else if (!pCreatureInfo.IsHidden) {
                pCreatureInfo.Hide();
                pQuestObj.Show();
            }
            
        }
        public void PartyMemberHovered(int idx) {
            var partyMember = party.GetCreature(idx % Party.MaxWidth, idx / Party.MaxWidth);
            if (partyMember != null) {
                if (pCreatureInfo.IsHidden) {
                    pQuestObj.Hide();
                    pCreatureInfo.Show();
                }
                pCreatureInfo.Initialize(partyMember);
            }
        }
        public void PartyMemberUnhovered(int idx) {
            if(pCreatureInfo.Clear(true)) {
                pQuestObj.Show();
            }
        }

        public void UpdateSlot(int idx, Container c) {
            CreatureContainer creature = c as CreatureContainer;
            partySlots[idx].SetImage(creature.IsEmpty ? null : creature.Creature.ImagePortrait);
            partySlots[idx].dndHandler.Initialize(!creature.IsEmpty, creature, gridLayout.cellSize);
        }



        public void Show(ref Party party) {
            gameObject.SetActive(true);
            this.party = party;
            pCreatureInfo.Show();
            pCreatureInfo.Clear(true);
            pQuestObj.Show();
            party.InitializeCallbacks(UpdateSlot);
            for(int i = 0; i < Party.MaxHeight; ++i) {
                int offset = i * Party.MaxWidth;
                for (int j = 0; j < Party.MaxWidth; ++j) {
                    UpdateSlot(offset + j, party.GetCreatureContainer(j, i));
                }
            }
                
        }

        public void Hide() {
            gameObject.SetActive(false);
        }
    }
}
