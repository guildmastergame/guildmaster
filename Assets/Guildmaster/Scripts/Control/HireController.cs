﻿namespace Assets.Guildmaster.Scripts.Control {
    using Mech.Entities.Creatures;
    using Presentation.Guild;
    using UnityEngine;
    using UnityEngine.Events;

    public class HireController : MonoBehaviour {
        [SerializeField]
        protected GuildController gc = null;
        protected int hireSlots;
        public HireCreaturePanel[] hirePanels;
        public CreatureInfoPanel pCreatureInfo;

        void Awake() {
            for (int i = 0; i < hirePanels.Length; ++i) {
                hirePanels[i].Initialize();
                hirePanels[i].ResetCreature();
                int _i = i;
                hirePanels[i].SetHireCallbacks(new UnityAction(() => HireCreature(_i)), (eventData) => SlotPortraitClicked(_i), (eventData) => SlotPortraitHovered(_i), (eventData) => SlotPortraitUnhovered(_i));
                //hirePanels[i].HireSlot.
            }
        }

        public void SlotPortraitClicked(int idx) {
            //Debug.Log("Clicks");
            pCreatureInfo.Initialize(hirePanels[idx].Creature, true);
        }
        /// <summary>
        /// Cursor entered portrait area
        /// </summary>
        /// <param name="idx"></param>
        public void SlotPortraitHovered(int idx) {
            //Debug.Log("Enter");
            var member = hirePanels[idx].Creature;
            if (member != null) {
                pCreatureInfo.Initialize(member);
            }
        }
        /// <summary>
        /// Cursor left portrait area
        /// </summary>
        /// <param name="idx"></param>
        public void SlotPortraitUnhovered(int idx) {
            //Debug.Log("Exit");
            pCreatureInfo.Clear();
        }

        public void HireCreature(int idx) {
            //Check place in creature list before
            if (gc.HireCreature(ref gc.Guild.HiringSlots[idx])) {
                if (gc.Guild.HiringSlots[idx].Count == 0) {
                    hirePanels[idx].ResetCreature();
                }
            } else { //Hire failed for some reason (not enough money in most cases)

            }
        }

        public void Show() {
            gameObject.SetActive(true);
            HiringSlot[] hireSlots = gc.Guild.HiringSlots;
            for (int i = 0; i < hireSlots.Length; ++i) {
                if (hireSlots[i].Count == 0) {
                    hirePanels[i].ResetCreature();
                    continue;
                }
                hirePanels[i].gameObject.SetActive(true);
                hirePanels[i].SetCreature(hireSlots[i]);
            }
        }

        public void Hide() {
            for (int i = 0; i < hirePanels.Length; ++i) {
                hirePanels[i].gameObject.SetActive(false);
            }
            gameObject.SetActive(false);
        }

    }
}
