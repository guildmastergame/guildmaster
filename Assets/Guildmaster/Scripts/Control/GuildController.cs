﻿namespace Assets.Guildmaster.Scripts.Control {
    using Mech;
    using Mech.Entities.Creatures;
    using UnityEngine;
    using UnityEngine.UI;
    using Presentation.Guild;
    using Mech.Quests;
    using System.Collections.Generic;
    using Mech.Quests.Dialogues;
    using System;

    public class GuildController : MonoBehaviour {
        public MasterController cMaster = null;
        public MapController cMap = null;
        public GuildMembersController cGuildMembers = null;
        public HireController cHire = null;
        public QuestController cQuest = null;
        public PartyController cParty = null;
        public DialogueController cDialogue = null;
        public BattleController cBattle = null;
        public QuestObjectivePanel pQuestObjective = null;
        public GameObject panelMenu = null;
        public GameData gd;
        public Guild Guild { get { return gd.guild; } }

        public Text textGameDate;
        public Text textMoney;
        public Text textFame;
        public GameObject spacerRightSide;//spacer for creature info and quest objective info when it hidden.

        private Queue<QuestTree> questsToProcess = new Queue<QuestTree>(10);
        /// <summary>
        /// Quest tree which is currently playing;
        /// </summary>
        private QuestTree curQTree;
        /// <summary>
        /// Called then a new game started or were loaded from a save file
        /// </summary>
        /// 

        void Update() {
            if(Input.GetKeyDown(KeyCode.Escape)) {
                ShowMap();
            }
        }


        public void GameStarted(GameData gd) {
            this.gd = gd;
            //Init guild for quick testing
            gd.guild.SetMember(0, CreatureVault.Get("guild_knight"));
            gd.guild.SetMember(1, CreatureVault.Get("guild_sorcerer"));
            gd.guild.SetMember(2, CreatureVault.Get("guild_cleric"));
            gd.guild.SetMember(3, CreatureVault.Get("guild_scout"));
            gd.guild.SetMember(4, CreatureVault.Get("guild_rookie"));
            cQuest.Initialize(this);
            UpdateGameDate();
            //Test battle
            //var nodes = gd.qTrees[0].GetNodes();
            //QuestBattle qb = null;
            //Party player = new Party();
            //player.SetCreature(CreatureVault.Get("guild_cleric"), 3, 0);
            //player.SetCreature(CreatureVault.Get("guild_rookie"), 2, 0);
            //player.SetCreature(CreatureVault.Get("guild_scout"), 1, 1);
            //player.SetCreature(CreatureVault.Get("guild_sorcerer"), 2, 1);
            ////player.SetCreature(CreatureVault.Get(""))
            //for (int i = 0; i < nodes.Count; ++i) {
            //    if(nodes[i].Type == QuestType.Skirmish) {
            //        qb = nodes[i] as QuestBattle;
            //        break;
            //    }
            //}
            //cBattle.StartBattle(qb, player);
        }

        public bool HireCreature(ref HiringSlot hiringSlot) {
            if(gd.guild.SpendMoney(hiringSlot.Price)) {
                gd.guild.AddMember(hiringSlot.Creature);
                cGuildMembers.Show();
                //TODO: decrease count of creatures in hiring slot
                return true;
            }
            return false;
        }

        public void HideAll() {
            pQuestObjective.Hide();
            cParty.Hide();
            cMap.Hide();
            cQuest.Hide();
            cHire.Hide();
            cGuildMembers.Hide();
            spacerRightSide.SetActive(false);
        }


        public void ShowHire() {
            //panelQuestObjective.Hide();
            //cParty.Hide();
            //cMap.Hide();
            //cQuest.Hide();
            HideAll();
            cHire.Show();
            cGuildMembers.Show();
        }

        public void ShowQuests() {
            //panelQuestObjective.Hide();
            //cParty.Hide();
            //cMap.Hide();
            //cHire.Hide();
            HideAll();
            //cGuildMembers.Hide();
            cQuest.Show();
        }

        /// <summary>
        /// This is require for call from UI button
        /// </summary>
        public void ShowMap() {
            ShowMap(null);
        }

        public void ShowMap(Vector2? mapPosition) {
            //cQuest.Hide();
            //cHire.Hide();
            //cGuildMembers.Hide();
            //panelQuestObjective.Hide();
            HideAll();
            //cParty.Hide();
            cMap.Show(mapPosition);
        }

        public void ShowQuestObjective(QuestTree qTree) {
            cMap.Hide();
            pQuestObjective.Show(qTree);
            cGuildMembers.Show(pQuestObjective);
            cGuildMembers.pCreatureInfo.Hide();
            var party = qTree.GetParty();
            cParty.Show(ref party);
        }

        public void StartDialogue(QuestDialogue qd, Party party) {
            cDialogue.Show(qd, party);
        }

        public void StartBattle(QuestBattle qb, Party playerParty) {
            HideAll();
            panelMenu.SetActive(false);
            cBattle.StartBattle(qb, playerParty);
        }

        public void DialogueFinished(DialogueNode node) {
            Debug.Log("Dialogue finished: " + node.Type);
            curQTree.NodePass(node.Type);
            if(curQTree.Status != QuestTreeStatus.InProgress) {
                Debug.Log(string.Format("[{0}]New quest tree status: {1}", curQTree.Name, curQTree.Status));
            }
            ProcessNextQuestTree();
        }

        public void BattleFinished(QuestBattle qb, BattleResult result, Party playerParty = null) {
            panelMenu.SetActive(true);
            ShowMap();
            Debug.Log("Battle finished: " + qb.Type);
            curQTree.NodePass(qb, result, playerParty);
            if (curQTree.Status != QuestTreeStatus.InProgress) {
                Debug.Log(string.Format("[{0}]New quest tree status: {1}", curQTree.Name, curQTree.Status));
            }
        }

        public void EndTurn() {
            cMap.Hide();
            ProcessQuestTrees();
        }

        private void EndDay() {
            gd.date.AddDays(1);
            UpdateGameDate();
            ShowMap();
        }

        private void ProcessQuestTrees() {
            for(int i = 0; i < gd.qTrees.Count; ++i) {
                if(gd.qTrees[i].IsReadyToEmbark) {
                    if(gd.qTrees[i].Status == QuestTreeStatus.Ready) {
                        gd.qTrees[i].Embark();
                    }
                    questsToProcess.Enqueue(gd.qTrees[i]);
                }
            }
            ProcessNextQuestTree();
        }

        private void ProcessNextQuestTree() {
            if(questsToProcess.Count == 0) {
                EndDay();
                return;
            }
            curQTree = questsToProcess.Dequeue();
            var node = curQTree.CurrentNode;
            switch (node.Type) {
                case QuestType.Dialogue:
                    StartDialogue(node as QuestDialogue, curQTree.GetParty());
                    break;
                case QuestType.Skirmish:
                case QuestType.Hunt:
                case QuestType.BlindSkirmish:
                    StartBattle(node as QuestBattle, curQTree.GetParty());
                    break;
            }
        }

        public void UpdateGameDate() {
            textGameDate.text = Date.Now.ToString();
        }

        public void UpdateResourceMoney(int value) {
            textMoney.text = value.ToString();
        }

        public void UpdateResourceFame(int value) {
            textFame.text = value.ToString();
        }

        
    }
}
